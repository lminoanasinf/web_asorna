<?php namespace App\Models;

use CodeIgniter\Model;

class UserAutonomiaModel extends Model
{
    protected $table = 'tbl_usuario_autonomia';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'id_autonomia',
        'id_user',
        'observacion'
    ];


}

