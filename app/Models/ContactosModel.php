<?php namespace App\Models;

use CodeIgniter\Model;

class ContactosModel extends Model
{
    protected $table = 'tbl_usuario_contactos';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'id_user',
        'id_tipo_contacto',
        'nombre',
        'telefono_fijo',
        'telefono_movil',
        'email',
        'fax',
        'observaciones'
        ];


        public function getAllByUser($id){
            $db = \Config\Database::connect();
        
            $sql = "SELECT  TTC.description AS tipo_contacto,TB.*
                    FROM $this->table TB
                        INNER JOIN tbl_tipo_contacto TTC ON TTC.id = TB.id_tipo_contacto
                    WHERE TB.id_user = $id";
                      
    
            $query = $db->query($sql);
            
            $results = $query->getResult();
            
            return json_encode($results);
        }



}

