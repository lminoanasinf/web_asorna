<?php namespace App\Models;

use CodeIgniter\Model;

class ApartadoModel extends Model
{
    protected $table = 'tbl_apartados';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'description'
    ];


    //Traemos todos los datos de roles modelando los datos para presentar en pantalla
    public function getAll()
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.description As 'Descripcion'
                FROM $this->table TB";
				  

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    //Todos los datos del usuario solicitado por id
    public function getByID($id){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TA.id As 'ID',
                    TA.description As 'Descripcion'
                FROM tbl_apartados TA
                    WHERE TA.id = $id";
                

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
    
    //Traemos los permisos para poder consultar
    public function getMenuByRole($role)
    {
        $db = \Config\Database::connect();

        $sql = "SELECT DISTINCT TB.id AS 'Id',
        TB.description AS Descripcion,
        TB.slug AS 'Slug',
        TB.sub_slug AS 'SubSlug',
        TB.icon AS 'Icon',
        TB.menu AS 'Menu'
FROM (SELECT TA.id AS 'Id',
        TA.description,
        TA.slug,
        TA.sub_slug,
        TA.icon,
        TA.menu,
          TA.orden
FROM tbl_apartados TA
    RIGHT JOIN tbl_permisos TP ON TA.id = TP.apartado_id
WHERE TP.role_id = $role
    AND TA.menu = 'Menu'
    AND TP.show_secction = 1
ORDER BY TA.orden ASC )TB";

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    //Traemos los permisos para poder consultar
    public function getMaintenanceByRole($role)
    {
        $db = \Config\Database::connect();

        $sql = "SELECT DISTINCT TA.id AS 'Id',
                        TA.description AS Descripcion,
                        TA.slug AS 'Slug',
                        TA.sub_slug AS 'SubSlug',
                        TA.icon AS 'Icon',
                        TA.menu AS 'Menu'
                FROM tbl_apartados TA
                    INNER JOIN tbl_permisos TP ON TA.id = TP.apartado_id
                WHERE TP.role_id = $role 
                    AND TA.menu = 'Mantenimiento'
                    AND TP.show_secction = 1" ;   

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
   
}

