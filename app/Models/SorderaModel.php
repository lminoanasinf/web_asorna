<?php namespace App\Models;

use CodeIgniter\Model;

class SorderaModel extends Model
{
    protected $table = 'tbl_sordera';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'tipo_sordera',
        'causa_sordera',
        'ayuda_tecnica',
        'nivel_estudios',
        'centro_estudios',
        'comunicacion',
        'id_user'
    ];
   
}

