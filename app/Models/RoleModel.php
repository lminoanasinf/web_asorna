<?php namespace App\Models;

use CodeIgniter\Model;

class RoleModel extends Model
{
    protected $table = 'tbl_roles';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'description',
        'facturar'
    ];

    //Traemos todos los datos de roles modelando los datos para presentar en pantalla
    public function getAll()
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.description As 'Descripcion',
						IF (TB.facturar = 1,'Sí','No') AS 'Facturar'
                FROM $this->table TB";
				  

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    // IF (TR.facturar = 1,'Sí','No') AS 'Facturar'
    //Todos los datos del usuario solicitado por id
    public function getByID($id){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                    TB.description As 'Descripcion',
                    TB.facturar AS 'Facturar'
                FROM $this->table TB
                    WHERE TB.id = $id";
                

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
   
}

