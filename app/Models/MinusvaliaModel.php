<?php namespace App\Models;

use CodeIgniter\Model;

class MinusvaliaModel extends Model
{
    protected $table = 'tbl_minusvalia';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'minusvalia',
        'porcentaje',
        'motivo',
        'id_user'
    ];
   
}

