<?php namespace App\Models;

use CodeIgniter\Model;

class PermsModel extends Model
{
    protected $table = 'tbl_permisos';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'role_id', 
        'apartado_id', 
        'show_secction',
        'edit_secction',
        'delete_secction',
        'create_secction'
    ];

    public function getPerms($id = '',$slug = ''){
        $db = \Config\Database::connect();
        
        $sql = "SELECT TP.id, 
            TP.role_id, 
            TP.apartado_id, 
            TP.show_secction AS 'show', 
            TP.edit_secction AS 'edit', 
            TP.delete_secction AS 'delete', 
            TP.create_secction AS 'create', 
            TR.description AS 'role_description', 
            TA.slug AS 'apartado_slug', 
            TA.description AS 'apartado_description' 
        FROM tbl_permisos TP 
            INNER JOIN tbl_roles TR ON TP.role_id = TR.id 
            INNER JOIN tbl_apartados TA ON TP.apartado_id = TA.id";

                if($id != ''){
                    $sql.= " WHERE TP.role_id = '".$id."'";
                    if($slug != ''){
                        $sql.= " AND TA.slug = '". $slug . "'";
                    }
                } else {
                    if($slug != ''){
                        $sql.= " WHERE TA.slug = '". $slug . "'";
                    }
                }
        
        $query = $db->query($sql);
        $results = $query->getResultArray();
        if($results) {
            return $results;
        } 

        return false;
    }

    public function getPermsIds($idrole = '',$idapartado = ''){
        $db = \Config\Database::connect();
        
        $sql = "SELECT TP.id, 
            TP.role_id, 
            TP.apartado_id, 
            TP.show_secction AS 'show', 
            TP.edit_secction AS 'edit', 
            TP.delete_secction AS 'delete', 
            TR.description AS 'role_description', 
            TA.slug AS 'apartado_slug', 
            TA.description AS 'apartado_description' 
        FROM tbl_permisos TP 
            INNER JOIN tbl_roles TR ON TP.role_id = TR.id 
            INNER JOIN tbl_apartados TA ON TP.apartado_id = TA.id
        WHERE TP.role_id = $idrole
            AND TP.apartado_id = $idapartado";

        $query = $db->query($sql);
        $results = $query->getResult();
        if($results) {
            return json_encode($results[0]);
        } 

        return false;
    }
}

