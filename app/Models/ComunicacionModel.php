<?php namespace App\Models;

use CodeIgniter\Model;

class ComunicacionModel extends Model
{
    protected $table = 'tbl_comunicacion';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'description',
        'online'
    ];

    //Traemos todos los datos de roles modelando los datos para presentar en pantalla
    public function getAll()
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.description As 'Descripcion',
                        TB.online AS 'Online'
                FROM $this->table TB";
				  

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    //Todos los datos del usuario solicitado por id
    public function getByID($id){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.description As 'Descripcion',
                        TB.online AS 'Online'
                FROM $this->table TB
                    WHERE TB.id = $id";
                

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
   
}

