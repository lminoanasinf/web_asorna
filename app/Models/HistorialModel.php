<?php namespace App\Models;

use CodeIgniter\Model;

class HistorialModel extends Model
{
    protected $table = 'tbl_usuario_historial';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'fecha',
        'id_user',
        'id_situacion',
        'motivo'
        ];


        public function getAllByUser($id){
            $db = \Config\Database::connect();
        
            $sql = "SELECT  TB.id,
                            TB.fecha,
                            TB.id_user,
                            TB.id_situacion,
                            TS.description AS situacion,
                            TB.motivo
                    FROM $this->table TB
                        INNER JOIN tbl_situaciones TS ON TS.id = TB.id_situacion
                    WHERE TB.id_user = $id";
                      
    
            $query = $db->query($sql);
            
            $results = $query->getResult();
            
            return json_encode($results);
        }

}

