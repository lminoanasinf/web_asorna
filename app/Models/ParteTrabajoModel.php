<?php namespace App\Models;

use CodeIgniter\Model;

class ParteTrabajoModel extends Model
{
    protected $table = 'tbl_parte_trabajo';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'id_ar', 
        'verificado', 
        'id_usuario',
        'fecha_servicio',
        'hora_inicio',
        'hora_final',
        'hora_inicio',
        'observaciones',
        'duracion',
        'n_unidades',
        'pax',
        'ps',
        'desplazamiento_ida',
        'desplazamiento_vuelta',
        'id_t'
    ];
    
    
    public function getPartesDeTrabajo($idParteTrabajo = "") {
        $db = \Config\Database::connect();

        $sql = "SELECT * FROM $this->table PT";

        if($idParteTrabajo!= "") {
            $sql.=" WHERE PT.ID = '$idParteTrabajo'" ;
        }


        $query = $db->query($sql);
		$results = $query->getResultArray();

        return $results;
    }

    public function getColumns($id = ""){

        $db = \Config\Database::connect();
        
        $sql = "SHOW columns FROM $this->table";

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    public function getApi($id = ""){

        $db = \Config\Database::connect();
        $sql = "SELECT  TB.id As ID,
                    TA.id_situaciones As 'IdSituaciones',
                    TU.role_id As 'IdRole',
                    TA.id_motivo_silse As 'IdMotivo',
                    TA.responsable AS 'IdResponsable',
                    TB.verificado As Verificado,
                    TB.duracion As Duración,
                    TA.Proyecto As Proyecto,
                    TSA.description As Situación,
                    CONCAT(TUI.name, ' ', TUI.lastname) As Interprete,
                    TB.fecha_servicio As Fecha,
                    TA.fecha_accion As Solicitud,
                    TA.hora_inicio As Inicio,
                    TA.hora_final as Fin,
                    TM.description As Motivo,
                    TA.facturar As Facturar,
                    TA.lugar As Dirección,
                    TB.n_unidades As 'NºUnidades',
                    TB.pax As PAX,
                    TB.ps As PS,
                    TB.desplazamiento_ida As 'Desplazamiento Ida',
                    TB.desplazamiento_vuelta As 'Desplazamiento Vuelta',
                    CONCAT(TU.name, ' ', TU.lastname,' ', TU.lastlastname) As Usuario,
                    TR.description As 'Tipo Usuario',
                    TS.description As Servicio,
                    TU.n_socio As 'NºSocio',
                    TA.facturar As Facturar,
                    TB.observaciones As Observaciones,
                    TA.Observaciones As 'Comunicación'
            FROM $this->table TB
            INNER JOIN tbl_acciones_realizar TA ON TB.id_ar = TA.id
            INNER JOIN tbl_usuarios TUI ON TB.id_t = TUI.id
            INNER JOIN tbl_usuarios TU ON TB.id_usuario = TU.id
            INNER JOIN tbl_motivos_silse TM ON TA.id_motivo_silse = TM.id
            INNER JOIN tbl_situaciones_acciones TSA ON TA.id_situaciones = TSA.id
            INNER JOIN tbl_servicios TS ON TA.id_servicios = TS.id
            INNER JOIN tbl_roles TR ON TU.role_id = TR.id";

            if($id != ""){
               $sql .=" WHERE TB.id = $id"; 
            }
            
                  

        $query = $db->query($sql);
        
        $results = $query->getResult();
        
        return json_encode($results);
    }

    public function datosNuevo($id = ""){

        $db = \Config\Database::connect();
        $sql = "SELECT  TB.id As ID,
                    TB.grupal As 'Grupal',
                    TB.id_situaciones As 'IdSituaciones',
                    TU.role_id As 'IdRole',
                    TB.id_motivo_silse As 'IdMotivo',
                    TB.responsable AS 'IdResponsable',
                    TB.id_persona AS 'IdUsuario',
                    TB.Proyecto As Proyecto,
                    TSA.description As Situación,
                    CONCAT(TUI.name, ' ', TUI.lastname) As Interprete,
                    TB.fecha_accion As Solicitud,
                    TB.hora_inicio As Inicio,
                    TB.hora_final as Fin,
                    TM.description As Motivo,
                    TB.facturar As Facturar,
                    TB.lugar As Dirección,
                    CONCAT(TU.name, ' ', TU.lastname,' ', TU.lastlastname) As Usuario,
                    TR.description As 'Tipo Usuario',
                    TS.description As Servicio,
                    TU.n_socio As 'NºSocio',
                    TB.facturar As Facturar,
                    TB.observaciones As 'Comunicación'
            FROM tbl_acciones_realizar TB
            INNER JOIN tbl_usuarios TUI ON TB.responsable = TUI.id
            INNER JOIN tbl_usuarios TU ON TB.id_persona = TU.id
            INNER JOIN tbl_motivos_silse TM ON TB.id_motivo_silse = TM.id
            INNER JOIN tbl_situaciones_acciones TSA ON TB.id_situaciones = TSA.id
            INNER JOIN tbl_servicios TS ON TB.id_servicios = TS.id
            INNER JOIN tbl_roles TR ON TU.role_id = TR.id
            WHERE TB.id_situaciones <> 3";
            if($id != ""){
               $sql .=" AND TB.id = $id"; 
            }

                
        $query = $db->query($sql);
        
        $results = $query->getResultArray();
        
        return json_encode($results[0]);
    }

}