<?php namespace App\Models;

use CodeIgniter\Model;

class RecursoUserModel extends Model
{
    protected $table = 'tbl_usuario_recurso';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'id_user',
        'id_recurso'
    ];

    public function findIdRecurso($id,$idrecurso){
        $db = \Config\Database::connect();
            
        $sql = "SELECT  TB.id,
                        TB.id_user,
                        TB.id_recurso,
                        TRS.description
                FROM $this->table TB
                INNER JOIN tbl_recursos_sociales TRS ON TRS.id = TB.id_recurso
                WHERE TB.id_user = $id AND TB.id_recurso = $idrecurso";   

        $query = $db->query($sql);
        
        $results = $query->getResult();
        
        return json_encode($results);
    }

    public function findbyUser($id){
        $db = \Config\Database::connect();
            
        $sql = "SELECT  TB.id,
                        TB.id_user,
                        TB.id_recurso,
                        TRS.description
                FROM $this->table TB
                INNER JOIN tbl_recursos_sociales TRS ON TRS.id = TB.id_recurso
                WHERE TB.id_user = $id";   

        $query = $db->query($sql);
        
        $results = $query->getResult();
        
        return json_encode($results);
    }

    
}

