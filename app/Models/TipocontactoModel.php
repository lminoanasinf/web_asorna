<?php namespace App\Models;

use CodeIgniter\Model;

class TipocontactoModel extends Model
{
    protected $table = 'tbl_tipo_contacto';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'description'
    ];

    //Traemos todos los datos de roles modelando los datos para presentar en pantalla
    public function getAll()
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.description As 'Descripcion'
                FROM $this->table TB";
				  

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    //Todos los datos del usuario solicitado por id
    public function getByID($id){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.description As 'Descripcion'
                FROM $this->table TB
                    WHERE TB.id = $id";
                

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
   
}

