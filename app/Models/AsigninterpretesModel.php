<?php namespace App\Models;

use CodeIgniter\Model;

class AsigninterpretesModel extends Model
{
    protected $table = 'tbl_asignacion_interpretes';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'ilse',
        'silse'
    ];
   
    public function getAll($id = "",$user = ""){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TAR.Proyecto As 'SILSE',
                        concat(TU.name,' ',TU.lastname,' ',TU.lastname) As 'ILSE',
                        TAR.fecha_accion AS 'fecha_inicio',
                        TAR.fecha_accion AS 'fecha_final',
                        TAR.hora_inicio AS 'hora_inicio',
                        TAR.hora_final AS 'hora_final',
                        TAR.observaciones as 'observaciones'
                FROM $this->table TB
                INNER JOIN tbl_usuarios TU ON TB.ilse = TU.id
                INNER JOIN tbl_acciones_realizar TAR ON TB.silse = TAR.id ";
        if($id) {
            $sql.=" WHERE TB.id=$id";
            if($user){
                $sql.= " AND TB.ilse = $user";
            }
        } else {
            if($user){
                $sql.= " WHERE TB.ilse = $user";
            } 
        }


		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    public function getNoAll($id = "",$user = ""){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.proyecto As 'SILSE',
                        TB.lugar As 'lugar',
                        'No asignada' As 'ILSE',
                        TB.fecha_accion AS 'fecha_inicio',
                        TB.fecha_accion AS 'fecha_final',
                        TB.hora_inicio AS 'hora_inicio',
                        TB.hora_final AS 'hora_final',
                        TB.observaciones as 'observaciones'
                FROM tbl_acciones_realizar TB
                WHERE TB.id_servicios = 6 AND NOT EXISTS (SELECT NULL
                     FROM tbl_asignacion_interpretes t2
                    WHERE t2.silse = TB.id)
                ";
        if($id) {
            $sql.=" WHERE TB.id=$id";
            if($user){
                $sql.= " AND TB.ilse = $user";
            }
        } else {
            if($user){
                $sql.= " WHERE TB.ilse = $user";
            } 
        }


		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    public function getCitas($id = "",$user = ""){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'id',
                        TAR.Proyecto As 'SILSE',
                        TAR.lugar As 'lugar',
                        TAR.id_motivo_silse As 'motivo',
                        concat(TU.name,' ',TU.lastname,' ',TU.lastname) As 'ILSE',
                        TAR.fecha_accion AS 'fecha_accion',
                        TAR.id_persona AS 'usuario',
                        TAR.hora_inicio AS 'hora_inicio',
                        TAR.hora_final AS 'hora_final',
                        TAR.observaciones as 'observaciones'
                FROM $this->table TB
                INNER JOIN tbl_usuarios TU ON TB.ilse = TU.id
                INNER JOIN tbl_acciones_realizar TAR ON TB.silse = TAR.id 
                WHERE TAR.id_acciones = 1";
        if($id != "") {
            $sql.=" AND TB.id=$id";
            if($user != ""){
                $sql.= " AND TB.ilse = $user";
            }
        } else {
            if($user != ""){
                $sql.= " AND TB.ilse = $user";
            } 
        }
        // return var_dump($sql);
		$query = $db->query($sql);
		
		$results = $query->getResultArray();
		
        return $results;
    }

    public function getCitasByUser($id = ""){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'id',
                        TAR.Proyecto As 'SILSE',
                        concat(TU.name,' ',TU.lastname,' ',TU.lastname) As 'ILSE',
                        TAR.fecha_accion AS 'fecha_accion',
                        TAR.hora_inicio AS 'hora_inicio',
                        TAR.hora_final AS 'hora_final',
                        TAR.observaciones as 'observaciones'
                FROM $this->table TB
                INNER JOIN tbl_usuarios TU ON TB.ilse = TU.id
                INNER JOIN tbl_acciones_realizar TAR ON TB.silse = TAR.id 
                WHERE TAR.id_acciones = 1 AND TAR.id = $id";
        // return var_dump($sql);
		$query = $db->query($sql);
		
		$results = $query->getResultArray();
		
        return $results;
    }

    public function getByID($id){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'Id',
                TB.ilse As 'Ilse',
                concat(TU.name,' ',TU.lastname,' ',TU.lastlastname) As 'Nombre',
                TB.silse As 'Silse',
                TA.proyecto As 'Proyecto'
                FROM $this->table TB
                    INNER JOIN tbl_acciones_realizar TA ON TA.id = TB.silse
                    INNER JOIN tbl_usuarios TU ON TU.id = TB.ilse
                WHERE TB.id = $id";
                
		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    public function getColumns($id = ""){

        $db = \Config\Database::connect();
        
        $sql = "SHOW columns FROM $this->table";

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
    

}

