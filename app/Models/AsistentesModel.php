<?php namespace App\Models;

use CodeIgniter\Model;

class AsistentesModel extends Model
{
    protected $table = 'tbl_asistentes';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'id_accion',
        'id_rango',
        'id_asistente',
        'hombres',
        'mujeres'
    ];


    //Traemos todos los datos de roles modelando los datos para presentar en pantalla
    public function getAll($id = NULL)
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.id_accion As 'IDAccion',
                        TB.id_rango As 'IDRango',
                        TB.id_asistente As 'IDAsistente',
                        TB.hombres As 'Hombres',
                        TB.mujeres As 'Mujeres'
                FROM $this->table TB";

        if($id) {
            $sql.=" WHERE TB.id=$id";
        }

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
    public function getAllByAccion($id = NULL)
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.id_accion As 'IDAccion',
                        TB.id_rango As 'IDRango',
                        TB.id_asistente As 'IDAsistente',
                        TB.hombres As 'Hombres',
                        TB.mujeres As 'Mujeres'
                FROM $this->table TB ";

        if($id) {
            $sql.=" WHERE TB.id_accion=$id";
        }

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
    //Todos los datos del usuario solicitado por id
    public function getByID($id){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                    TB.id_accion As 'IDAccion',
                    TB.id_rango As 'IDRango',
                    TB.id_asistente As 'IDAsistente',
                    TB.hombres As 'Hombres',
                    TB.mujeres As 'Mujeres'
                FROM $this->table TB
                    WHERE TB.id = $id";
                

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
    
    public function asistentesSocios($id = NULL)
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.id_accion As 'IDAccion',
                        TB.id_rango As 'IDRango',
                        TB.id_asistente As 'IDAsistente',
                        TB.hombres As 'Hombres',
                        TB.mujeres As 'Mujeres'
                FROM $this->table TB
                 WHERE TB.id_asistente != $id";

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
}

