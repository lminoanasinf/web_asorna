<?php namespace App\Models;

use CodeIgniter\Model;

class TareasModel extends Model
{
    protected $table = 'tbl_tareas';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'description',
        'observations',
        'service_id'
    ];

    //Traemos todos los datos de roles modelando los datos para presentar en pantalla
    public function getAll()
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.description As 'Descripcion',
                        TB.observations AS 'Observaciones',
                        TS.description AS 'Servicio'
                FROM $this->table TB
                    INNER JOIN tbl_servicios TS ON TS.id = TB.service_id";
				  

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    //Todos los datos del usuario solicitado por id
    public function getByID($id){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.description As 'Descripcion',
                        TB.observations AS 'Observaciones',
                        TS.description AS 'Servicio',
                        TB.service_id AS 'ServicioID'
                FROM $this->table TB
                    INNER JOIN tbl_servicios TS ON TS.id = TB.service_id
                WHERE TB.id = $id";
                

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
   
}

