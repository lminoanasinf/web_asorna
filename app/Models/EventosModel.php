<?php namespace App\Models;

use CodeIgniter\Model;

class EventosModel extends Model
{
    protected $table = 'tbl_eventos';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'id_user',
        'titulo',
        'fecha_inicio',
        'fecha_fin',
        'hora_inicio',
        'hora_fin',
        'comentarios',
        'color_fondo',
        'color_texto',
        'patron'
    ];

    public function getDisp($user = "",$mes  = ""){
        $db = \Config\Database::connect();

        $mes = (int)$mes + 1;
        $mes = (string)$mes;
        
        $sql = "SELECT  * 
                FROM $this->table TB";

        

        if($user != ""){
            $sql .= " WHERE id_user = $user";
        }

        if($mes != ""){
            if($user != ""){
                $sql .= " AND MONTH(fecha_inicio) = $mes";
            } else {
                $sql .= " WHERE MONTH(fecha_inicio) = $mes";
            }
        }

		$query = $db->query($sql);
		
		$results = $query->getResultArray();
		
        return $results;
    }

    public function checkDisp($user = "",$fechai = "", $fechaf = ""){
        $db = \Config\Database::connect();

        $sql = "SELECT * FROM $this->table TB WHERE id_user = '$user' AND '$fechai' BETWEEN TB.fecha_inicio AND TB.fecha_fin OR '$fechaf' BETWEEN TB.fecha_inicio AND TB.fecha_fin";

		$query = $db->query($sql);
		
		$results = $query->getResultArray();
		
        return $results;
    }
}

