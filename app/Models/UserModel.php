<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'tbl_usuarios';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'n_socio', 
        'email', 
        'password', 
        'name',
        'lastname',
        'lastlastname',
        'role_id',
        'servicioalta_id',
        'dni',
        'fecha_nacimiento',
        'lugar_nacimiento',
        'nacionalidad',
        'sexo',
        'direccion',
        'codigo_postal',
        'localidad',
        'provincia',
        'telefono_fijo',
        'telefono_movil',
        'fax',
        'envio_email',
        'envio_fax',
        'envio_movil',
        'envio_sms',
        'envio_mensajeria',
        'estado_civil',
        'hijos',
        'notas',
        'minusvalia',
        'porc_minusvalia',
        'motivo_minusvalia',
        'tipo_sordera',
        'causa_sordera',
        'ayudas_tecnicas',
        'tipo_comunicacion',
        'estudios',
        'centro_estudios',
        'nivel_estudios',
        'profesion',
        'situacion_economica',
        'situacion_laboral',
        'ambito_social',
        'fecha_defuncion',
        'updated_at',
        'eliminated',
        'comentario',
        'color_fondo',
        'color_texto',
        'device_token',
        'device'
    ];
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];


    protected function beforeInsert(array $data)
    {
        $data = $this->passwordHash($data);
        return $data;
    }
    
    protected function beforeUpdate(array $data)
    {
        $data = $this->passwordHash($data);

        return $data;
    }
    
    protected function passwordHash(array $data)
    {
        if(isset($data['data']['password'])) {
            $data['data']['password'] = password_hash($data['data']['password'],PASSWORD_DEFAULT); 
        }
        return $data;
    }

    //Traemos todos los datos de usuarios modelando los datos para presentar en pantalla
    public function getAll()
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TU.id As 'ID',
                        TU.email As 'Email',
                        TU.n_socio As 'Socio',
						TU.name As 'Nombre',
                        TU.lastname As 'ApellidoUno',
                        TU.lastlastname As 'ApellidoDos',
                        TU.telefono_movil As 'Movil',
                        TU.created_at As 'Creado',
                        TU.updated_at As 'Actualizado',
                        TR.description AS 'Permiso'
                FROM tbl_usuarios TU
					INNER JOIN tbl_roles TR ON TU.role_id = TR.id";   

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    //Todos los datos del usuario solicitado por id
    public function getByID($id){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TU.id,
                        TU.n_socio,
                        TU.email,
						TU.name,
                        TU.lastname,
                        TU.lastlastname,
                        TU.role_id,
                        TU.fecha_nacimiento,
                        TU.lugar_nacimiento,
                        TU.servicioalta_id,
                        TU.nacionalidad,
                        TU.created_at,
                        TU.updated_at,
                        TR.description
                FROM tbl_usuarios TU
                    INNER JOIN tbl_roles TR ON TU.role_id = TR.id
                WHERE TU.id = $id";
                

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
    
    //Traemos los permisos para poder consultar
    public function getPermUser($id,$slug)
    {
        $db = \Config\Database::connect();

        $sql = "SELECT  TP.show_secction AS 'Show',
                        TP.edit_secction AS 'Edit',
                        TP.delete_secction AS 'Delete',
                        TP.apartado_id AS 'Apartado',
                        TA.slug AS 'Slug'
                FROM tbl_permisos TP
                    INNER JOIN tbl_apartados TA ON TP.apartado_id = TA.id
                WHERE TP.role_id = $id AND TA.slug = '$slug'" ;   

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
   
        //Traemos todos los datos de usuarios modelando los datos para presentar en pantalla
        public function getAllData($id)
        {
            $db = \Config\Database::connect();
            
            $sql = "SELECT  TU.*,
                            TR.description AS 'TipoUsuario',
                            TS.description AS 'Servicio',
                            TSD.tipo_sordera,
                            TSD.causa_sordera,
                            TSD.ayuda_tecnica,
                            TSD.nivel_estudios,
                            TSD.centro_estudios,
                            TSD.comunicacion,
                            TM.minusvalia,
                            TM.porcentaje,
                            TM.motivo
                    FROM tbl_usuarios TU
                        INNER JOIN tbl_roles TR ON TR.id = TU.role_id
                        INNER JOIN tbl_servicios TS ON TS.id = TU.servicioalta_id
                        INNER JOIN tbl_sordera TSD ON TSD.id_user = TU.id
                        INNER JOIN tbl_minusvalia TM ON TM.id_user = TU.id
                    WHERE TU.id = $id";   
    
            $query = $db->query($sql);
            
            $results = $query->getResult();
            
            return json_encode($results);
        }

        //Traemos todos los datos de usuarios minusvalia modelando los datos para presentar en pantalla
        public function getMinusvalia($id)
        {
            $db = \Config\Database::connect();
            
            $sql = "SELECT  TB.id,
                            TB.minusvalia,
                            TB.porcentaje,
                            TB.motivo,
                            TU.name,
                            TU.lastname
                    FROM tbl_minusvalia TB
                        INNER JOIN tbl_usuarios TU ON TU.id = TB.id_user
                    WHERE TU.id = $id";   
    
            $query = $db->query($sql);
            
            $results = $query->getResult();
            
            return json_encode($results);
        }

         //Traemos todos los datos de usuarios minusvalia modelando los datos para presentar en pantalla
         public function getAutonomia($id)
         {
             $db = \Config\Database::connect();
             
             $sql = "SELECT  TB.id,
                             TB.observacion,
                             TAP.description
                     FROM tbl_usuario_autonomia TB
                         INNER JOIN tbl_usuarios TU ON TU.id = TB.id_user
                         INNER JOIN tbl_autonomia_personal TAP ON TAP.id = TB.id_autonomia
                     WHERE TU.id = $id";   
     
             $query = $db->query($sql);
             
             $results = $query->getResult();
             
             return json_encode($results);
         }

         //Traemos todos los datos de usuarios minusvalia modelando los datos para presentar en pantalla
         public function getServicios()
         {
             $db = \Config\Database::connect();
             
             $sql = "SELECT  TB.id,
                             TB.description
                     FROM tbl_servicios TB";   
     
             $query = $db->query($sql);
             
             $results = $query->getResult();
             
             return json_encode($results);
         }

         public function getSocios()
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TU.id As 'ID',
						TU.name As 'Nombre',
						TU.n_socio As 'Socio',
                        TU.lastname As 'ApellidoUno',
                        TU.lastlastname As 'ApellidoDos',
                        TU.telefono_movil As 'Movil'
                FROM tbl_usuarios TU
                WHERE TU.role_id = 6";

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    public function getColor($id)
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TU.id As 'ID',
						TU.color_fondo As 'fondo',
                        TU.color_texto As 'texto'
                FROM tbl_usuarios TU
                WHERE TU.id = $id";

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return $results;
    }


}

