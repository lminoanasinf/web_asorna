<?php namespace App\Models;

use CodeIgniter\Model;

class DatoBancoModel extends Model
{
    protected $table = 'tbl_datos_bancarios';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'fecha',
        'id_user',
        'numero_cuenta',
        'ruta'
    ];


}

