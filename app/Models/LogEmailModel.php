<?php namespace App\Models;

use CodeIgniter\Model;

class LogEmailModel extends Model
{
    protected $table = 'tbl_email_log';
    protected $allowedFields = [
        'email_from', 
        'email_to', 
        'object',
        'description'
    ];

   
}

