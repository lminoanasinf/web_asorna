<?php namespace App\Models;

use CodeIgniter\Model;

class SolicitudInterpretesModel extends Model
{
    protected $table = 'tbl_acciones_realizar';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'grupal',
        'fecha_solicitud',
        'proyecto',
        'fecha_solicitud',
        'id_servicios',
        'id_acciones',
        'id_persona',
        'responsable',
        'fecha_accion',
        'lugar',
        'hora_inicio',
        'hora_final',
        'socios',
        'facturar',
        'observaciones',
        'id_situaciones',
        'id_motivo_situaciones',
        'foto',
        'id_online'
    ];


    //Traemos todos los datos de roles modelando los datos para presentar en pantalla
    public function getAll()
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.grupal As Grupal,
        TB.fecha_solicitud As Solicitud,
        TB.id_persona As IdPersona,
        TB.proyecto As Proyecto,
        TB.id_servicios As IdServicios,
        TS.description as Servicios,
        TB.id_acciones As IdAcciones,
        TB.responsable As Responsable,
        TA.description As Acciones,
        TB.fecha_accion As Fecha,
        TB.lugar As Lugar,
        TB.hora_inicio As Inicio,
        TB.hora_final As Final,
        TB.socios As Socio,
        TB.facturar As Facturar,
        TB.observaciones As Observaciones,
        TB.id_situaciones As IdSituaciones,
        TB.id_motivo_situaciones As IdMotivoSituaciones,
        TSS.description As Situaciones,
        TMSA.description As Motivo
FROM $this->table TB
    INNER JOIN tbl_servicios TS ON TS.id = TB.id_servicios
    INNER JOIN tbl_situaciones_acciones TA ON TA.id = TB.id_acciones
    INNER JOIN tbl_situaciones TSS ON TSS.id = TB.id_situaciones
    INNER JOIN tbl_motivos_situaciones TMS ON TMS.id = TB.id_motivo_situaciones
    INNER JOIN tbl_motivos_situaciones_acciones TMSA ON TMSA.id = TB.id_motivo_situaciones
WHERE TB.id_servicios = 6 ";
                  

        $query = $db->query($sql);
        
        $results = $query->getResult();
        
        return json_encode($results);
    }

    public function getById($id)
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.grupal As Grupal,
        TB.fecha_solicitud As Solicitud,
        TB.id_persona As IdPersona,
        CONCAT(TU.name, ' ', TU.lastname) As Persona,
        TB.proyecto As Proyecto,
        TB.id_servicios As IdServicios,
        TS.description as Servicios,
        TB.id_acciones As IdAcciones,
        TB.responsable As Responsable,
        TA.description As Acciones,
        TB.fecha_accion As Fecha,
        TB.lugar As Lugar,
        TB.hora_inicio As Inicio,
        TB.hora_final As Final,
        TB.socios As Socio,
        TB.facturar As Facturar,
        TB.observaciones As Observaciones,
        TB.id_situaciones As IdSituaciones,
        TB.id_motivo_situaciones As IdMotivoSituaciones,
        TSS.description As Situaciones,
        TMS.description As Motivo
FROM $this->table TB
    INNER JOIN tbl_servicios TS ON TS.id = TB.id_servicios
    INNER JOIN tbl_situaciones_acciones TA ON TA.id = TB.id_acciones
    INNER JOIN tbl_usuarios TU ON TU.id = TB.id_persona
    INNER JOIN tbl_situaciones TSS ON TSS.id = TB.id_situaciones
    INNER JOIN tbl_motivos_situaciones TMS ON TMS.id = TB.id_motivo_situaciones
    WHERE TB.id = $id ";
                  

        $query = $db->query($sql);
        
        $results = $query->getResult();
        
        return json_encode($results);
    }

    public function getApi()
    {
        $db = \Config\Database::connect();
        $sql = "SELECT  TB.id As ID,
                    TA.description As Situaciones,
                    TMSA.description As Motivo,
                    TB.grupal As Grupal,
                    TB.fecha_solicitud As Solicitud,
                    TS.description As Proyecto,
                    TB.proyecto as Servicios,
                    TDA.description As Acciones,
                    CONCAT(TUD.name, ' ', TUD.lastname) As Responsable,
                    TB.fecha_accion As Fecha,
                    TB.lugar As Lugar,
                    TB.hora_inicio As Inicio,
                    TB.hora_final As Final,
                    TB.socios As Socio,
                    TB.facturar As Facturar,
                    TB.observaciones As Observaciones
            FROM $this->table TB
                INNER JOIN tbl_servicios TS ON TS.id = TB.id_servicios
                INNER JOIN tbl_usuarios TU ON TU.id = TB.id_persona 
                INNER JOIN tbl_usuarios TUD ON TUD.id = TB.responsable 
                INNER JOIN tbl_situaciones_acciones TA ON TA.id = TB.id_situaciones
                INNER JOIN tbl_tipo_acciones TDA ON TDA.id = TB.id_acciones
                INNER JOIN tbl_motivos_situaciones_acciones TMSA ON TMSA.id = TB.id_motivo_situaciones
            WHERE TB.id_servicios =6";
                  

        $query = $db->query($sql);
        
        $results = $query->getResult();
        
        return json_encode($results);
    }

    public function getCitas($id)
    {
        $db = \Config\Database::connect();
        $sql = "SELECT  TB.*
            FROM $this->table TB
                WHERE TB.id_servicios = 6 AND TB.id_acciones <> 3 AND TB.id_persona = $id";
                  

        $query = $db->query($sql);
        
        $results = $query->getResultArray();
        
        return $results;
    }
}