<?php namespace App\Models;

use CodeIgniter\Model;

class AutonomiapersonalModel extends Model
{
    protected $table = 'tbl_autonomia_personal';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'description'
    ];

    //Traemos todos los datos de roles modelando los datos para presentar en pantalla
    public function getAll()
    {
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.description As 'Descripcion'
                FROM $this->table TB";
				  

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    //Todos los datos del usuario solicitado por id
    public function getByID($id){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID',
                        TB.description As 'Descripcion'
                FROM $this->table TB
                    WHERE TB.id = $id";
                

		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }
   
    //Todos los datos de un usuario
    public function getUserAuto($id){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID', 
                        TB.observacion As 'Observacion', 
                        TAP.description As 'Tipo', 
                        TB.id_user AS 'id_user', 
                        TB.id_autonomia AS 'id_autonomia' 
            FROM tbl_usuario_autonomia TB 
                INNER JOIN tbl_autonomia_personal TAP ON TAP.id = TB.id_autonomia 
            WHERE TB.id_user = $id";
                
		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

    //Datos por id de usuario y auto
    public function getUserAutoByIds($id,$idautonomia){
        $db = \Config\Database::connect();
        
        $sql = "SELECT  TB.id As 'ID', 
                        TB.observacion As 'Observacion', 
                        TAP.description As 'Tipo', 
                        TB.id_user AS 'id_user', 
                        TB.id_autonomia AS 'id_autonomia' 
            FROM tbl_usuario_autonomia TB 
                INNER JOIN tbl_autonomia_personal TAP ON TAP.id = TB.id_autonomia 
            WHERE TB.id_user = $id AND TB.id_autonomia = $idautonomia";
                
		$query = $db->query($sql);
		
		$results = $query->getResult();
		
        return json_encode($results);
    }

 //Datos por id de usuario y auto
 public function getUserAutoById($id){
    $db = \Config\Database::connect();
    
    $sql = "SELECT  TB.id As 'ID', 
                    TB.observacion As 'Observacion', 
                    TAP.description As 'Tipo', 
                    TB.id_user AS 'id_user', 
                    TB.id_autonomia AS 'id_autonomia' 
        FROM tbl_usuario_autonomia TB 
            INNER JOIN tbl_autonomia_personal TAP ON TAP.id = TB.id_autonomia 
        WHERE TB.id = $id";
            
    $query = $db->query($sql);
    
    $results = $query->getResult();
    
    return json_encode($results);
}

public function createUserAuto($data){
    $db = \Config\Database::connect();
    
    $sql = "INSERT INTO tbl_usuario_autonomia (id_user, id_autonomia, observacion)
                VALUES (" . $data . ")";
            
    $query = $db->query($sql);
    
    return true;
}
    
public function updateUserAuto($id,$data){
    $db = \Config\Database::connect();
    $last= 0;
    $sql = "UPDATE tbl_usuario_autonomia 
            SET id_user=".$data['id_user'].",".
            "id_autonomia=".$data['id_autonomia'].",".
            "observacion='".$data['observacion']."' WHERE id = " . $id;
            
    $query = $db->query($sql);
    
    return true;
}
}

