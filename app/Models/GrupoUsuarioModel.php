<?php namespace App\Models;

use CodeIgniter\Model;

class GrupoUsuarioModel extends Model
{
    protected $table = 'tbl_usuario_grupos';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'id_user',
        'id_grupo',
        'observaciones'
        ];


        public function getAllByUser($id){
            $db = \Config\Database::connect();
        
            $sql = "SELECT  TGP.description AS grupo,TB.*
                    FROM $this->table TB
                        INNER JOIN tbl_grupo_pertenencia TGP ON TGP.id = TB.id_grupo
                    WHERE TB.id_user = $id";
                      
    
            $query = $db->query($sql);
            
            $results = $query->getResult();
            
            return json_encode($results);
        }

//Datos por id de usuario y grupo
public function getUserGrupoByIds($id,$idgrupo){
    $db = \Config\Database::connect();
    
    $sql = "SELECT  TB.*, TGP.description AS grupo  
        FROM tbl_usuario_grupos TB 
            INNER JOIN tbl_grupo_pertenencia TGP ON TGP.id = TB.id_grupo 
        WHERE TB.id_user = $id AND TB.id_grupo = $idgrupo";
            
    $query = $db->query($sql);
    
    $results = $query->getResult();
    
    return json_encode($results);
}

}

