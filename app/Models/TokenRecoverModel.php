<?php namespace App\Models;

use CodeIgniter\Model;

class TokenRecoverModel extends Model
{
    protected $table = 'tbl_recuperar_pass';
    protected $primaryKey = 'token';
    protected $allowedFields = [
        'token', 
        'user_id', 
        'created_at',
        'deleted_at',
        'used'
    ];
    
}

