<?php namespace App\Controllers;

use App\Models\UserModel;
use App\Models\ApartadoModel;
use App\Models\EventosModel;

class Dispinterpretes extends BaseController
{

    protected $redireccion = "dispinterpretes";
    protected $redireccionView = "maintence/dispinterpretes";
    

	public function show()
	{
		$data = [];

		// $modelSection = new UserSection();
		$modelApartado = new ApartadoModel();
									
		$modelEventos = new EventosModel();
		$modelUsuarios = new UserModel();

		$data['eventos'] ="";
		
        if(session()->get('role') == 1){
            $data['eventos'] = $modelEventos->asArray()->findAll();
        } else {
            $data['eventos'] = $modelEventos->asArray()->where('id_user',session()->get('id'))->findAll();
		}
		
		$data['interpretes'] = $modelUsuarios->asArray()->where('role_id',5)->findAll();
		
		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view($this->redireccionView.'/show',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function nuevadisp(){
		$modelEventos = new EventosModel();
		$modelUsuario = new UserModel();
		// Comprobamos el metodo de la petición

		// return var_dump($this->request->getPost());
		if($this->request->getMethod() == 'post') {
		
						  
				//Consulta id usuario para el color
				$idUsuario = $this->request->getPost('interprete');
				$color = $modelUsuario->getColor($idUsuario);

				// return var_dump($color);
				$fecha_inicio =  strtotime($this->request->getVar('fecha_inicio')); 
				$fecha_fin =  strtotime($this->request->getVar('fecha_fin')); 
			
				$dias = [
					"Monday" => "L",
					"Tuesday" => "M",
					"Wednesday" => "X",
					"Thursday" => "J",
					"Friday" => "V",
					"Saturday" => "S",
					"Sunday" => "D"
				];

				$patron = $this->request->getVar('patron');
				$cont = 0;
				$user = $modelUsuario->where('id',$this->request->getPost('interprete'))->first();
				
				if($user['color_texto'] == ""){$color_texto = "#000000";} else {$color_texto = $user['color_texto'];};
				if($patron == "" || $patron == NULL){ $patron = "LMXJVSD";}
				
				for($i=$fecha_inicio; $i<=$fecha_fin; $i+=86400){
					$stri = date("Y-m-d", $i) . $this->request->getVar('hora_inicio').":00";
					$strf = date("Y-m-d", $i) . $this->request->getVar('hora_fin').":00";
					$diai = date("Y-m-d H:i:s",strtotime($stri));
					$diaf = date("Y-m-d H:i:s",strtotime($strf));
					$dstr = date("l", $i);
					if(strpos($patron,$dias[$dstr]) !== false){
						$newData = [
							'titulo' => $user['name']." ". $user['lastname']." ".$user['lastlastname'],
							'id_user' => $this->request->getPost('interprete'),
							'fecha_inicio' => $diai,
							'fecha_fin' => $diaf,
							'hora_inicio' => $this->request->getVar('hora_inicio').":00",
							'hora_fin' => $this->request->getVar('hora_fin').":00",
							'comentarios' => $this->request->getVar('comentario'),
							'patron' => $patron,
							'color_fondo' => $user['color_fondo'],
							'color_texto' => $color_texto
						];
						//Guardamos
						$idevento = $modelEventos->insert($newData);
					}
				}
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina
				return 'redirect';

		}

		// $respuesta = $modelEventos->where('id',$id)->findAll();
		// return json_encode($respuesta);
	}

	public function actualizardisp($id){
		$modelEventos = new EventosModel();


		$modelEventos->update($id,$newData);

		return true;
	}

	public function borrardisp($id){
		$modelEventos = new EventosModel();
		$modelEventos->where('id',$id)->delete();
		return true;
	}

}
