<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use \DateTime;
use App\Models\UserModel;
use App\Models\RoleModel;
use App\Models\MotivesiteModel;
use App\Models\MotivosilseModel;
use App\Models\SituacionesModel;
use App\Models\TipoAccionesModel;
use App\Models\MotsitaccModel;
use App\Models\SitaccModel;
use App\Models\TipocontactoModel;
use App\Models\TipodocumentoModel;
use App\Models\GrupopertenenciaModel;
use App\Models\RecursossocialesModel;
use App\Models\AutonomiapersonalModel;
use App\Models\ServiciosModel;
use App\Models\TareasModel;
use App\Models\ComunicacionModel;
use App\Models\AccionesModel;
use App\Models\SolicitudInterpretesModel;
use App\Models\RangoedadModel;
use App\Models\AsistentesModel;
use App\Models\AsigninterpretesModel;
use App\Models\EventosModel;
use App\Models\ParteTrabajoModel;

class Api extends ResourceController
{
	use ResponseTrait;
    protected $format    = 'json';
    
    // API Usuarios
        // Coger todos 
        public function getUsers()
        {
            $model = new UserModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $user){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/user/edit/'.$user->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$user->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$user->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
               
                $user->btnEditar = $buttonEdit;
                $user->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function userById($id = null)
        {
            $model = new UserModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado ningun usuario con el id '.$id);
            }
        }

    // API Roles
        // Coger todos 
        public function getRoles()
        {
            $model = new RoleModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/role/edit/'.$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function roleById($id = null)
        {
            $model = new RoleModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado ningun usuario con el id '.$id);
            }
        }

    // API Motivos situaciones
        // Coger todos
        public function getMotivesite()
        {
            $slug = 'motivesite';
            $model = new MotivesiteModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function motivesitesById($id = null)
        {
            $model = new MotivesiteModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado motivos situaciones con el id '.$id);
            }
        }

    // API Situaciones
        // Coger todos
        public function getSituaciones()
        {
            $slug = 'situaciones';
            $model = new SituacionesModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function SituacionesById($id = null)
        {
            $model = new SituacionesModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado situaciones con el id '.$id);
            }
        }

    // API Tipo Acciones
        // Coger todos
        public function getTipoacciones()
        {
            $slug = 'tipoacciones';
            $model = new TipoAccionesModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function TipoaccionesById($id = null)
        {
            $model = new TipoAccionesModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado situaciones con el id '.$id);
            }
        }

    // API Motivos situaciones acciones
        // Coger todos
        public function getMotsitacc()
        {
            $slug = 'motsitacc';
            $model = new MotsitaccModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function MotsitaccById($id = null)
        {
            $model = new MotsitaccModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }

    // API Situaciones acciones
        // Coger todos
        public function getSitacc()
        {
            $slug = 'sitacc';
            $model = new SitaccModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function SitaccById($id = null)
        {
            $model = new SitaccModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }

    // API Tipo contacto
        // Coger todos
        public function getTipocontacto()
        {
            $slug = 'tipocontacto';
            $model = new TipocontactoModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function TipocontactoById($id = null)
        {
            $model = new TipocontactoModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }

    // API Tipo documento
        // Coger todos
        public function getTipodocumento()
        {
            $slug = 'tipodocumento';
            $model = new TipodocumentoModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                $item->Documento = '<a href="'.$item->Documento.'">'.$item->Documento.'</a>';
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function TipodocumentoById($id = null)
        {
            $model = new TipodocumentoModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }
    // API Grupos pertenencia
        // Coger todos
        public function getGrupopertenencia()
        {
            $slug = 'grupopertenencia';
            $model = new GrupopertenenciaModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function grupopertenenciaById($id = null)
        {
            $model = new GrupopertenenciaModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }

    // API Recursos sociales
        // Coger todos
        public function getRecursossociales()
        {
            $slug = 'recursossociales';
            $model = new RecursossocialesModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function recursossocialesById($id = null)
        {
            $model = new RecursossocialesModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }
    // API Autonomia Personal
        // Coger todos
        public function getAutonomiapersonal()
        {
            $slug = 'autonomiapersonal';
            $model = new AutonomiapersonalModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function autonomiapersonalById($id = null)
        {
            $model = new AutonomiapersonalModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }
    // API Servicios
        // Coger todos
        public function getServicios()
        {
            $slug = 'servicios';
            $model = new ServiciosModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function serviciosById($id = null)
        {
            $model = new ServiciosModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }
    // API Tareas
        // Coger todos
        public function getTareas()
        {
            $slug = 'tareas';
            $model = new TareasModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function tareasById($id = null)
        {
            $model = new TareasModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }
    
    // API Comunicación
        // Coger todos
        public function getComunicacion()
        {
            $slug = 'comunicacion';
            $model = new ComunicacionModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                if($item->Online == "1") { $item->Online = 'Sí';}else { $item->Online = "No";}
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function comunicacionById($id = null)
        {
            $model = new ComunicacionModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }

    // API AccionesIndividualesModel
        // Coger todos
        public function getAcciones()
        {
            $slug = 'acciones';
            $model = new AccionesModel();
            $modelAsistentes = new AsistentesModel();
            $modelSocios = new UserModel();
            $results = json_decode($model->getApi());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $asistentes = $modelAsistentes->asArray()->where('id_accion',$item->ID)->first();
                // return var_dump($asistentes);

                // $asistentesSocios = json_decode($modelAsistentes->asistentesSocios($item->ID));
                // $sociosHombres = 0;
                // $sociosMujeres = 0;
                // foreach($asistentesSocios as $asistente){
                //     $persona = $modelSocios->asArray()->where('id',$asistente->IDAsistente)->first();
                //     if($persona->sexo == "Varon"){
                //         $sociosHombres++;
                //     }else if($persona->sexo == "Hembra"){
                //         $sociosMujeres++;
                //     }
                // }
                // $sociosHombres = "";
                // $sociosMujeres = "";
                if(isset($asistentes['hombres']) && $asistentes['hombres']!= Null){
                    $asistentesHombres = $asistentes['hombres'];
                } else {
                    $asistentesHombres = 0;
                }

                if(isset($asistentes['mujeres']) && $asistentes['mujeres']!= Null){
                    $asistentesMujeres = $asistentes['mujeres'];
                }else {
                    $asistentesMujeres = 0;
                }

                
                $asistentesTotales = $asistentesMujeres + $asistentesHombres;

                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                // if($item->Web == "1") { $item->Web = 'Sí';}else { $item->Web = "No";}
                // if($item->Email == "1") { $item->Email = 'Sí';}else { $item->Email = "No";}
                // if($item->RRSS == "1") { $item->RRSS = 'Sí';}else { $item->RRSS = "No";}
                // if($item->Mensajeria == "1") { $item->Mensajeria = 'Sí';}else { $item->Mensajeria = "No";}
                if($item->Facturar == "1") { $item->Facturar = 'Sí';}else { $item->Facturar = "No";}
                if($item->Socio == "1") { $item->Socio = 'Sí';}else { $item->Socio = "No";}
                if($item->Grupal == "1") { $item->Grupal = 'Sí';}else { $item->Grupal = "No";}
                $item->Asistentes = (string)$asistentesTotales;
                $item->Mujeres = (string)$asistentesMujeres;
                $item->Hombres = (string)$asistentesHombres;
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function AccionesById($id = null)
        {
            $model = new AccionesModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }

        // API Solicitud interpretes
        // Coger todos
        public function getSolinterpretes()
        {
            $slug = 'solinterpretes';
            $model = new SolicitudInterpretesModel();
            $modelAsistentes = new AsistentesModel();

            $results = json_decode($model->getApi());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $asistentes = $modelAsistentes->asArray()->where('id_accion',$item->ID)->first();

                if($asistentes['hombres'] != Null){
                    $asistentesHombres = $asistentes['hombres'];
                } else {
                    $asistentesHombres = 0;
                }

                if($asistentes['mujeres'] != Null){
                    $asistentesMujeres = $asistentes['mujeres'];
                }else {
                    $asistentesMujeres = 0;
                }

                
                $asistentesTotales = $asistentesMujeres + $asistentesHombres;
                
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                // if($item->Web == "1") { $item->Web = 'Sí';}else { $item->Web = "No";}
                // if($item->Email == "1") { $item->Email = 'Sí';}else { $item->Email = "No";}
                // if($item->RRSS == "1") { $item->RRSS = 'Sí';}else { $item->RRSS = "No";}
                // if($item->Mensajeria == "1") { $item->Mensajeria = 'Sí';}else { $item->Mensajeria = "No";}
                if($item->Facturar == "1") { $item->Facturar = 'Sí';}else { $item->Facturar = "No";}
                if($item->Socio == "1") { $item->Socio = 'Sí';}else { $item->Socio = "No";}
                if($item->Grupal == "1") { $item->Grupal = 'Sí';}else { $item->Grupal = "No";}
                $item->Asistentes = (string)$asistentesTotales;
                $item->Mujeres = (string)$asistentesMujeres;
                $item->Hombres = (string)$asistentesHombres;
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function SolinterpretesById($id = null)
        {
            $model = new SolicitudInterpretesModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }

    // API Rango Edad
        // Coger todos
        public function getRangoEdad()
        {
            $slug = 'rangoedad';
            $model = new RangoedadModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function RangoEdadById($id = null)
        {
            $model = new RangoedadModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado elementos con el id '.$id);
            }
        }

    // API Motivos situaciones
        // Coger todos
        public function getMotivosilse()
        {
            $slug = 'motivosilse';
            $model = new MotivosilseModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
    
        // Uno por id
        public function MotivosilseById($id = null)
        {
            $model = new MotivosilseModel();
            $data = $model->getWhere(['id' => $id])->getResult();
            if($data){
                return $this->respond($data);
            }else{
                return $this->failNotFound('No se ha encontrado motivos situaciones con el id '.$id);
            }
        }

        // API Asignación interpretes
        // Coger todos
        public function getAsignacion()
        {
            $slug = 'asigninterpretes';
            $model = new AsigninterpretesModel();
            $results = json_decode($model->getAll());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
        public function getNoAsignacion()
        {
            $slug = 'asigninterpretes';
            $modelAsig = new AsigninterpretesModel();

            $results = json_decode($modelAsig->getNoAll());
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 
                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/new/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }

         // API Parte trabajo
        // Coger todos
        public function getParteTrabajo()
        {
            $slug = 'partetrabajo';
            $model = new ParteTrabajoModel();

            $results = json_decode($model->getApi());
           
            //personalizamos cada elemento para añadir los botones de Editar y Eliminar
            foreach($results as $item){ 

                $buttonEdit = '<form method="get" action="'.base_url().'/'.$slug."/edit/".$item->ID.'"><button id="btnEditar" type="submit" class="btn btn-primary btnEditar" data-toggle="modal" data-target="#Editar" data-id="'.$item->ID.'" style="color:white;"  >Editar</button></form>';
                $buttonDelete = '<button id="btnEliminar" type="submit" data-toggle="model" data-target="#Eliminar" data-id="'.$item->ID.'" style="color:white;" class="btn btn-danger" >Eliminar</button>';
                $item->btnEditar = $buttonEdit;
                $item->btnEliminar = $buttonDelete;
            }
            $final = ["data" => $results];
            return $this->respond($final);
        }
        
    // API MOVIL
        protected $apiKey = "Bearer 123456789";

        public function comprobarApiKey($key){
            if(isset($key) &&  $key == $this->apiKey){return true;};
            return false;
        }

        public function login()
        {
            $model = new UserModel();

            $apiKeyResult = $this->comprobarApiKey($this->request->getHeaderLine('Authorization'));

            $recive = json_decode(trim(file_get_contents('php://input')), true);

            if($recive){
                if(isset($recive['USUARIO']) && $recive['USUARIO'] != "" || isset($recive['CONTRASENA']) && $recive['CONTRASENA'] != "" ){
                    $user = $model->where('dni', $recive['USUARIO'])
                                ->first();
                                if($user) {
                                    $pass = password_verify($recive['CONTRASENA'], $user['password']);
                                } else {
                                    $pass = NULL;
                                }
                    if($user && $pass){

                        return $this->response->setStatusCode(200)
                                              ->setJSON(json_encode($user));
                    }
                    
                    $mensaje = ["mensaje" => "Usuario y/o contraseña incorrecta"];
                        return $this->response->setStatusCode(406)
                                            ->setJSON(json_encode($mensaje));

                }
                $mensaje = ["mensaje" => "No se han recibido el usuario"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));

            }
            $mensaje = ["mensaje" => "No se han recibido datos"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
        }
    
        public function cargarModalidades()
        {
            
            $model = new ComunicacionModel();

            $apiKeyResult = $this->comprobarApiKey($this->request->getHeaderLine('Authorization'));
            $data = $model->asArray()->where('online',1)->findAll();
            if($data){
                return $this->response->setStatusCode(200)
                ->setJSON(json_encode($data));
            } else {
                $data = ['mensaje' => "No se han encontrado datos"];
                return $this->response->setStatusCode(406)
                ->setJSON(json_encode($data));
            }
            
                        
        }

        public function cargarMotivos()
        {
            
            $model = new MotivosilseModel;

            $apiKeyResult = $this->comprobarApiKey($this->request->getHeaderLine('Authorization'));
            $data = $model->asArray()->findAll();
            if($data){
                return $this->response->setStatusCode(200)
                ->setJSON(json_encode($data));
            } else {
                $data = ['mensaje' => "No se han encontrado datos"];
                return $this->response->setStatusCode(406)
                ->setJSON(json_encode($data));
            }
            
                        
        }
        public function solicitudInterprete()
        {
            $model = new SolicitudInterpretesModel();

            $apiKeyResult = $this->comprobarApiKey($this->request->getHeaderLine('Authorization'));

            $recive = json_decode(trim(file_get_contents('php://input')), true);

            if($recive){
                   
                if($recive['USUARIO']){$usuario = $recive['USUARIO'];} else {$usuario = "";}
                if($recive['FECHA']){$fecha = str_replace('/', '-', $recive['FECHA']);$fecha_solicitud = date('Y-m-d',strtotime($fecha));}else {$fecha_solicitud = "";}
                if($recive['MODALIDAD']){$modalidad = $recive['MODALIDAD'];}else {$modalidad ="";}
                if($recive['LUGAR']){$lugar = $recive['LUGAR'];}else {$lugar = "";}
                if($recive['HORA_INICIO']){$hora_inicio = $recive['HORA_INICIO'];}else {$hora_inicio = "";}
                if($recive['HORA_FIN']){$hora_fin = $recive['HORA_FIN'];}else {$hora_fin = "";}
                if($recive['MOTIVO']){$motivo = $recive['MOTIVO'];}else {$motivo = "";}
                if($recive['OBSERVACIONES']){$observaciones = $recive['OBSERVACIONES'];}else {$observaciones = "";}
                if($recive['IMAGEN']){$foto = $recive['IMAGEN'];}else {$foto = "";}

                $newData =[
                    'grupal' => 0,
                    'fecha_solicitud' => $fecha_solicitud,
                    'id_persona' => $usuario,
                    'proyecto' => "",
                    'id_servicios' => 6,
                    'id_acciones' => 1,
                    'responsable' => 1,
                    'fecha_accion' => date_format(new DateTime('NOW'),"Y-m-d"),
                    'lugar' => $lugar,
                    'hora_inicio' => $hora_inicio,
                    'hora_final' => $hora_fin,
                    'asistentes' => 0,
                    'socios' => 0,
                    'facturar' => 0,
                    'documento' => 1,
                    'observaciones' =>$observaciones,
                    'id_motivo_silse' =>$motivo,
                    'id_situaciones' => 1,
                    'id_motivo_situaciones' => 1,
                    'foto' => $foto,
                    'id_online' => $modalidad
                ];

                $id = $model->insert($newData);
                            
                if($id){
                    $mensaje = ["mensaje" => "Solicitud insertada correctamente"];
                    return $this->response->setStatusCode(200)
                                        ->setJSON(json_encode($mensaje));
                } else {
                    $mensaje = ["mensaje" => "Ha surgido un problema al realizar la solicitud"];
                    return $this->response->setStatusCode(406)
                                        ->setJSON(json_encode($mensaje));
                }
                    
            } else {
                $mensaje = ["mensaje" => "No se ha rellenado correctamente la solicitud"];
                    return $this->response->setStatusCode(406)
                                        ->setJSON(json_encode($mensaje));
            }

        }

        public function citas()
        {
            $model = new SolicitudInterpretesModel();
            $modelInt = new AsigninterpretesModel();
            helper('html');
            $apiKeyResult = $this->comprobarApiKey($this->request->getHeaderLine('Authorization'));

            $recive = json_decode(trim(file_get_contents('php://input')), true);
            if(!$recive['USUARIO'] || $recive['USUARIO'] == ""){
                $mensaje = ["mensaje" => "Error al solicitar los datos"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            }
            if($recive){
                   
                $datos = $model->getCitas($recive['USUARIO']);

                $newData = [];
                if($datos){
                    foreach($datos as $dato){

                        $datosInt = $modelInt->getCitasByUser($dato['id']);

                        if($dato['foto']) {
                            $comentario = "Pendiente de revisar la fotografía";
                        } else{
                            $comentario = $dato['observaciones'];
                        }

                        if($datosInt != []){

                            $interprete = $datosInt[0]['ILSE'];

                        }else {
                            $interprete = "Interprete no asignada";
                        }
                        $newData[] = [
                            'lugar' => $dato['lugar'],
                            'fecha_solicitud' => invert_fecha($dato['fecha_solicitud'],true),
                            'hora_inicio' => $dato['hora_inicio'],
                            'hora_final' => $dato['hora_final'],
                            'observaciones' => $comentario,
                            'interprete' => $interprete,
                            'id_situaciones' => $dato['id_situaciones'],
                        ];
                    }
                    return $this->response->setStatusCode(200)
                                        ->setJSON(json_encode($newData));
                } else {
                    $mensaje = ["mensaje" => "No hay ninguna solicitud"];
                    return $this->response->setStatusCode(406)
                                        ->setJSON(json_encode($mensaje));
                }
                    
            } else {
                $mensaje = ["mensaje" => "Ha surgido un problema al revisar las solicitudes, contacte directamente con ASORNA"];
                    return $this->response->setStatusCode(406)
                                        ->setJSON(json_encode($mensaje));
            }

        }

        function registrar_token(){
            $recive = json_decode(trim(file_get_contents('php://input')), true);
            $model = new UserModel();

            if(!$recive['USUARIO'] || $recive['USUARIO'] == ""){
                $mensaje = ["mensaje" => "No se ha enviado usuario"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            }
            if(!$recive['TOKEN'] || $recive['TOKEN'] == ""){
                $mensaje = ["mensaje" => "No se ha enviado token"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            }

            $newData = [
                'id' => $recive['USUARIO'],
                'device_token' => $recive['TOKEN'],
                'device' => $recive["DEVICE"]
            ];

            $model->save($newData);

            $mensaje = ["mensaje" => "Token registrado correctamente"];

            return $this->response->setStatusCode(200)
            ->setJSON(json_encode($mensaje));

        }

        function borrar_token(){
            $recive = json_decode(trim(file_get_contents('php://input')), true);
            $model = new SolicitudInterpretesModel();
            if(!$recive['USUARIO'] || $recive['USUARIO'] == ""){
                $mensaje = ["mensaje" => "No se ha enviado usuario"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            }
            $newData = [
                'id' => $recive['USUARIO'],
                'device_token' => ''
            ];

            $model->save($newData);

            $mensaje = ["mensaje" => "Token registrado correctamente"];

            return $this->response->setStatusCode(406)
            ->setJSON(json_encode($mensaje));

        }

        //Enviar push
        function push_notification_android(){
            $model = new UserModel();

            $recive = json_decode(trim(file_get_contents('php://input')), true);

            #API access key from Google API's Console
            define( 'API_ACCESS_KEY', 'AAAAIaqjsDA:APA91bEuezewjMygfn2HrHHKnIP1k8AiWROfFDrKqH1xZ58oBwiqDAm7tghPsFCO0SmuWgV69MkgLBBGBoqojTUsTgH4mWkQo3A_NGapYbtmyM4AQMvQvANQVLh44bKVHuXglWZ8KIyP' );
            // $registrationIds = $_GET['id'];

            $user = $model->where("device_token",$recive['TOKEN'])->first();
            $device = $user['device'];
        #prep the bundle
            $msg = array
                (
                'body' 	=> 'Se ha asignado una interprete a su solicitud',
                'title'	=> 'ASORNA - Notificacion',
                        'icon'	=> 'myicon',/*Default Icon*/
                        'sound' => 'mySound',/*Default sound*/
                        "badge" => 1
                );

                if($device == "iOS"){
                    $fields = array
                    (
                        'to'		=> $recive['TOKEN'],
                        'payload' => [
                            'aps' => [
                                'alert' => [
                                    'title' => '$GOOG up 1.43% on the day',
                                    'body' => '$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
                                ],
                                'badge' => 42,
                                'sound' => 'default',
                            ],
                        ]
                    );
                } else {
                    $fields = array
                    (
                        'to'		=> $recive['TOKEN'],
                        'data'	=> $msg
                    );
                }
            
            $headers = array
                    (
                        'Authorization: key=' . API_ACCESS_KEY,
                        'Content-Type: application/json'
                    );

        #Send Reponse To FireBase Server	
                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                curl_setopt( $ch,CURLOPT_POST, true );
                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                $result = curl_exec($ch );
                curl_close( $ch );

        #Echo Result Of FireBase Server
        echo $result;
        }

        public function dispinterprete()
        {
            $model = new EventosModel();
            helper('html');
            $apiKeyResult = $this->comprobarApiKey($this->request->getHeaderLine('Authorization'));

            $recive = json_decode(trim(file_get_contents('php://input')), true);
            if(!isset($recive['USUARIO'])){
                $mensaje = ["mensaje" => "Error al solicitar los datos"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            }
            if($recive['USUARIO'] != ""){
                   
                $datos = $model->getDisp($recive['USUARIO'],$recive['MES']);
                $newData = [];
                if($datos){
                    foreach($datos as $dato){
                        $newData[] = [
                            'id' => $dato['id'],
                            'titulo' => $dato['titulo'],
                            'fecha_inicio' => invert_fecha($dato['fecha_inicio'],true),
                            'fecha_fin' => invert_fecha($dato['fecha_fin'],true),
                            'hora_inicio' => $dato['hora_inicio'],
                            'hora_fin' => $dato['hora_fin'],
                            'comentarios' => $dato['comentarios']
                        ];
                    }
                    return $this->response->setStatusCode(200)
                                        ->setJSON(json_encode($newData));
                } else {
                    $mensaje = ["mensaje" => "No hay ninguna disponibilidad asociada"];
                    return $this->response->setStatusCode(406)
                                        ->setJSON(json_encode($mensaje));
                }
                    
            } else if($recive['USUARIO'] == ""){
            
                $datos = $model->asArray()->findAll();
                $newData = [];
                if($datos){
                    foreach($datos as $dato){
                        $newData[] = [
                            'id' => $dato['id'],
                            'titulo' => $dato['titulo'],
                            'fecha_inicio' => invert_fecha($dato['fecha_inicio'],true),
                            'fecha_fin' => invert_fecha($dato['fecha_fin'],true),
                            'hora_inicio' => $dato['hora_inicio'],
                            'hora_fin' => $dato['hora_fin'],
                            'comentarios' => $dato['comentarios']
                        ];
                    }
                    return $this->response->setStatusCode(200)
                                        ->setJSON(json_encode($newData));
                } else {
                    $mensaje = ["mensaje" => "No hay ninguna disponibilidad asociada"];
                    return $this->response->setStatusCode(406)
                                        ->setJSON(json_encode($mensaje));
                }
            
            }else {
                $mensaje = ["mensaje" => "Ha surgido un problema al revisar las disponibilidades, contacte directamente con ASORNA"];
                    return $this->response->setStatusCode(406)
                                        ->setJSON(json_encode($mensaje));
            }

        }

        public function agregardispinterprete()
        {
            $model = new EventosModel();
            $modelUsuario = new UserModel();

            helper('html');

            $apiKeyResult = $this->comprobarApiKey($this->request->getHeaderLine('Authorization'));

            $recive = json_decode(trim(file_get_contents('php://input')), true);

            if(!isset($recive['USUARIO'])){
                $mensaje = ["mensaje" => "Error al recibir el usuario"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            }
            if(!isset($recive['FECHA_INICIO'])){
                $mensaje = ["mensaje" => "Error al recibir la fecha de inicio"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            }
            if(!isset($recive['FECHA_FIN'])){
                $mensaje = ["mensaje" => "Error al recibir la fecha de fin"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            }
            if(!isset($recive['HORA_INICIO'])){
                $mensaje = ["mensaje" => "Error al recibir la hora de incio"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            }
            if(!isset($recive['HORA_FIN'])){
                $mensaje = ["mensaje" => "Error al recibir la hora de fin"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            }
            
            if(!isset($recive['COMENTARIO'])){
                $mensaje = ["mensaje" => "Error al recibir el comentario"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            } 
            if(!isset($recive['PATRON'])){
                $mensaje = ["mensaje" => "Error al recibir el comentario"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            } 

            if($recive['USUARIO'] != ""){
                   
                $usuario = $modelUsuario->asArray()->where('id',$recive['USUARIO'])->first();
                $disp = $model->asArray()->where('id',$recive['USUARIO']);

                $newData = [];
                if($usuario){

                    //Consulta id usuario para el color
				$idUsuario = $this->request->getPost('interprete');
				
                
                // return var_dump(invert_fecha_api($recive['FECHA_INICIO'],true));
                
				$fecha_inicio =  strtotime(invert_fecha_api($recive['FECHA_INICIO'],true)); 
				$fecha_fin =  strtotime(invert_fecha_api($recive['FECHA_FIN'],true)); 
                    
                
				$dias = [
					"Monday" => "L",
					"Tuesday" => "M",
					"Wednesday" => "X",
					"Thursday" => "J",
					"Friday" => "V",
					"Saturday" => "S",
					"Sunday" => "D"
				];

				$patron = $recive['PATRON'];
				$cont = 0;
                if($usuario['color_texto'] == ""){$color_texto = "#000000";} else {$color_texto = $usuario['color_texto'];};
                
                if($patron == "" || $patron == NULL){ $patron = "LMXJVSD";}
                for($i=$fecha_inicio; $i<=$fecha_fin; $i+=86400){
					$stri = date("Y-m-d", $i) . $recive['HORA_INICIO']; 
                    $strf = date("Y-m-d", $i) . $recive['HORA_FIN'];
                    
					$diai = date("Y-m-d H:i:s",strtotime($stri));
                    $diaf = date("Y-m-d H:i:s",strtotime($strf));

                    $dstr = date("l", $i);
					if(strpos($patron,$dias[$dstr]) !== false){
                        $check = $model->checkDisp($recive['USUARIO'],$diai,$diaf);

                       if($check != []){
                        $mensaje = ["error"=> true, "mensaje" => "Algunas disponibilidades entran en conflicto"];
                        return $this->response->setStatusCode(200)
                                            ->setJSON(json_encode($mensaje));  
                       } 
                       
                    }
                }
                   
				for($i=$fecha_inicio; $i<=$fecha_fin; $i+=86400){
					$stri = date("Y-m-d", $i) . $recive['HORA_INICIO']; 
                    $strf = date("Y-m-d", $i) . $recive['HORA_FIN'];
                    
					$diai = date("Y-m-d H:i:s",strtotime($stri));
                    $diaf = date("Y-m-d H:i:s",strtotime($strf));

                    $dstr = date("l", $i);
					if(strpos($patron,$dias[$dstr]) !== false){
                        $check = $model->checkDisp($recive['USUARIO'],$diai,$diaf);
                       
						$newData = [
							'titulo' => $usuario['name']." ". $usuario['lastname']." ".$usuario['lastlastname'],
							'id_user' => $recive['USUARIO'],
							'fecha_inicio' => $diai,
							'fecha_fin' => $diaf,
							'hora_inicio' => $recive['HORA_INICIO'],
							'hora_fin' => $recive['HORA_FIN'],
							'comentarios' => $recive['COMENTARIO'],
							'patron' => $patron,
							'color_fondo' => $usuario['color_fondo'],
							'color_texto' => $color_texto
                        ];

                        
						//Guardamos
                        $idevento = $model->insert($newData);
                        //variable booleana 'error' en caso de que exista
					}
                }
                    $mensaje = ["mensaje" => "La disponibilidad se ha insertado correctamente"];
                    return $this->response->setStatusCode(200)
                                        ->setJSON(json_encode($mensaje));
                } else {
                    $mensaje = ["mensaje" => "No existe el usuario"];
                    return $this->response->setStatusCode(406)
                                        ->setJSON(json_encode($mensaje));
                }
                    
            }else {
                $mensaje = ["mensaje" => "Ha surgido un problema al insertar las disponibilidades, contacte directamente con ASORNA"];
                    return $this->response->setStatusCode(406)
                                        ->setJSON(json_encode($mensaje));
            }

        }

        public function citasinterprete()
        {
            $model = new AsigninterpretesModel();
            $modelMotivo = new MotivosilseModel();
            $modelUsuario = new UserModel();

            helper('html');
            $apiKeyResult = $this->comprobarApiKey($this->request->getHeaderLine('Authorization'));

            $recive = json_decode(trim(file_get_contents('php://input')), true);

            if(!isset($recive['USUARIO'])){
                $mensaje = ["mensaje" => "Error al solicitar los datos"];
                return $this->response->setStatusCode(406)
                                    ->setJSON(json_encode($mensaje));
            }
            if($recive['USUARIO'] != ""){
                   
                $datos = $model->getCitas("",$recive['USUARIO']);
                $newData = [];

                if($datos){
                    
                    foreach($datos as $dato){
                        $datosInt = $model->getCitasByUser($dato['id']);
                        $datosMotivo = $modelMotivo->where('id',$dato['motivo'])->first();
                        $datosUser = $modelUsuario->where('id',$dato['usuario'])->first();

                        if($datosInt != []){
    
                            $interprete = $datosInt[0]['ILSE'];
    
                        }else {
                            $interprete = "Interprete no asignada";
                        }

                        if($datosMotivo == 0 || !$datosMotivo) {
                            $motivo = "No hay motivo";
                        } else {
                            $motivo = $datosMotivo['description'];
                        }
                        $newData[] = [
                            'id' => $dato['id'],
                            'lugar' => $dato['lugar'],
                            'fecha_accion' => invert_fecha($dato['fecha_accion'],true),
                            'hora_inicio' => $dato['hora_inicio'],
                            'hora_final' => $dato['hora_final'],
                            'observaciones' => $dato['observaciones'],
                            'interprete' => $interprete,
                            'usuario' => $datosUser['name'] . " " . $datosUser['lastname']. " " . $datosUser['lastlastname'],
                            'motivo'=> $motivo
                        ];
                    }
                    return $this->response->setStatusCode(200)
                                        ->setJSON(json_encode($newData));
                } else {
                    $mensaje = ["mensaje" => "No hay ninguna cita asociada"];
                    return $this->response->setStatusCode(406)
                                        ->setJSON(json_encode($mensaje));
                }
                    
            } else if($recive['USUARIO'] == ""){
            
                $datos = $model->getCitas("","");
                $newData = [];
                if($datos){
                    foreach($datos as $dato){
                        $datosInt = $modelInt->getCitasByUser($dato['id']);
                    if($datosInt != []){

                        $interprete = $datosInt[0]['ILSE'];

                    }else {
                        $interprete = "Interprete no asignada";
                    }
                        $newData[] = [
                            'id' => $dato['id'],
                            'proyecto' => $dato['SILSE'],
                            'fecha_inicio' => invert_fecha($dato['fecha_inicio'],true),
                            'fecha_fin' => invert_fecha($dato['fecha_final'],true),
                            'hora_inicio' => $dato['hora_inicio'],
                            'hora_fin' => $dato['hora_final'],
                            'observaciones' => $dato['observaciones'],
                            'interprete' => $interprete
                        ];
                    }
                    return $this->response->setStatusCode(200)
                                        ->setJSON(json_encode($newData));
                } else {
                    $mensaje = ["mensaje" => "No hay ninguna cita asociada"];
                    return $this->response->setStatusCode(406)
                                        ->setJSON(json_encode($mensaje));
                }
            
            }else {
                $mensaje = ["mensaje" => "Ha surgido un problema al revisar las citaas, contacte directamente con ASORNA"];
                    return $this->response->setStatusCode(406)
                                        ->setJSON(json_encode($mensaje));
            }

        }


        public function descargarapk(){
            ftp://proxxi@proxxi.es/web/asorna/uploads
            return $this->response->download(base_url()."/assets/uploads/Asorna.apk",null);
        }
    }