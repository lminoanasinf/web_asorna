<?php namespace App\Controllers;

use App\Models\UserModel;
use App\Models\RoleModel;
use App\Models\PermsModel;
use App\Models\ApartadoModel;
use App\Models\AsigninterpretesModel;
use App\Models\AccionesModel;
use App\Models\ParteTrabajoModel;
use App\Models\SolicitudInterpretesModel;

class Partetrabajo extends BaseController
{
	public function index()
	{
		$data = [];

		
		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view('templates/dashboard/dashboard',$data);
		echo view('templates/dashboard/footer',$data);
	}

	// Ver
	public function show()
	{

		helper(['form']);
		$uri = service('uri');
		$modelPerm = new PermsModel();
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['show'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para ver esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to('/dashboard');
		}
		
		$data = [];
		$col =[];
		$model = new ParteTrabajoModel();

		
			$columns = json_decode($model->getApi());
			foreach($columns[0] as $key => $value){
				$col[0][$key] =  "";
			}

		$data['columns'] = $col;
		
		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view('maintence/partetrabajo/show',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function edit($id = "")
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$modelPerm = new PermsModel();
		$model = new ParteTrabajoModel();

		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
		
		if($id == "") {
			// $id = $this->request->getVar('id');

			if($id == ""){
				// Creamos una session para mostrar el mensaje de denegación por permiso
				$session = session();
				$session->setFlashdata('error', 'No se ha seleccionado ningun elemento para editar');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/asigninterpretes/show');
			}
		}
		if($id != ""){
			$data['id'] = $id;

		}

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/asigninterpretes/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {
		

			// reglas de validación
			$rules = [
				'verificado' => 'required',
				'duracion' => 'required',
				'unidades' => 'required',
				'PAX' => 'required',
				'PS' => 'required',
				'despIda' => 'required',
				'despVuelta' => 'required'
			];

			// Comprobación de las validaciones
			if(! $this->validate($rules)) {

				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;

			} else {

				// Acutlizar
				$newData = [
                    'id' => $id,
					'verificado' => $this->request->getVar('verificado'),
					'duracion' => $this->request->getVar('duracion'),
					'unidades' => $this->request->getVar('unidades'),
					'PAX' => $this->request->getVar('PAX'),
					'PS' => $this->request->getVar('PS'),
					'despIda' => $this->request->getVar('despIda'),
					'despVuelta' => $this->request->getVar('despVuelta')
				];

				$model->save($newData);

				}
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/asigninterpretes/edit/'.$id);
		}


		$data['data'] = json_decode($model->getApi($id));

		if($data['data'] == []){
			$data['data'] =[
				"ID" => "1",
				"IdSituaciones" => "1",
				"IdRole" => "1",
				"IdMotivo" => "1",
				"IdResponsable" => "1",
				"Verificado" => "No",
				"Duración" => "2",
				"Proyecto" => "Traducción",
				"Situación" => "Alta", 
				"Interprete" => "Luis Miño",
				"Fecha" => "2021-03-29",
				"Solicitud" => "2020-10-13 16:32:41",
				"Inicio" => "16:00:00",
				"Fin" => "17:00:00",
				"Motivo" => "Laboral",
				"Facturar" => "1",
				"Dirección" => "Palacio de congresos",
				"NºUnidades" => "8",
				"PAX" => "1",
				"PS" => "1",
				"Desplazamiento Ida" => "2",
				"Desplazamiento Vuelta" => "2",
				"Usuario" => "Juan Lopez Gomez",
				"Tipo Usuario" => "Administrador sistema",
				"Servicio" => "SILSE",
				"NºSocio" => "0",
				"Observaciones" => "Observaciones de parte de trabajo",
				"Comunicación" => "0"
			];
		}
		if($modelPerm->getPerms($id)){
		$data['perms'] = $modelPerm->getPerms($id);

		} else {
		$data['perms'] = [];

		}

			
		echo view('templates/dashboard/header',$data);
		echo view('maintence/partetrabajo/edit',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function new()
	{
				//Variable con todos los datos a pasar a las vistas
				$data = [];

				// Cargamos los helpers de formularios
				helper(['form']);
				$uri = service('uri');
				$modelPerm = new PermsModel();
				$model = new ParteTrabajoModel();
				$modelSolicitud = new SolicitudInterpretesModel();
		
				$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
				
				if($perm[0]['create'] == 0){
					// Creamos una session para mostrar el mensaje de denegación por permiso
					$session = session();
					$session->setFlashdata('error', 'No tienes permisos para crear en esta sección');
					
					// Redireccionamos a la pagina de login
					return redirect()->to(base_url().'/asigninterpretes/show');
				}
		
				// Comprobamos el metodo de la petición
				if($this->request->getMethod() == 'post') {
		
					// reglas de validación
					$rules = [
                        'duracion' => 'required',
                        'unidades' => 'required',
                        'PAX' => 'required',
                        'PS' => 'required',
                        'despIda' => 'required',
                        'despVuelta' => 'required',
                        'id_ar' => 'required'
                    ];
		
					// Comprobación de las validaciones
					if(! $this->validate($rules)) {
		
						
						
						// Guardamos el error para mostrar en la vista
						$data['validation'] = $this->validator;
		
					} else {
		
						// Acutlizar rol
						$newData = [
                            'verificado' => $this->request->getVar('verificado'),
                            'duracion' => $this->request->getVar('duracion'),
                            'n_unidades' => $this->request->getVar('unidades'),
                            'pax' => $this->request->getVar('PAX'),
                            'ps' => $this->request->getVar('PS'),
                            'desplazamiento_ida' => $this->request->getVar('despIda'),
                            'desplazamiento_vuelta' => $this->request->getVar('despVuelta'),
                            'id_ar' => $this->request->getVar('id_ar'),
							'fecha_servicio' => $this->request->getVar('fechasolicitud'),
                            'hora_inicio' => $this->request->getVar('horainicio'),
                            'hora_fin' => $this->request->getVar('horafin'),
                            'id_usuario' => $this->request->getVar('idusuario'),
                            'id_t' => $this->request->getVar('idresponsable')
                        ];
						// return var_dump($newData);
                        $model->insert($newData);

						$newData = [
							'id' => $this->request->getVar('id_ar'),
							'id_situaciones' => 3
						];

						$modelSolicitud->save($newData);


						// Creamos una session para mostrar el mensaje de registro correcto
						$session = session();
						$session->setFlashdata('success', 'Actualizado correctamente');
						
						// Redireccionamos a la pagina de login
						return redirect()->to(base_url().'/partetrabajo/show');
					}
		
				}
                
				$data['silse'] = $modelSolicitud->asArray()->findAll();

				echo view('templates/dashboard/header',$data);
				echo view('maintence/partetrabajo/new',$data);
				echo view('templates/dashboard/footer',$data);
	}

	// Borrar
	public function delete($id)
	{
		$modelPerm = new PermsModel();
		$perm = $modelPerm->getPerms(session()->get('role'),'role');

		if($perm[0]['delete'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para borrar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to('/role/show');
		}

		$modelUser = new UserModel();
		$users = $modelUser->where('role_id',$id)->findAll();

		if($users){
			// Creamos una session para mostrar el mensaje de error
			$session = session();
			$session->setFlashdata('error', 'No puedes eliminar un Tipo que contenga usuarios');
			return redirect()->to('/role/show');
		} 

		$model = new RoleModel();
		$answer = $model->delete($id);
		// Creamos una session para mostrar el mensaje de registro correcto
		$session = session();
		$session->setFlashdata('success', 'Eliminado correctamente');

		// Redireccionamos a la pagina de login
		return redirect()->to(base_url().'/role/show/');

	}

    function datos(){
        $model = new ParteTrabajoModel();

        $id = $this->request->getVar('id');

        echo $model->datosNuevo($id);
        
    }

}