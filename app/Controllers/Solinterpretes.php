<?php namespace App\Controllers;

use App\Models\UserModel;
use App\Models\PermsModel;
use App\Models\RoleModel;
use App\Models\ServiciosModel;
use App\Models\SitaccModel;
use App\Models\SituacionesModel;
use App\Models\MotsitaccModel;
use App\Models\AutonomiapersonalModel;
use App\Models\SolicitudInterpretesModel;
use App\Models\MotivesiteModel;
use App\Models\TipoAccionesModel;
use App\Models\ComunicacionModel;
use App\Models\RangoedadModel;
use App\Models\AsistentesModel;
use App\Models\AccionesModel;
use App\Models\MotivosilseModel;

class Solinterpretes extends BaseController
{
    protected $redireccion = "solinterpretes";
    protected $redireccionView = "maintence/solinterpretes";

    public function index()
    {
        $this->show();
    }

    // Ver
    public function show()
    {

        helper(['form']);
        $uri = service('uri');
        $uri = service('uri'); 
        $modelPerm = new PermsModel();
        $modelServicio = new ServiciosModel();
        $model = new SolicitudInterpretesModel();

        $perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

        if($perm[0]['show'] == 0){
            // Creamos una session para mostrar el mensaje de denegación por permiso
            $session = session();
            $session->setFlashdata('error', 'No tienes permisos para ver esta sección');
            
            // Redireccionamos
            return redirect()->to('/dashboard');
        }
        
        $data = [];

        $data['columns'] = json_decode($model->getApi());
        $data['servicios'] = json_decode($modelServicio->getAll());

        // Cargamos las vistas en orden
        echo view('templates/dashboard/header',$data);
        echo view($this->redireccionView.'/show',$data);
        echo view('templates/dashboard/footer',$data);
    }

    public function edit($id = "")
    {
        //Variable con todos los datos a pasar a las vistas
        $data = [];

        // Cargamos los helpers de formularios
        helper(['form']);
        $uri = service('uri');
        $modelPerm = new PermsModel();
        $roleModel = new RoleModel();
        $model = new SolicitudInterpretesModel();
        $modelTipoAcciones = new TipoAccionesModel();
        $modelServicio = new ServiciosModel();
        $modelSituacionesAcciones = new SitaccModel();
        $modelSituaciones = new SituacionesModel();
        $modelMotivos = new MotsitaccModel();
        $modelPersonas = new UserModel();
        $modelEdades = new RangoedadModel();
        $modelAsistentes = new AsistentesModel();
        $modelOnline = new ComunicacionModel();

        $perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
        $data['id'] = $id;

        if($id == "") {

            if($id == ""){
                // Creamos una session para mostrar el mensaje de denegación por permiso
                $session = session();
                $session->setFlashdata('error', 'No se ha seleccionado ningun elemento para editar');
                
                // Redireccionamos
                return redirect()->to(base_url()."/".$this->redireccion.'/show');
            }
        }

        if($perm[0]['edit'] == 0){
            // Creamos una session para mostrar el mensaje de denegación por permiso
            $session = session();
            $session->setFlashdata('error', 'No tienes permisos para editar esta sección');
            
            // Redireccionamos
            return redirect()->to(base_url()."/".$this->redireccion.'/show');
        }

        // Comprobamos el metodo de la petición
        if($this->request->getMethod() == 'post') {
        
            // reglas de validación
            $rules = [
                'grupal' => 'required',
                'solicitud' => 'required',
                'servicio' => 'required',
                'acciones' => 'required',
                'responsable' => 'required',
                'fecha' => 'required',
                'lugar' => 'required',
                'inicio' => 'required',
                'final' => 'required',
                'socio' => 'required',
                'facturar' => 'required',
            ];

            // Comprobación de las validaciones
            if(! $this->validate($rules)) {

                $newData = [
                    'grupal' => $this->request->getPost('grupal'),
                    'fecha_solicitud' => $this->request->getPost('solicitud'),
                    'id_persona' => $this->request->getPost('persona'),
                    'proyecto' => $this->request->getPost('proyecto'),
                    'id_servicio' => 6,
                    'id_acciones' => $this->request->getPost('acciones'),
                    'responsable' => $this->request->getPost('responsable'),
                    'fecha_accion' => $this->request->getPost('fecha'),
                    'lugar' => $this->request->getPost('lugar'),
                    'hora_inicio' => $this->request->getPost('inicio'),
                    'hora_final' => $this->request->getPost('final'),
                    'socios' => $this->request->getPost('socio'),
                    'facturar' => $this->request->getPost('facturar'),
                    'observaciones' => $this->request->getPost('observaciones'),
                    'id_online' => $this->request->getPost('online'),
                    'id_situaciones' => $this->request->getPost('situaciones'),
                    'id_motivo_situaciones' => $this->request->getPost('motivo')
                ];
                
                // Guardamos el error para mostrar en la vista
                $data['validation'] = $this->validator;

            } else {
                          
                // Acutlizar rol
                $newData = [
                    'grupal' => $this->request->getPost('grupal'),
                    'fecha_solicitud' => $this->request->getPost('solicitud'),
                    'proyecto' => $this->request->getPost('proyecto'),
                    'id_servicios' => 6,
                    'id_acciones' => $this->request->getPost('acciones'),
                    'responsable' => $this->request->getPost('responsable'),
                    'fecha_accion' => $this->request->getPost('fecha'),
                    'lugar' => $this->request->getPost('lugar'),
                    'hora_inicio' => $this->request->getPost('inicio'),
                    'hora_final' => $this->request->getPost('final'),
                    'asistentes' => $this->request->getPost('asistentes'),
                    'socios' => $this->request->getPost('socio'),
                    'facturar' => $this->request->getPost('facturar'),
                    'observaciones' => $this->request->getPost('observaciones'),
                    'id_online' => $this->request->getPost('online'),
                    'id_situaciones' => 1,
                    'id_motivo_situaciones' => 1
                ];

                //Guardamos
                $idAccion  = $model->save($newData);

                $asistencias = $modelAsistentes->asArray()->where('id_accion',$id);

                //Borramos todos los registros de asistentes y los volvemos a guardar
                foreach($asistencias as $asistencia){
                    $modelAsistentes->delete($asistencia['id']);
                }

                // En caso de que sea grupal guardamos los datos introducidos
                if($this->request->getPost('grupal') == 1){
                    if($this->request->getPost('boton_especifico') == 1){
                        foreach($this->request->getPost('sociosasis') as $item){
                            $newData = [
                                'hombres' => $this->request->getPost('hombres'),
                                'mujeres' => $this->request->getPost('mujeres'),
                                'id_rango' => $this->request->getPost('edad'),
                                'id_accion' => $idAccion,
                                'id_asistente' => $item,
                            ];
                            $modelAsistentes->insert($newData);

                        }
                    } else {
                        $newData = [
                            'hombres' => $this->request->getPost('hombres'),
                            'mujeres' => $this->request->getPost('mujeres'),
                            'id_rango' => $this->request->getPost('edad'),
                            'id_accion' => $idAccion,
                            'id_asistente' => 0,
                        ];

                        $modelAsistentes->insert($newData);

                    }
                    
                }

                // Creamos una session para mostrar el mensaje de registro correcto
                $session = session();
                $session->setFlashdata('success', 'Añadido correctamente');
                
                // Redireccionamos a la pagina
                return redirect()->to(base_url()."/".$this->redireccion.'/show');
            }

        }
        $data['data'] = json_decode($model->getByID($id));
        $data['servicios'] = json_decode($modelServicio->getAll());
        $data['acciones'] = json_decode($modelSituacionesAcciones->getAll());
        $data['situaciones'] = json_decode($modelSituaciones->getAll());
        $data['motivos'] = json_decode($modelMotivos->getAll());
        $data['personas'] = json_decode($modelPersonas->getAll());
        $data['socios'] = json_decode($modelPersonas->getSocios());
        $data['online'] = json_decode($modelOnline->getAll());
        $data['edades'] = json_decode($modelEdades->getAll());
        $data['socios'] = json_decode($modelPersonas->getSocios());
        $data['asistentes'] = json_decode($modelAsistentes->getAllByAccion($id));
        $asistentes = $modelAsistentes->asArray()->where('id_accion',$id)->findAll();
        $data['asistentesBool'] = 'false';
        $data['asistentesIDS'] = [];
        foreach($asistentes as $asistente){
            if($asistente['id_asistente']){
                $data['asistentesIDS'][] = $asistente['id_asistente'];
                $data['asistentesBool'] = 'true';
            }
            $data['hombres'] = $asistente['hombres'];
            $data['mujeres'] = $asistente['mujeres'];
            $data['edad'] = $asistente['id_rango'];
        }
        if($modelPerm->getPerms($id)){
            $data['perms'] = $modelPerm->getPerms($id);
        } else {
            $data['perms'] = [];
        }

        echo view('templates/dashboard/header',$data);
        echo view($this->redireccionView.'/edit',$data);
        echo view('templates/dashboard/footer',$data);
    }

    public function new($id = "")
    {
        //Variable con todos los datos a pasar a las vistas
        $data = [];

        // Cargamos los helpers de formularios
        helper(['form']);
        $uri = service('uri');
        $modelPerm = new PermsModel();
        $roleModel = new RoleModel();
        $model = new SolicitudInterpretesModel();
        $modelServicio = new ServiciosModel();
        $modelSituacionesAcciones = new SitaccModel();
        $modelSituaciones = new SituacionesModel();
        $modelMotivos = new MotivesiteModel();
        $modelPersonas = new UserModel();
        $modelEdades = new RangoedadModel();
        $modelAsistentes = new AsistentesModel();
        $modelOnline = new ComunicacionModel();

        $perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
        
        if($perm[0]['edit'] == 0){
            // Creamos una session para mostrar el mensaje de denegación por permiso
            $session = session();
            $session->setFlashdata('error', 'No tienes permisos para editar esta sección');
            
            // Redireccionamos
            return redirect()->to(base_url()."/".$this->redireccion.'/show');
        }

        // Comprobamos el metodo de la petición
        if($this->request->getMethod() == 'post') {
        
            // reglas de validación
            $rules = [
                'grupal' => 'required',
                'solicitud' => 'required',
                'servicio' => 'required',
                'acciones' => 'required',
                'responsable' => 'required',
                'fecha' => 'required',
                'lugar' => 'required',
                'inicio' => 'required',
                'final' => 'required',
                'socio' => 'required',
                'facturar' => 'required',
            ];

            // Comprobación de las validaciones
            if(! $this->validate($rules)) {

                $newData = [
                    'grupal' => $this->request->getPost('grupal'),
                    'fecha_solicitud' => $this->request->getPost('solicitud'),
                    'id_persona' => $this->request->getPost('persona'),
                    'proyecto' => $this->request->getPost('proyecto'),
                    'id_servicio' => 6,
                    'id_acciones' => $this->request->getPost('acciones'),
                    'responsable' => $this->request->getPost('responsable'),
                    'fecha_accion' => $this->request->getPost('fecha'),
                    'lugar' => $this->request->getPost('lugar'),
                    'inicio' => $this->request->getPost('inicio'),
                    'final' => $this->request->getPost('final'),
                    'socios' => $this->request->getPost('socio'),
                    'facturar' => $this->request->getPost('facturar'),
                    'observaciones' => $this->request->getPost('observaciones'),
                    'id_online' => $this->request->getPost('online'),
                    'id_situaciones' => 1,
                    'id_motivo_situaciones' => 1
                ];
                
                // Guardamos el error para mostrar en la vista
                $data['validation'] = $this->validator;

            } else {
                          
                // Acutlizar rol
                $newData = [
                    'grupal' => $this->request->getPost('grupal'),
                    'fecha_solicitud' => $this->request->getPost('solicitud'),
                    'proyecto' => $this->request->getPost('proyecto'),
                    'id_servicios' => 6,
                    'id_acciones' => $this->request->getPost('acciones'),
                    'responsable' => $this->request->getPost('responsable'),
                    'fecha_accion' => $this->request->getPost('fecha'),
                    'lugar' => $this->request->getPost('lugar'),
                    'hora_inicio' => $this->request->getPost('inicio'),
                    'hora_final' => $this->request->getPost('final'),
                    'asistentes' => $this->request->getPost('asistentes'),
                    'socios' => $this->request->getPost('socio'),
                    'facturar' => $this->request->getPost('facturar'),
                    'observaciones' => $this->request->getPost('observaciones'),
                    'id_online' => $this->request->getPost('online'),
                    'id_situaciones' => 1,
                    'id_motivo_situaciones' => 1
                ];

                //Guardamos
                $idAccion  = $model->insert($newData);

                // En caso de que sea grupal guardamos los datos introducidos
                if($this->request->getPost('grupal') == 1){
                    if($this->request->getPost('boton_especifico') == 1){
                        foreach($this->request->getPost('sociosasis') as $item){
                            $newData = [
                                'hombres' => $this->request->getPost('hombres'),
                                'mujeres' => $this->request->getPost('mujeres'),
                                'id_rango' => $this->request->getPost('edad'),
                                'id_accion' => $idAccion,
                                'id_asistente' => $item,
                            ];
                            $modelAsistentes->insert($newData);

                        }
                    } else {
                        $newData = [
                            'hombres' => $this->request->getPost('hombres'),
                            'mujeres' => $this->request->getPost('mujeres'),
                            'id_rango' => $this->request->getPost('edad'),
                            'id_accion' => $idAccion,
                            'id_asistente' => 0,
                        ];

                        $modelAsistentes->insert($newData);

                    }
                    
                }

                // Creamos una session para mostrar el mensaje de registro correcto
                $session = session();
                $session->setFlashdata('success', 'Añadido correctamente');
                
                // Redireccionamos a la pagina
                return redirect()->to(base_url()."/".$this->redireccion.'/show');
            }

        }

        $data['servicios'] = json_decode($modelServicio->getAll());
        $data['acciones'] = json_decode($modelSituacionesAcciones->getAll());
        $data['situaciones'] = json_decode($modelSituaciones->getAll());
        $data['motivos'] = json_decode($modelMotivos->getAll());
        $data['personas'] = json_decode($modelPersonas->getAll());
        $data['socios'] = json_decode($modelPersonas->getSocios());
        $data['online'] = json_decode($modelOnline->getAll());
        $data['edades'] = json_decode($modelEdades->getAll());

        if($modelPerm->getPerms($id)){
            $data['perms'] = $modelPerm->getPerms($id);
        } else {
            $data['perms'] = [];
        }

        echo view('templates/dashboard/header',$data);
        echo view($this->redireccionView.'/new',$data);
        echo view('templates/dashboard/footer',$data);
    }

    // Borrar
	public function delete($id)
	{
        $modelPerm = new PermsModel();
        $modelSituaciones = new SitaccModel();
        $modelMotivos = new MotsitaccModel();
        $model = new AccionesModel();

		$perm = $modelPerm->getPerms(session()->get('role'),'role');

		if($perm[0]['delete'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para borrar en esta sección');
			
			// Redireccionamos
			return redirect()->to(base_url()."/".$this->redireccion.'/show');
		}

        // Comprobamos el metodo de la petición
        if($this->request->getMethod() == 'post') {
        
            // Comprobación de las validaciones
                          
                // Acutlizar rol
                $newData = [
                    'id' => $id,
                    'id_situaciones' => $this->request->getPost('situaciones'),
                    'id_motivo_situaciones' => $this->request->getPost('motivo')
                ];

                //Guardamos
                $model->save($newData);

                // Creamos una session para mostrar el mensaje de registro correcto
                $session = session();
                $session->setFlashdata('success', 'Cancelada acción correctamente');
                
                // Redireccionamos a la pagina
                return redirect()->to(base_url()."/".$this->redireccion.'/show');
            }

        $data['data'] = json_decode($model->getByID($id));
        $data['situaciones'] = json_decode($modelSituaciones->getAll());
        $data['motivos'] = json_decode($modelMotivos->getAll());
        $data['id'] = $id;
		
        echo view('templates/dashboard/header',$data);
        echo view($this->redireccionView.'/delete',$data);
        echo view('templates/dashboard/footer',$data);

    }

    function fulldelete($id){
        $model = new AccionesModel();
        $modelAsistentes = new AsistentesModel();
        $asistentes = $modelAsistentes->asArray()->where('id_accion',$id)->findAll();

        if($asistentes){
            foreach($asistentes as $asistente){
                $modelAsistentes->delete($asistente['id']);
            }
        }

        $model->delete($id);


        if($asistentes){
            // Creamos una session para mostrar el mensaje de registro correcto
            $session = session();
            $session->setFlashdata('success', 'Eliminada acción correctamente y datos de los asistentes');
        } else {
            // Creamos una session para mostrar el mensaje de registro correcto
            $session = session();
            $session->setFlashdata('success', 'Eliminada acción correctamente');
        }
        
        // Redireccionamos a la pagina
        return redirect()->to(base_url()."/".$this->redireccion.'/show');

    }

    function formulario(){
//Variable con todos los datos a pasar a las vistas
        $data = [];

        // Cargamos los helpers de formularios
        helper(['form']);
        $uri = service('uri');
        $modelPerm = new PermsModel();
        $roleModel = new RoleModel();
        $model = new SolicitudInterpretesModel();
        $modelServicio = new ServiciosModel();
        $modelSituacionesAcciones = new SitaccModel();
        $modelSituaciones = new SituacionesModel();
        $modelMotivos = new MotivesiteModel();
        $modelPersonas = new UserModel();
        $modelEdades = new RangoedadModel();
        $modelMotivosSilse = new MotivosilseModel();
        $modelOnline = new ComunicacionModel();
        
        $perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
        
        if(session('IsLoggedIn') == 1){
            // Creamos una session para mostrar el mensaje de denegación por permiso
            $session = session();
            $session->setFlashdata('error', 'Debes estár registrado para solicitar interprete');
            
            // Redireccionamos
            return redirect()->to(base_url()."/".$this->redireccion.'/show');
        }

        // Comprobamos el metodo de la petición
        if($this->request->getMethod() == 'post') {
        
            // reglas de validación
            $rules = [
                'grupal' => 'required',
                'fecha' => 'required',
                'lugar' => 'required',
                'inicio' => 'required',
                'final' => 'required'
            ];

            // Comprobación de las validaciones
            if(! $this->validate($rules)) {

                $newData = [
                    'grupal' => $this->request->getPost('grupal'),
                    'fecha_solicitud' => now(),
                    'id_persona' => session('id'),
                    'proyecto' => $this->request->getPost('proyecto'),
                    'id_servicio' => 6,
                    'id_acciones' => 1,
                    'responsable' => NULL,
                    'fecha_accion' => $this->request->getPost('fecha'),
                    'lugar' => $this->request->getPost('lugar'),
                    'hoinicio' => $this->request->getPost('inicio'),
                    'final' => $this->request->getPost('final'),
                    'socios' => $this->request->getPost('socio'),
                    'facturar' => $this->request->getPost('facturar'),
                    'observaciones' => $this->request->getPost('observaciones'),
                    'id_online' => $this->request->getPost('online'),
                    'id_situaciones' => 1,
                    'id_motivo_situaciones' => 1
                ];
                
                // Guardamos el error para mostrar en la vista
                $data['validation'] = $this->validator;

            } else {
                          
                // Acutlizar rol
                $newData = [
                    'grupal' => 0,
                    'fecha_solicitud' => now(),
                    'proyecto' => "",
                    'id_servicios' => 6,
                    'id_persona' => session('id'),
                    'id_acciones' => 1,
                    'responsable' => 1,
                    'fecha_accion' => $this->request->getPost('fecha'),
                    'lugar' => $this->request->getPost('lugar'),
                    'hora_inicio' => $this->request->getPost('inicio'),
                    'hora_final' => $this->request->getPost('final'),
                    'socios' => 1,
                    'observaciones' => $this->request->getPost('observaciones'),
                    'id_online' => $this->request->getPost('online'),
                    'id_situaciones' => 1,
                    'id_motivo_situaciones' => 1,
                    'id_motivo_silse' => $this->request->getPost('motivossilse')
                ];

                //Guardamos
                $idAccion  = $model->insert($newData);

                // Creamos una session para mostrar el mensaje de registro correcto
                $session = session();
                $session->setFlashdata('success', 'Petición solicitada correctamente');
                
                // Redireccionamos a la pagina
                return redirect()->to(base_url()."/forminterpretes");
            }

        }

        $data['servicios'] = json_decode($modelServicio->getAll());
        $data['acciones'] = json_decode($modelSituacionesAcciones->getAll());
        $data['situaciones'] = json_decode($modelSituaciones->getAll());
        $data['motivos'] = json_decode($modelMotivos->getAll());
        $data['personas'] = json_decode($modelPersonas->getAll());
        $data['socios'] = json_decode($modelPersonas->getSocios());
        $data['online'] = json_decode($modelOnline->getAll());
        $data['edades'] = json_decode($modelEdades->getAll());
        $data['motivossilse'] = json_decode($modelMotivosSilse->getAll());

        echo view('templates/dashboard/header_formularios',$data);
        echo view($this->redireccionView.'/formulario',$data);
        echo view('templates/dashboard/footer',$data);
    }

}