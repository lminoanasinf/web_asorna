<?php namespace App\Controllers;

use App\Models\UserModel;
use App\Models\LogEmailModel;
use App\Models\TokenRecoverModel;
use App\Models\MinusvaliaModel;
use App\Models\SorderaModel;
use App\Models\HistorialModel;

use \DateTime;

class Login extends BaseController
{

	// Pantalla y sistema de inicio de sesión
	public function index()
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {

			// reglas de validación
			$rules = [
				'email' => 'required|min_length[6]|max_length[100]|valid_email',
				'password' => 'required|min_length[8]|max_length[255]|validateUser[email,password]'
			];

			//  Errores a mostrar
			$errors = [
				'password' => [
					'validateUser' => 'Usuario y/o contraseña incorrectos'
				]
			];

			// Comprobación de las validaciones
			if(! $this->validate($rules,$errors)) {

				// Guardamos el error para mostrar en la vista
				$data['validacion'] = $this->validator;

			} else {
				//Guardar en BBDD
				$model = new UserModel();
				
				//Pedimos los datos de ese usuario
				$user = $model->where('email', $this->request->getVar('email'))
								->first();

				//Asignamos el usuario a la sesión
				$this->setUserSession($user);

				// Redireccionamos a la pagina de login
				return redirect()->to('dashboard');

			}
		}

		// Cargamos las vistas en orden
		echo view('templates/login/header',$data);
		echo view('templates/login/login',$data);
		echo view('templates/login/footer',$data);
	}

	//Guardar datos de usuario en la sesión
	private function setUserSession($user)
	{
		$data = [
			'id' => $user['id'],
			'role' => $user['role_id'],
			'email' => $user['email'],
			'name' => $user['name'],
			'lastname' => $user['lastname'],
			'isLoggedIn' => true
		];

		session()->set($data);
	}

	// Función para registrarse
	public function register()
	{

		$model = new UserModel();
		$minusvaliaModel = new MinusvaliaModel();
		$sorderaModel = new SorderaModel();
		$historialModel = new HistorialModel();

		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
                
        // Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {
			// reglas de validación
			$rules = [
				'name' => 'required|min_length[3]|max_length[100]',
				'lastname' => 'required|min_length[3]|max_length[100]',
				'lastlastname' => 'required|min_length[3]|max_length[100]',
				'email' => 'required|min_length[6]|max_length[100]|is_unique[tbl_usuarios.email]',
				'password' => 'required|min_length[8]|max_length[255]',
				'password_confirm' => 'matches[password]'
			];
			// Comprobación de las validaciones
			if(! $this->validate($rules)) {
                $newData = [
					'name' => $this->request->getVar('name'),
					'lastname' => $this->request->getVar('lastname'),
					'lastlastname' => $this->request->getVar('lastlastname'),
					'email' => $this->request->getVar('email'),
					'password' => $this->request->getVar('password')
                ];
				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;
			} else {
				//Guardar en BBDD
	
				$newData = [
					'name' => $this->request->getVar('name'),
					'lastname' => $this->request->getVar('lastname'),
					'email' => $this->request->getVar('email'),
					'password' => $this->request->getVar('password')
				];
				
				//Guardamos
				$userid = $model->insert($newData);
				
				$historial = [
					'fecha'  => date('now'),
					'id_user' => $userid,
					'id_situacion' => 1,
					'motivo' => ''
				];

				$historialModel->insert($historial);

				$sordera = [
					'tipo_sordera' => "Ninguna",
					'causa_sordera' => "",
					'ayuda_tecnica' => "",
					'nivel_estudios' => "",
					'centro_estudios' => "",
					'comunicacion' => "",
					'id_user' => $userid,
				];

				$sorderaModel->insert($sordera);

				$minusvalia = [
					'minusvalia' => "",
					'porcentaje' => 0,
					'motivo' => "",
					'id_user' => $userid,
				];

				$minusvaliaModel->insert($minusvalia);
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Registrado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/');
			}
		}

		// Cargamos las vistas de registro en orden
		echo view('templates/login/header',$data);
		echo view('templates/login/register',$data);
		echo view('templates/login/footer',$data);
	}

	// Carga y procesamiento de datos del perfil de usuario
	public function profile() 
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$model = new UserModel();

        // Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {
			// reglas de validación
			$rules = [
				'name' => 'required|min_length[3]|max_length[100]',
				'lastname' => 'required|min_length[3]|max_length[100]',
				'lastlastname' => 'required|min_length[3]|max_length[100]',
			];

			if($this->request->getPost('password') != ''){
				$rules['password'] = 'required|min_length[8]|max_length[255]';
				$rules['password_confirm'] = 'matches[password]';
			}

			// Comprobación de las validaciones
			if(! $this->validate($rules)) {

                $newData = [
					'name' => $this->request->getVar('name'),
					'lastname' => $this->request->getVar('lastname'),
					'lastlastname' => $this->request->getVar('lastlastname')
				];
				
				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;

			} else {
	
				$newData = [
					'id' => session()->get('id'),
					'name' => $this->request->getPost('name'),
					'lastname' => $this->request->getPost('lastname'),
					'lastlastname' => $this->request->getVar('lastlastname')
				];
				
				if($this->request->getPost('password') != ''){
					$newData['password'] = $this->request->getPost('password');
				}

				//Guardamos
				$model->save($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to('/profile');
			}
		}

		$data['user'] = $model->where('id',session()->get('id'))
							  ->first();

		// Cargamos las vistas de registro en orden
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/profile',$data);
		echo view('templates/dashboard/footer',$data);
	}

	// Carga de página y procesamiento de contraseña olvidada
	public function forgotOld()
	{

		$data = [];

		//Cargamos los helpers de formularios
		helper('form');
		helper('email');
		

		//Comprobamos si es una petición
		if($this->request->getMethod() == 'post') {

			// reglas de validación
			$rules = [
				'email' => 'required|min_length[6]|max_length[100]|valid_email|validateEmail[email]',
			];

			//  Errores a mostrar
			$errors = [
				'email' => [
					'validateEmail' => 'Email no registrado en el sistema'
				]
			];
			
			// Comprobamos la validación y errores
			if(!$this->validate($rules,$errors)) {

				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;

			} else {
				$modelemail = new LogEmailModel();
				$user = new UserModel();
				$token = md5(uniqid(Rand(), true));

				$dataToken = [
					'password' => $token
				];
				
				$dataUser = $user->where('email',$this->request->getVar('email'))->first();
				$idUser = $dataUser['id'];

				//Guardar en BBDD
				$resp = $user->update($idUser,$dataToken);
				
				// $message = view('template/email/emailForgot',$data,true);
				$message = emailForgot($dataToken['password']);
				$emailTo = $this->request->getVar('email');

				sendEmailPass($emailTo,$message,$token);

				$newData = [
					'email_from' => 'noreply@asorna.org',
					'email_to' => $this->request->getVar('email'),
					'object' => 'ASORNA - Restaurar contraseña',
					'description' => emailForgot($token),
				];
				
				$modelemail->insert($newData);

				$session = session();
				$session->setFlashdata('success', 'Correo enviado');
				

				// Redireccionamos a la pagina de login
				return redirect()->to('forgot');

			}

		}

		// Cargamos las vistas de registro en orden
		echo view('templates/login/header',$data);
		echo view('templates/login/forgot',$data);
		echo view('templates/login/footer',$data);

	}

	// Carga de página y procesamiento de contraseña olvidada
	public function forgot()
	{

		$data = [];

		//Cargamos los helpers de formularios
		helper('form');
		helper('email');
		

		//Comprobamos si es una petición
		if($this->request->getMethod() == 'post') {

			// reglas de validación
			$rules = [
				'email' => 'required|min_length[6]|max_length[100]|valid_email|validateEmail[email]',
			];

			//  Errores a mostrar
			$errors = [
				'email' => [
					'validateEmail' => 'Email no registrado en el sistema'
				]
			];
			
			// Comprobamos la validación y errores
			if(!$this->validate($rules,$errors)) {

				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;

			} else {
				// Declaramos los modelos necesarios
				$modelemail = new LogEmailModel();
				$userModel = new UserModel();
				$tokenModel = new TokenRecoverModel();

				//Generamos un token md5 aleatorio
				$token = md5(uniqid(Rand(), true));

				//Comprobamos que no existe, de lo contrario lo volvemos a generar hasta que no haya coincidencias
				$tokenCheck = false;

				//Generamos un token hasta que no exista en la bbdd
				do {
					$respond = $tokenModel->where('token',$token)->first();
					if($respond == NULL){
						$tokenCheck = true;
					} else {
						$token = md5(uniqid(Rand(), true));
					}
				} while (!$tokenCheck);
				
				$dataUser = $userModel->where('email',$this->request->getVar('email'))->first();

				$dateNow = new DateTime();
				$createdAt = $dateNow->format("Y-m-d H:i:s");
				$dateCheck = $dateNow->modify('5 minutes');
				$deletedAt = $dateCheck->format("Y-m-d H:i:s");


				$idUser = $dataUser['id'];
				$dataTokenInsert = [
					'token' => $token,
					'user_id' => $idUser,
					'deleted_at' =>	$deletedAt
				];

				//Guardar en BBDD
				$resp = $tokenModel->insert($dataTokenInsert);
				
				// $message = view('template/email/emailForgot',$data,true);
				$message = emailForgot($token,$idUser);
				$emailTo = $this->request->getVar('email');

				sendEmailPass($emailTo,$message,$token,$idUser);

				$newData = [
					'email_from' => 'noreply@asorna.org',
					'email_to' => $this->request->getVar('email'),
					'object' => 'ASORNA - Restaurar contraseña',
					'description' => emailForgot($token),
				];
				
				$modelemail->insert($newData);

				$session = session();
				$session->setFlashdata('success', 'Correo enviado');
				

				// Redireccionamos a la pagina de login
				return redirect()->to('forgot');

			}

		}

		// Cargamos las vistas de registro en orden
		echo view('templates/login/header',$data);
		echo view('templates/login/forgot',$data);
		echo view('templates/login/footer',$data);

	}

	// Carga de página y procesamiento de token con cambio de contraseña
	public function recovery($token = "")
	{


		//Adquirimos el token con el id y los separamos del url
		$decoder = explode("@",$token);
		$idUrl = $decoder[0];
		$tokenUrl = $decoder[1];

		//Declaramos los modelos a usar
		$model = new UserModel();
		$tokenModel = new TokenRecoverModel();

		//Comprobación de token valido
		$resp = $tokenModel->where('token',$tokenUrl)
		->first();
		
		$data = [
			'token' => $token
		];


		$position = strpos($token,"@");

		//Comprobamos que el token tiene el formato correcto
		if(!$position){

			$session = session();
			$session->setFlashdata('error', 'Formato invalido en el token de seguridad');
			return redirect()->to(base_url()."/".$token);
		}


		if($resp['used'] != "0") {

			$session = session();

			$session->setFlashdata('error', 'Este enlace ya ha sido usado, tiene que solitar uno nuevo');

			return redirect()->to(base_url()."/forgot");
		}

		//Cargamos los helpers de formularios
		helper('form');

		if($this->request->getMethod() == 'post') {
			
			//Comprobación de token valido
			$resp = $tokenModel->where('token',$tokenUrl)
							   ->first();

			$deletedAt = new DateTime($resp['deleted_at']);
			$createdAt = new DateTime($resp['created_at']);
			$nowAt = new DateTime();

			if($resp['used'] != "0") {
				$session = session();
				$session->setFlashdata('error', 'Este enlace ya ha sido usado, tiene que solitar uno nuevo');
				return redirect()->to('/recovery'."/".$token);
			}

			// Diferencia entre la fecha de creación y de borrado del token
			$difference = $deletedAt->diff($createdAt);

			//Diferencia entre la hora actual y la hora de borrado del token
			$check = $deletedAt->diff($nowAt);

		

			//Si coincide la id de usuario enviada con la del token en la bbdd se emite mensaje de error
			if($idUrl != $resp['user_id']){

				$session = session();
				$session->setFlashdata('error', 'No estás autorizado para el cambio de contraseña de esta cuenta');
				
				// Redireccionamos a la pagina de login
				return redirect()->to('/recovery'."/".$token);
			} 

			//Comprobamos que y(año) no sean más que la hora de eliminación
			if($check->y > $difference->y){
				$session = session();
				$session->setFlashdata('error', 'Se ha expirado el tiempo para el cambio de contraseña ');
				return redirect()->to('/forgot'."/".$token);
			}
			//Comprobamos que m(mes) no sean más que la hora de eliminación
			if($check->m > $difference->m){
				$session = session();
				$session->setFlashdata('error', 'Se ha expirado el tiempo para el cambio de contraseña ');
				return redirect()->to('/forgot'."/".$token);
			}
			//Comprobamos que d(día) no sean más que la hora de eliminación
			if($check->d > $difference->d){
				$session = session();
				$session->setFlashdata('error', 'Se ha expirado el tiempo para el cambio de contraseña ');
				return redirect()->to('/forgot'."/".$token);
			}
			//Comprobamos que h(hora) no sean más que la hora de eliminación
			if($check->h > $difference->h){
				$session = session();
				$session->setFlashdata('error', 'Se ha expirado el tiempo para el cambio de contraseña ');
				return redirect()->to('/forgot'."/".$token);
			}
			//Comprobamos que i(minutos) no sean más que la hora de eliminación
			if((int)$check->i > $difference->i){
				$session = session();
				$session->setFlashdata('error', 'Se ha expirado el tiempo para el cambio de contraseña ');
				return redirect()->to('/forgot'."/".$token);
			}
			
			if($resp['used'] != "0") {
				$session = session();
				$session->setFlashdata('error', 'Este enlace ya ha sido usado, tiene que solitar uno nuevo');
				return redirect()->to('/forgot'."/".$token);
			}

			// reglas de validación
			$rules = [
				'password' => 'required|min_length[8]|max_length[255]',
				'password_confirm' => 'matches[password]'
			];


			// Comprobación de las validaciones
			if(! $this->validate($rules)) {

                $newData = [
					'password' => $this->request->getVar('password')
				];
				
				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;

				// Cargamos las vistas de registro en orden
				echo view('templates/login/header',$data);
				echo view('templates/login/recovery',$data);
				echo view('templates/login/footer',$data);

			} else {
				//Guardar en BBDD
				
				$newData = [
					'password' => $this->request->getVar('password')
				];
				$newDataToken = [
					'used' => 1
				];
				
				//Guardamos
				$model->update($resp['user_id'],$newData);
				$tokenModel->update($tokenUrl,$newDataToken);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Cambio de contraseña realizado con exito');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/');
			}
		
		}
		
		// Cargamos las vistas de registro en orden
		echo view('templates/login/header',$data);
		echo view('templates/login/recovery',$data);
		echo view('templates/login/footer',$data);
	}

	// Sistema de desconexión
	public function logout()
	{
		session()->destroy();
		return redirect()->to(base_url()."/");
	}

}