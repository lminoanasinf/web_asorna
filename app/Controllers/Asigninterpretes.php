<?php namespace App\Controllers;

use App\Models\UserModel;
use App\Models\RoleModel;
use App\Models\PermsModel;
use App\Models\ApartadoModel;
use App\Models\AsigninterpretesModel;
use App\Models\AccionesModel;
use App\Models\SolicitudInterpretesModel;

class Asigninterpretes extends BaseController
{
	public function index()
	{
		$data = [];

		
		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view('templates/dashboard/dashboard',$data);
		echo view('templates/dashboard/footer',$data);
	}

	// Ver
	public function show()
	{

		helper(['form']);
		$uri = service('uri');
		$modelPerm = new PermsModel();
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['show'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para ver esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to('/dashboard');
		}
		
		$data = [];
		$col =[];
		$model = new AsigninterpretesModel();

		if($model->getAll() != "[]"){
			$col = json_decode($model->getAll());
		} else {
			$columns = json_decode($model->getColumns());
			foreach($columns as $column){
				$col[0][strtoupper($column->Field)] =  "";
			}
		}
		$data['columns'] = $col;
		
		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view('maintence/asignacion/show',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function edit($id = "")
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$modelPerm = new PermsModel();
		$model = new AsigninterpretesModel();
		$modelsilse = new AccionesModel();
		$modelilse = new UserModel();
		$modelSoli = new SolicitudInterpretesModel();

		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
		$solicitud = $modelSoli->asArray()->where('id',(int)$this->request->getPost('silse'))->first();
		if($id == "") {
			// $id = $this->request->getVar('id');

			if($id == ""){
				// Creamos una session para mostrar el mensaje de denegación por permiso
				$session = session();
				$session->setFlashdata('error', 'No se ha seleccionado ningun elemento para editar');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/asigninterpretes/show');
			}
		}
		if($id != ""){
			$data['id'] = $id;

		}

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/asigninterpretes/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {
		

			// reglas de validación
			$rules = [
				'ilse' => 'required',
				'silse' => 'required'
			];

			// Comprobación de las validaciones
			if(! $this->validate($rules)) {

				$newData = [
					'ilse' => $this->request->getVar('ilse'),
					'silse' => $this->request->getVar('silse')
				];
				
				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;

			} else {

				// Acutlizar rol
				$newData = [
					'id' => $id,
					'ilse' => $this->request->getPost('ilse'),
					'silse' => (int)$this->request->getPost('silse')
				];

				$model->save($newData);
				$this->push_notification_android($solicitud['id_persona']);

				// Actualizar permisos

					//Guardamos
					$modelPerm->save($newData);

				}
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/asigninterpretes/edit/'.$id);
		}


		$data['data'] = json_decode($model->getByID($id));
		$data['ilses'] = $modelilse->where('role_id',5)->findAll();
		$data['silses'] = $modelsilse->where('id_servicios',6)->findAll();

		if($modelPerm->getPerms($id)){
		$data['perms'] = $modelPerm->getPerms($id);

		} else {
		$data['perms'] = [];

		}

			
		echo view('templates/dashboard/header',$data);
		echo view('maintence/asignacion/edit',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function new($id ="")
	{
				//Variable con todos los datos a pasar a las vistas
				$data = [];

				if($id != ""){
					$data['id'] = $id;
		
				}
				// Cargamos los helpers de formularios
				helper(['form']);
				$uri = service('uri');
				$modelPerm = new PermsModel();
				$model = new AsigninterpretesModel();
				$modelsilse = new AccionesModel();
				$modelilse = new UserModel();
				$modelSoli = new SolicitudInterpretesModel();
		
				$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
				
				if($perm[0]['create'] == 0){
					// Creamos una session para mostrar el mensaje de denegación por permiso
					$session = session();
					$session->setFlashdata('error', 'No tienes permisos para crear en esta sección');
					
					// Redireccionamos a la pagina de login
					return redirect()->to(base_url().'/asigninterpretes/show');
				}
		
				// Comprobamos el metodo de la petición
				if($this->request->getMethod() == 'post') {
		$solicitud = $modelSoli->asArray()->where('id',(int)$this->request->getPost('silse'))->first();

		
					// reglas de validación
					$rules = [
						'silse' => 'required',
						'ilse' => 'required'
					];
		
					// Comprobación de las validaciones
					if(! $this->validate($rules)) {
		
						$newData = [
							'silse' => $this->request->getVar('silse'),
							'ilse' => $this->request->getVar('ilse')
						];
						
						// Guardamos el error para mostrar en la vista
						$data['validation'] = $this->validator;
		
					} else {
		
						// Acutlizar rol
						$newData = [
							'ilse' => $this->request->getPost('ilse'),
							'silse' => (int)$this->request->getPost('silse')
						];

						if($model->where('ilse',$this->request->getPost('ilse'))->where('silse',$this->request->getPost('silse'))->findAll() == NULL){
							$idReturn = $model->insert($newData);
							$this->push_notification_android($solicitud['id_persona']);

							
						// //API URL
							// $url = base_url().'/api/push_notification_android';
							// //create a new cURL resource
							// $ch = curl_init($url);
							// //setup request to send json via POST
							// $data = array(
							// 	'username' => 'codexworld',
							// 	'password' => '123456'
							// );
							// $payload = json_encode(array("user" => $data));
							// //attach encoded JSON string to the POST fields
							// curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
							// //set the content type to application/json
							// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
							// //return response instead of outputting
							// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							// //execute the POST request
							// $result = curl_exec($ch);
							// //close cURL resource
							// curl_close($ch);

						
						} else {
							// Creamos una session para mostrar el mensaje de denegación por permiso
							$session = session();
							$session->setFlashdata('error', 'Ya existe un registro con esa descripción');
							
							// Redireccionamos a la pagina de login
							return redirect()->to(base_url().'/asigninterpretes/new');
						}
							
						// Creamos una session para mostrar el mensaje de registro correcto
						$session = session();
						$session->setFlashdata('success', 'Actualizado correctamente');
						
						// Redireccionamos a la pagina de login
						return redirect()->to(base_url().'/asigninterpretes/show');
					}
		
				}
		
		
				$data['ilses'] = $modelilse->where('role_id',5)->findAll();
				$silses = $modelsilse->where('id_servicios',6)->findAll();
				$asignadas = $model->findAll();
				

				if($asignadas){
					$arrayAsignadas = [];
					$arrayNoAsignadas = [];
					foreach($asignadas as $asignada){
						$arrayAsignadas[] = $asignada['silse'];
					}

					foreach($silses as $accion){
						if(array_search($accion['id'],$arrayAsignadas) === false){
							$arrayNoAsignadas[] = $accion;
						}
					}
					$data['silses'] = $arrayNoAsignadas;
				} else {
					$data['silses'] = $silses;
				}

				$data['id'] = $id;

				echo view('templates/dashboard/header',$data);
				echo view('maintence/asignacion/new',$data);
				echo view('templates/dashboard/footer',$data);
	}

	// Borrar
	public function delete($id)
	{
		$modelPerm = new PermsModel();
		$perm = $modelPerm->getPerms(session()->get('role'),'role');

		if($perm[0]['delete'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para borrar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to('/role/show');
		}

		$modelUser = new UserModel();
		$users = $modelUser->where('role_id',$id)->findAll();

		if($users){
			// Creamos una session para mostrar el mensaje de error
			$session = session();
			$session->setFlashdata('error', 'No puedes eliminar un Tipo que contenga usuarios');
			return redirect()->to('/role/show');
		} 

		$model = new RoleModel();
		$answer = $model->delete($id);
		// Creamos una session para mostrar el mensaje de registro correcto
		$session = session();
		$session->setFlashdata('success', 'Eliminado correctamente');

		// Redireccionamos a la pagina de login
		return redirect()->to(base_url().'/role/show/');

	}

	function push_notification_android($id = ""){
		$model = new UserModel();

		#API access key from Google API's Console
		define( 'API_ACCESS_KEY', 'AAAAIaqjsDA:APA91bEuezewjMygfn2HrHHKnIP1k8AiWROfFDrKqH1xZ58oBwiqDAm7tghPsFCO0SmuWgV69MkgLBBGBoqojTUsTgH4mWkQo3A_NGapYbtmyM4AQMvQvANQVLh44bKVHuXglWZ8KIyP' );
		// $registrationIds = $_GET['id'];

		$user = $model->where("id",$id)->first();
		$device = $user['device'];
	#prep the bundle
		$msg = array
			(
			'body' 	=> 'Se ha asignado una interprete a su solicitud',
			'title'	=> 'ASORNA - Notificacion',
					'icon'	=> 'myicon',/*Default Icon*/
					'sound' => 'mySound',/*Default sound*/
					"badge" => 1
			);

			if($device == "iOS"){
				$fields = array
				(
					'to'		=> $user['device_token'],
					'payload' => [
						'aps' => [
							'alert' => [
								'title' => 'ASORNA - Notificacion',
								'body' => 'Se ha asignado una interprete a su solicitud',
							],
							'badge' => 1,
							'sound' => 'default',
						],
					]
				);
			} else {
				$fields = array
				(
					'to'		=> $user['device_token'],
					'data'	=> $msg
				);
			}
		
		$headers = array
				(
					'Authorization: key=' . API_ACCESS_KEY,
					'Content-Type: application/json'
				);

	#Send Reponse To FireBase Server	
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch );

	#Echo Result Of FireBase Server
	echo $result;
	}

}