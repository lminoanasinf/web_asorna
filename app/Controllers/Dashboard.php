<?php namespace App\Controllers;

use App\Models\UserModel;
use App\Models\ApartadoModel;

class Dashboard extends BaseController
{
	public function index()
	{
		$data = [];

		// $modelSection = new UserSection();
		$modelApartado = new ApartadoModel();
									
		// Consultamos todos los apartados con los permismos según usuario
		$data['mantenimiento'] = json_decode($modelApartado->getMaintenanceByRole(session()->get('role')));
	
		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view('templates/dashboard/dashboard',$data);
		echo view('templates/dashboard/footer',$data);
	}

}
