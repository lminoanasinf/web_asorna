<?php namespace App\Controllers;

use App\Models\UserModel;
use App\Models\RoleModel;
use App\Models\PermsModel;
use App\Models\RecursoUserModel;
use App\Models\RecursossocialesModel;
use App\Models\AutonomiapersonalModel;
use App\Models\DatoBancoModel;
use App\Models\HistorialModel;
use App\Models\ContactosModel;
use App\Models\TipocontactoModel;
use App\Models\GrupoUsuarioModel;
use App\Models\GrupopertenenciaModel;
use App\Models\MinusvaliaModel;
use App\Models\SorderaModel;
use App\Models\AccionesModel;
use App\Models\UserAutonomiaModel;


class User extends BaseController
{
	public function index()
	{
		$data = [];

		
		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view('templates/dashboard/dashboard',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function show()
	{

		$uri = service('uri');
		$modelPerm = new PermsModel();
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['show'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para ver esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/dashboard');
		}

		$data = [];
		$model = new UserModel();

		$data['columns'] = json_decode($model->getAll());

		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/show',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function edit($id = "")
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserModel();
		$modelRole = new RoleModel();
		$modelPerm = new PermsModel();
		$modelRecursos = new RecursoUserModel();
		$modelAutonomiaUser = new AutonomiapersonalModel();
		$modelDatoBanco = new DatoBancoModel();
		$historialModel = new HistorialModel();
		$contactosModel = new ContactosModel();
		$modelGruposUsuario = new GrupoUsuarioModel();
		$modelAcciones = new AccionesModel();

		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		$data['roles'] = $modelRole->findAll();
		
		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}
		
			$data['recursos'] = json_decode($modelRecursos->findbyUser($id));
			$data['autonomia'] = json_decode($modelAutonomiaUser->getUserAuto($id));
			$data['datobanco'] = $modelDatoBanco->where('id_user',$id)->findAll();
			$data['historial'] = json_decode($historialModel->getAllByUser($id));
			$data['contactos'] = json_decode($contactosModel->getAllByUser($id));
			$data['grupos'] = json_decode($modelGruposUsuario->getAllByUser($id));
			$data['user'] = json_decode($model->getAllData($id));




			if(!isset($data['user'][0])) {	// Creamos una session para mostrar el mensaje de denegación por permiso
				$session = session();
				$session->setFlashdata('error', 'Ha surgido un problema al cargar el usuario, contacte con soporte');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/show');}
			$data['user'] = (array)$data['user'][0];
			$data['acciones'] = json_decode($modelAcciones->getByUserId($id));

		echo view('templates/dashboard/header',$data);
		echo view('templates/user/edit',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function new()
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];
		$modelRole = new RoleModel();
		$modelPerm = new PermsModel();
		$model = new UserModel();
		$minusvaliaModel = new MinusvaliaModel();
		$sorderaModel = new SorderaModel();
		$historialModel = new HistorialModel();

		$uri = service('uri');

		// Cargamos los helpers de formularios
		helper(['form']);
		$data['roles'] = $modelRole->findAll();
	
		//Comprobamos si es administrador para crear usuarios
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
			
		if($perm[0]['create'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para crear en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {
		
			$user = $model->where('email',$this->request->getVar('email'))
					  ->first();

			if($user){ 
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('error', 'Ya existe un usuario registrado con este email');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/new/');
			}
			
			// reglas de validación
			$rules = [
				'name' => 'required|min_length[3]|max_length[100]',
				'lastname' => 'required|min_length[3]|max_length[100]',
				'email' => 'required|min_length[6]|max_length[100]|is_unique[tbl_usuarios.email]',
				'password' => 'required|min_length[8]|max_length[255]',
				'password_confirm' => 'matches[password]',
				'role' => 'required'
			];

			// Comprobación de las validaciones
			if(! $this->validate($rules)) {

				$newData = [
					'name' => $this->request->getVar('name'),
					'lastname' => $this->request->getVar('lastname'),
					'email' => $this->request->getVar('email'),
					'password' => $this->request->getVar('password'),
					'role_id' => $this->request->getVar('role')
				];
				
				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;

			} else {
				//Guardar en BBDD
	
				$newData = [
					'name' => $this->request->getVar('name'),
					'lastname' => $this->request->getVar('lastname'),
					'lastlastname' => $this->request->getVar('lastlastname'),
					'email' => $this->request->getVar('email'),
					'password' => $this->request->getVar('password'),
					'role_id' => $this->request->getVar('role'),
					'servicioalta_id' => 7
				];

				//Guardamos
				$userid = $model->insert($newData);

				$historial = [
					'fecha'  => date('now'),
					'id_user' => $userid,
					'id_situacion' => 1,
					'motivo' => 'Registro Web'
				];

				$historialModel->insert($historial);
				
				$sordera = [
					'tipo_sordera' => "Ninguna",
					'causa_sordera' => "",
					'ayuda_tecnica' => "",
					'nivel_estudios' => "",
					'centro_estudios' => "",
					'comunicacion' => "",
					'id_user' => $userid,
				];

				$idSordera = $sorderaModel->insert($sordera);

				$minusvalia = [
					'minusvalia' => "",
					'porcentaje' => 0,
					'motivo' => "",
					'id_user' => $userid,
				];

				$idminusvalia = $minusvaliaModel->insert($minusvalia);

				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Registrado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'.$userid);
			}

		}

		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/new',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function delete($id)
	{
		$model = new UserModel();
		$minusvaliaModel = new MinusvaliaModel();
		$sorderaModel = new SorderaModel();
		$historialModel = new HistorialModel();
		$answer = $model->find($id);

		if(!$answer){
			// Creamos una session para mostrar el mensaje de error
			$session = session();
			$session->setFlashdata('error', 'Ha surgido un problema al eliminar');
			return redirect()->to(base_url().'/user/show');

		}
		//Comprobamos que no elimine a otro administrador
		if($answer['role_id'] == 1){
			// Creamos una session para mostrar el mensaje de error
			$session = session();
			$session->setFlashdata('error', 'No se puede eliminar a otro Administrador, para esto contacta soporte');
			return redirect()->to(base_url().'/user/show');
		}

		if($id == session()->get('id')){
			// Creamos una session para mostrar el mensaje de error
			$session = session();
			$session->setFlashdata('error', 'No te puedes eliminar a ti mismo');
			return redirect()->to(base_url().'/user/show');
		} 

		$model->delete($id);
		$historialModel->where('id_user',$id)->delete();
		$sorderaModel->where('id_user',$id)->delete($id);
		$minusvaliaModel->where('id_user',$id)->delete($id);
		
		// Creamos una session para mostrar el mensaje de registro correcto
		$session = session();
		$session->setFlashdata('success', 'Eliminado correctamente');

		// Redireccionamos a la pagina de login
		return redirect()->to(base_url().'/user/show');

	}

	public function datousuario($id = "")
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserModel();
		$modelRole = new RoleModel();
		$modelPerm = new PermsModel();
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		$data['roles'] = $modelRole->findAll();

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {

				$newData = [
					'id' => $id,
					'name' => $this->request->getVar('name'),
					'lastname' => $this->request->getVar('lastname'),
					'lastlastname' => $this->request->getVar('lastlastname'),
					'email' => $this->request->getVar('email'),
					'n_socio' => $this->request->getVar('n_socio'),
					'role_id' => $this->request->getVar('role'),
					'fecha_nacimiento' => $this->request->getVar('fecha_nacimiento'),
					'lugar_nacimiento' => $this->request->getVar('lugar_nacimiento'),
					'nacionalidad' => $this->request->getVar('nacionalidad'),
					'sexo' => $this->request->getVar('sexo'),
					'direccion' => $this->request->getVar('direccion'),
					'codigo_postal' => $this->request->getVar('codigo_postal'),
					'localidad' => $this->request->getVar('localidad'),
					'provincia' => $this->request->getVar('provincia'),
					'estado_civil' => $this->request->getVar('estado_civil'),
					'hijos' => $this->request->getVar('hijos'),
					'servicioalta_id' => $this->request->getVar('servicio_alta'),
				];

				if($this->request->getPost('password') != ''){
					$newData['password'] = $this->request->getPost('password');
				}
			//Guardamos
			$model->save($newData);
			
			// Creamos una session para mostrar el mensaje de registro correcto
			$session = session();
			$session->setFlashdata('success', 'Actualizado correctamente');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/edit/'.$id);
			

		}
			
		$servicioalta = json_decode($model->getServicios());
		$data['servicioalta'] = $servicioalta;
		$data['user'] = json_decode($model->getByID($id));
		$data['user'] = (array)$data['user'][0];
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/datosusuario',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function datosordera($id = "")
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new SorderaModel();
		$modelRole = new RoleModel();
		$modelPerm = new PermsModel();
		$modelUser = new UserModel();

		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		$data['roles'] = $modelRole->findAll();

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {

			if($id == ""){
				$id = $this->request->getVar('id');
			}

			// reglas de validación
			$rules = [
				'causa_sordera' => 'required',
				'nivel_estudios' => 'required',
				'ayuda_tecnica' => 'required',
				'centro_estudios' => 'required',
				'comunicacion' => 'required'
			];

			// Comprobación de las validaciones
			if(! $this->validate($rules)) {

				$newData = [
					'causa_sordera' => $this->request->getVar('causa_sordera'),
					'ayuda_tecnica' => $this->request->getVar('ayuda_tecnica'),
					'nivel_estudios' => $this->request->getVar('nivel_estudios'),
					'centro_estudios' => $this->request->getVar('centro_estudios'),
					'comunicacion' => $this->request->getVar('comunicacion')
				];
				
				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;

			} else {
					$idsordera = $model->asArray()->where('id_user',$id)->first();
					$newData = [
						'id' => $idsordera,
						'causa_sordera' => $this->request->getVar('causa_sordera'),
						'tipo_sordera' => $this->request->getVar('tipo_sordera'),
						'ayuda_tecnica' => $this->request->getVar('ayuda_tecnica'),
						'nivel_estudios' => $this->request->getVar('nivel_estudios'),
						'centro_estudios' => $this->request->getVar('centro_estudios'),
						'comunicacion' => $this->request->getVar('comunicacion')
					];
				//Guardamos
				$model->save($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'.$id);
			}

		}

		$data['user'] = json_decode($modelUser->getAllData($id));
		$data['user'] = (array)$data['user'][0];
		$data['id'] = $id;
			
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/datosordera',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function datominusvalia($id = "")
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];



		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new MinusvaliaModel();
		$modelRole = new RoleModel();
		$modelUser = new UserModel();
		$modelPerm = new PermsModel();
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		$data['roles'] = $modelRole->findAll();

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		if($id == "") {
			$id = $this->request->getVar('id');

		}


		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {
			// reglas de validación
			$rules = [
				'porcentaje' => 'required',
				'motivo' => 'required|min_length[3]|max_length[100]',
			];

			// Comprobación de las validaciones
			if(! $this->validate($rules)) {

				$newData = [
					'name' => $this->request->getVar('name'),
					'lastname' => $this->request->getVar('lastname'),
					'role_id' => $this->request->getVar('role')
				];
				
				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;

			} else {

				$minus = $model->asArray()->where('id_user',$id)->first();

				$newData = [
					'id' => $minus['id'],
					'porcentaje' => $this->request->getPost('porcentaje'),
					'motivo' => $this->request->getPost('motivo'),
					'minusvalia' => $this->request->getPost('minusvalia'),
					'id_user' => $id
				];
				

				//Guardamos
				$model->save($newData);

				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'.$id);
			}

		}

		$data['id'] = $id;
		$data['user'] = json_decode($modelUser->getMinusvalia($id));
		$data['user'] = (array)$data['user'][0];
			
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/datominusvalia',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function borrarrecurso($id,$idrecurso){
				//Variable con todos los datos a pasar a las vistas
				$data = [];

				// Cargamos los helpers de formularios
				helper(['form']);
				$uri = service('uri');
				$model = new RecursoUserModel();
				$modelPerm = new PermsModel();
				$modelRole = new RoleModel();
				
				$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
		
				if($perm[0]['delete'] == 0){
					// Creamos una session para mostrar el mensaje de denegación por permiso
					$session = session();
					$session->setFlashdata('error', 'No tienes permisos para borrar esta sección');
					
					// Redireccionamos a la pagina de login
					return redirect()->to(base_url().'/user/edit/'.$id);
				}
				$data['roles'] = $modelRole->findAll();

				$answer =  json_decode($model->findIdRecurso($id,$idrecurso));
				$answer = (array) $answer[0];

				if(!$answer){
					// Creamos una session para mostrar el mensaje de error
					$session = session();
					$session->setFlashdata('error', 'Ha surgido un problema al eliminar');
					return redirect()->to(base_url().'/user/edit/'.$id);
		
				}
		
				$model->delete($answer['id']);

				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Eliminado correctamente');

				return redirect()->to(base_url().'/user/edit/'.$id);
	}

	public function nuevorecurso($id){
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new RecursossocialesModel();
		$modelPerm = new PermsModel();
		$modelRole = new RoleModel();
		$modelUser = new UserModel();
		$modelRecursoUser = new RecursoUserModel();
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		$data['roles'] = $modelRole->findAll();


		if($this->request->getMethod() == 'post') {
			
			$where = ['id_user' => $id, 'id_recurso' => $this->request->getVar('recurso')];
			$resp = $modelRecursoUser->where($where)->findAll();

			if($resp) {
				// Creamos una session para mostrar el mensaje de registro erroneo porque ya está asignado
			$session = session();
			$session->setFlashdata('error', 'Ya se encuentra asignado el recurso a este usuario');

			return redirect()->to(base_url().'/user/nuevorecurso/'.$id);
			}

			$newData = [
				'id_recurso' => $this->request->getVar('recurso'),
				'id_user' => $id
			];

			$modelRecursoUser->save($newData);

			// Creamos una session para mostrar el mensaje de registro correcto
			$session = session();
			$session->setFlashdata('success', 'Añadido correctamente');

			return redirect()->to(base_url().'/user/edit/'.$id);
		}

		$data['recursos'] = $model->findAll();
		$data['user'] = json_decode($modelUser->getAllData($id));
		$data['user'] = (array)$data['user'][0];
		$data['recursoUser'] = $modelRecursoUser->where('id_user',$id)->findAll();

		echo view('templates/dashboard/header',$data);
		echo view('templates/user/datorecursos',$data);
		echo view('templates/dashboard/footer',$data);
}

	public function datocomunicacion($id = "")
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserModel();
		$modelRole = new RoleModel();
		$modelPerm = new PermsModel();
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		$data['roles'] = $modelRole->findAll();

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		
		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {

			// reglas de validación
			$rules = [
				'telefono_fijo' => 'required|min_length[3]|max_length[100]',
				'telefono_movil' => 'required|min_length[3]|max_length[100]',
			];

			// Comprobación de las validaciones
			if(! $this->validate($rules)) {

				$newData = [
					'telefono_fijo' => $this->request->getVar('telefono_fijo'),
					'telefono_movil' => $this->request->getVar('telefono_movil'),
					'fax' => $this->request->getVar('fax')
				];
				
				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;

			} else {
				$email = 0;
				$fax = 0;
				$movil = 0;
				$sms = 0;
				$mensajeria = 0;

				if($this->request->getVar('envio_email') == "envio_email"){$email = 1;} else {$email = 0;}
				if($this->request->getVar('envio_fax') == "envio_fax"){$fax = 1;} else {$fax = 0;}
				if($this->request->getVar('envio_movil') == "envio_movil"){$movil = 1;} else {$movil = 0;}
				if($this->request->getVar('envio_sms') == "envio_sms"){$sms = 1;} else {$sms = 0;}
				if($this->request->getVar('envio_mensajeria') == "envio_mensajeria"){$mensajeria = 1;} else {$mensajeria = 0;}

				$newData = [
					'id' => $id,
					'telefono_fijo' => $this->request->getVar('telefono_fijo'),
					'telefono_movil' => $this->request->getVar('telefono_movil'),
					'fax' => $this->request->getVar('fax'),
					'envio_email' => $email,
					'envio_fax' => $fax,
					'envio_movil' => $movil,
					'envio_sms' => $sms,
					'envio_mensajeria' => $mensajeria 
				];

				//Guardamos
				$model->save($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'.$id);
			}

		}

		$data['user'] = json_decode($model->getAllData($id));
		$data['user'] = (array)$data['user'][0];
			
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/datocomunicacion',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function borrarautonomia($id,$userid)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserAutonomiaModel();
		$modelPerm = new PermsModel();
		$modelRole = new RoleModel();
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['delete'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para borrar esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/edit/'.$userid);
		}

		$data['roles'] = $modelRole->findAll();

		$model->where('id',$id)->delete();

		// Creamos una session para mostrar el mensaje de registro correcto
		$session = session();
		$session->setFlashdata('success', 'Eliminado correctamente');

		return redirect()->to(base_url().'/user/edit/'.$userid);
	}

	public function datoautonomiaedit($id,$userid)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];
		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserModel();
		$modelRole = new RoleModel();
		$modelPerm = new PermsModel();
		$modelAutonomiaUser = new AutonomiapersonalModel();

		$data['autonomiaSel'] = json_decode($modelAutonomiaUser->getUserAutoById($id));
		$data['autonomiaSel'] = (array)$data['autonomiaSel'][0];
	
		$data['autonomias'] = $modelAutonomiaUser->findAll();

		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		$data['roles'] = $modelRole->findAll();

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {
			$data['autonomiaSel'] = json_decode($modelAutonomiaUser->getUserAutoByIds($userid,$this->request->getVar('tipo_autonomia')));


			if($data['autonomiaSel']){
				foreach($data['autonomiaSel'] as $item) {
					if($id != $data['autonomiaSel'][0]->ID && $this->request->getVar('tipo_autonomia') == $item->id_autonomia){

							// Creamos una session para mostrar el mensaje de denegación por permiso
							$session = session();
							$session->setFlashdata('error', 'Ya está asignada esta autonomía a este usuario');
	
							// Redireccionamos a la pagina de ver
							return redirect()->to(base_url().'/user/edit/'.$id);
					}
				}	
			}

				$newData = [
					'id_user' => $userid,
					'id_autonomia' => $this->request->getVar('tipo_autonomia'),
					'observacion' => $this->request->getVar('observacion')
				];

				//Guardamos
				$modelAutonomiaUser->updateUserAuto($id,$newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'. $userid);
		

		}

		$data['user'] = json_decode($model->getAllData($userid));
		$data['user'] = (array)$data['user'][0];
			
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/datoautonomia',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function datoautonomianew($id)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserModel();
		$modelRole = new RoleModel();
		$modelPerm = new PermsModel();
		$modelAutonomiaUser = new AutonomiapersonalModel();
	
		$data['autonomias'] = $modelAutonomiaUser->findAll();

		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		$data['roles'] = $modelRole->findAll();

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {

			$data['autonomiaSel'] = json_decode($modelAutonomiaUser->getUserAutoByIds($id,$this->request->getVar('tipo_autonomia')));
			if($data['autonomiaSel']){
				// Creamos una session para mostrar el mensaje de denegación por permiso
				$session = session();
				$session->setFlashdata('error', 'Ya está asignada esta autonomía a este usuario');
				
				// Redireccionamos a la pagina de ver
				return redirect()->to(base_url().'/user/edit/'.$id);
			}

			$newData = $id .",". $this->request->getVar('tipo_autonomia') .", '". $this->request->getVar('observacion') . "'" ;

			//Guardamos
			$modelAutonomiaUser->createUserAuto($newData);
			
			// Creamos una session para mostrar el mensaje de registro correcto
			$session = session();
			$session->setFlashdata('success', 'Actualizado correctamente');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/edit/'.$id);
	

		}

		$data['user'] = json_decode($model->getAllData($id));
		$data['user'] = (array)$data['user'][0];

		echo view('templates/dashboard/header',$data);
		echo view('templates/user/datoautonomianew',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function borrardatobancario($id,$userid)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$modelPerm = new PermsModel();
		$modelDatoBanco = new DatoBancoModel();
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['delete'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para borrar esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/edit/'.$userid);
		}

		$modelDatoBanco->where('id',$id)->delete();

		// Creamos una session para mostrar el mensaje de registro correcto
		$session = session();
		$session->setFlashdata('success', 'Eliminado correctamente');

		return redirect()->to(base_url().'/user/edit/'.$userid);
	}

	public function abrirruta($id){
		$modelDatoBanco = new DatoBancoModel();

		$dato = $modelDatoBanco->where('id',$id)->first();
		$ruta = $dato['ruta'];
		exec('cd '. $ruta);
		exec('start .');
	}
	public function datobancarioedit($id,$userid)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];
		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserModel();
		$modelPerm = new PermsModel();
		$modelDatoBanco = new DatoBancoModel();

		$data['datobancario'] = $modelDatoBanco->where('id',$id)->findAll();
		$data['datobancario'] = $data['datobancario'][0];

		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {

				$newData = [
					'id' => $id,
					'fecha' => $this->request->getVar('fecha'),
					'numero_cuenta' => $this->request->getVar('numero_cuenta'),
					'ruta' => $this->request->getVar('ruta')
				];

				//Guardamos
				$modelDatoBanco->save($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'. $userid);
		

		}

		$data['user'] = json_decode($model->getAllData($userid));
		$data['user'] = (array)$data['user'][0];
			
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/datobancario',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function datobancarionew($id)
	{
			//Variable con todos los datos a pasar a las vistas
			$data = [];

			// Cargamos los helpers de formularios
			helper(['form']);
			$uri = service('uri');
			$model = new UserModel();
			$modelRole = new RoleModel();
			$modelPerm = new PermsModel();
			$modelDatoBanco = new DatoBancoModel();
		
			$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

			$reps = $modelDatoBanco->where('id_user',$id)->findAll();


			if($perm[0]['create'] == 0){
				// Creamos una session para mostrar el mensaje de denegación por permiso
				$session = session();
				$session->setFlashdata('error', 'No tienes permisos para crear en esta sección');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/show');
			}
			


			// Comprobamos el metodo de la petición
			if($this->request->getMethod() == 'post') {

				foreach($reps as $item){
					if($item['numero_cuenta'] == $this->request->getVar('numero_cuenta')){
						// Creamos una session para mostrar el mensaje de denegación por permiso
						$session = session();
						$session->setFlashdata('error', 'Ya se encuentra asignado ese numero de cuenta a este usuario');
						
						// Redireccionamos a la pagina de login
						return redirect()->to(base_url().'/user/datobancarioedit');
					}
					if($item['ruta'] == $this->request->getVar('ruta')){
						// Creamos una session para mostrar el mensaje de denegación por permiso
						$session = session();
						$session->setFlashdata('error', 'Ya se encuentra asignada esa ruta a este usuario');
						
						// Redireccionamos a la pagina de login
						return redirect()->to(base_url().'/user/datobancarioedit');
					}
				}

				$newData = [
					'id_user' => $id,
					'fecha' => $this->request->getVar('fecha'),
					'numero_cuenta' => $this->request->getVar('numero_cuenta'),
					'ruta' => $this->request->getVar('ruta')
				]; 
	
				//Guardamos
				$modelDatoBanco->insert($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'.$id);
		
	
			}
	
			$data['user'] = json_decode($model->getAllData($id));
			$data['user'] = (array)$data['user'][0];
	
			echo view('templates/dashboard/header',$data);
			echo view('templates/user/datobancarionew',$data);
			echo view('templates/dashboard/footer',$data);
	}

	public function borrarcontacto($id,$userid)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$modelPerm = new PermsModel();
		$modelContacto = new ContactosModel();
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['delete'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para borrar esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/edit/'.$userid);
		}

		$modelContacto->where('id',$id)->delete();

		// Creamos una session para mostrar el mensaje de registro correcto
		$session = session();
		$session->setFlashdata('success', 'Eliminado correctamente');

		return redirect()->to(base_url().'/user/edit/'.$userid);
	}

	public function contactoedit($id,$userid)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];
		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserModel();
		$modelPerm = new PermsModel();
		$modelContacto = new ContactosModel();
		$modelTipoContacto = new TipocontactoModel();

		$data['tipocontacto'] = $modelTipoContacto->findAll();
		$data['contactoSel'] = $modelContacto->where('id',$id)->findAll();
		$data['contactoSel'] = $data['contactoSel'][0];
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {

				$newData = [
					'id' => $id,
					'fecha' => $this->request->getVar('fecha'),
					'numero_cuenta' => $this->request->getVar('numero_cuenta'),
					'ruta' => $this->request->getVar('ruta')
				];

				//Guardamos
				$modelContacto->save($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'. $userid);
		

		}

		$data['user'] = json_decode($model->getAllData($userid));
		$data['user'] = (array)$data['user'][0];
			
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/datocontacto',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function datocontactonew($id)
	{
			//Variable con todos los datos a pasar a las vistas
			$data = [];

			// Cargamos los helpers de formularios
			helper(['form']);
			$uri = service('uri');
			$model = new UserModel();
			$modelRole = new RoleModel();
			$modelPerm = new PermsModel();
			$modelContacto = new ContactosModel();
			$modelTipoContacto = new TipocontactoModel();
			
			$data['tipocontacto'] = $modelTipoContacto->findAll();
			$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));


			if($perm[0]['create'] == 0){
				// Creamos una session para mostrar el mensaje de denegación por permiso
				$session = session();
				$session->setFlashdata('error', 'No tienes permisos para crear en esta sección');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/show');
			}
			

			if($this->request->getMethod() == 'post') {
				$newData = [
					'id_user' => $id,
					'id_tipo_contacto' => $this->request->getVar('id_tipo_contacto'),
					'nombre' => $this->request->getVar('nombre'),
					'telefono_fijo' => $this->request->getVar('telefono_fijo'),
					'telefono_movil' => $this->request->getVar('telefono_movil'),
					'email' => $this->request->getVar('email'),
					'fax' => $this->request->getVar('fax'),
					'observaciones' => $this->request->getVar('observaciones')
				]; 
	
				//Guardamos
				$modelContacto->insert($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'.$id);
		
	
			}
	
			$data['user'] = json_decode($model->getAllData($id));
			$data['user'] = (array)$data['user'][0];
	
			echo view('templates/dashboard/header',$data);
			echo view('templates/user/datocontactonew',$data);
			echo view('templates/dashboard/footer',$data);
	}

	public function borrargrupopertenencia($id,$userid)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$modelPerm = new PermsModel();
		$modelGruposUsuario = new GrupoUsuarioModel();
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['delete'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para borrar esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/edit/'.$userid);
		}

		$modelGruposUsuario->where('id',$id)->delete();

		// Creamos una session para mostrar el mensaje de registro correcto
		$session = session();
		$session->setFlashdata('success', 'Eliminado correctamente');

		return redirect()->to(base_url().'/user/edit/'.$userid);
	}

	public function grupopertenenciaedit($id,$userid)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];
		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserModel();
		$modelPerm = new PermsModel();
		$modelGruposPertenencia = new GrupopertenenciaModel();
		$modelGruposUsuario = new GrupoUsuarioModel();

		$data['grupos'] = $modelGruposPertenencia->findAll();
		$data['grupoPertenenciaSel'] = $modelGruposUsuario->where('id',$id)->findAll();
		$data['grupoPertenenciaSel'] = $data['grupoPertenenciaSel'][0];
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {

			$data['grupoPertenenciaSel'] = json_decode($modelGruposUsuario->getUserGrupoByIds($userid,$this->request->getVar('id_grupo')));

			if($data['grupoPertenenciaSel']){
				foreach($data['grupoPertenenciaSel'] as $item) {
					if($id != $data['grupoPertenenciaSel'][0]->id && $this->request->getVar('id_grupo') == $item->id_grupo){

							// Creamos una session para mostrar el mensaje de denegación por permiso
							$session = session();
							$session->setFlashdata('error', 'Ya está asignado este grupo a este usuario');
	
							// Redireccionamos a la pagina de ver
							return redirect()->to(base_url().'/user/edit/'.$id);
					}
				}	
			}
			
				$newData = [
					'id' => $id,
					'id_user' => $userid,
					'id_grupo' => $this->request->getVar('id_grupo'),
					'observaciones' => $this->request->getVar('observaciones')
				];

				//Guardamos
				$modelGruposUsuario->save($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'. $userid);
		

		}

		$data['user'] = json_decode($model->getAllData($userid));
		$data['user'] = (array)$data['user'][0];
			
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/datogrupo',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function grupopertenencianew($id)
	{
			//Variable con todos los datos a pasar a las vistas
			$data = [];

			// Cargamos los helpers de formularios
			helper(['form']);
			$uri = service('uri');
			$model = new UserModel();
			$modelRole = new RoleModel();
			$modelPerm = new PermsModel();
			$modelGruposPertenencia = new GrupopertenenciaModel();
			$modelGruposUsuario = new GrupoUsuarioModel();
			
			$data['grupos'] = $modelGruposPertenencia->findAll();
			$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));


			if($perm[0]['create'] == 0){
				// Creamos una session para mostrar el mensaje de denegación por permiso
				$session = session();
				$session->setFlashdata('error', 'No tienes permisos para crear en esta sección');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/show');
			}
			

			if($this->request->getMethod() == 'post') {

				$data['grupoPertenenciaSel'] = json_decode($modelGruposUsuario->getUserGrupoByIds($id,$this->request->getVar('id_grupo')));

				if($data['grupoPertenenciaSel']){

					// Creamos una session para mostrar el mensaje de denegación por permiso
					$session = session();
					$session->setFlashdata('error', 'Ya está asignado este grupo a este usuario');

					// Redireccionamos a la pagina de ver
					return redirect()->to(base_url().'/user/edit/'.$id);
				}
			
				$newData = [
					'id_user' => $id,
					'id_grupo' => $this->request->getVar('id_grupo'),
					'observaciones' => $this->request->getVar('observaciones')
				]; 
	
				//Guardamos
				$modelGruposUsuario->insert($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'.$id);
		
	
			}
	
			$data['user'] = json_decode($model->getAllData($id));
			$data['user'] = (array)$data['user'][0];
	
			echo view('templates/dashboard/header',$data);
			echo view('templates/user/datogruponew',$data);
			echo view('templates/dashboard/footer',$data);
	}

	public function comentariosedit($id)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];
		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserModel();
		$modelPerm = new PermsModel();
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {

				$newData = [
					'id' => $id,
					'comentario' => $this->request->getVar('comentario')
				];

				//Guardamos
				$model->save($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'. $id);
		

		}

		$data['user'] = json_decode($model->getAllData($id));
		$data['user'] = (array)$data['user'][0];
			
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/comentario',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function situacioneconomicaedit($id)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];
		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserModel();
		$modelPerm = new PermsModel();
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {

				$newData = [
					'id' => $id,
					'situacion_economica' => $this->request->getVar('comentario')
				];

				//Guardamos
				$model->save($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'. $id);
		

		}

		$data['user'] = json_decode($model->getAllData($id));
		$data['user'] = (array)$data['user'][0];
			
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/situacioneconomica',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function situacionlaboraledit($id)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];
		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserModel();
		$modelPerm = new PermsModel();
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {

				$newData = [
					'id' => $id,
					'situacion_laboral' => $this->request->getVar('comentario')
				];

				//Guardamos
				$model->save($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'. $id);
		

		}

		$data['user'] = json_decode($model->getAllData($id));
		$data['user'] = (array)$data['user'][0];
			
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/situacionlaboral',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function ambitosocialedit($id)
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];
		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$model = new UserModel();
		$modelPerm = new PermsModel();
		
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/user/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {

				$newData = [
					'id' => $id,
					'ambito_social' => $this->request->getVar('comentario')
				];

				//Guardamos
				$model->save($newData);
				
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url().'/user/edit/'. $id);
		

		}

		$data['user'] = json_decode($model->getAllData($id));
		$data['user'] = (array)$data['user'][0];
			
		echo view('templates/dashboard/header',$data);
		echo view('templates/user/ambitosocial',$data);
		echo view('templates/dashboard/footer',$data);
	}
}