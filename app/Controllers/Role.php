<?php namespace App\Controllers;

use App\Models\UserModel;
use App\Models\RoleModel;
use App\Models\PermsModel;
use App\Models\ApartadoModel;

class Role extends BaseController
{
	public function index()
	{
		$data = [];

		
		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view('templates/dashboard/dashboard',$data);
		echo view('templates/dashboard/footer',$data);
	}

	// Ver
	public function show()
	{

		helper(['form']);
		$uri = service('uri');
		$modelPerm = new PermsModel();
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['show'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para ver esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url()."/".'dashboard');
		}
		
		$data = [];
		$model = new RoleModel();

		$data['columns'] = json_decode($model->getAll());

		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view('maintence/role/show',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function edit($id = "")
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$modelPerm = new PermsModel();
		$roleModel = new RoleModel();
		$modelApartado = new ApartadoModel();

		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
		
		
		if($id == "") {
			$id = $this->request->getVar('id');

			if($id == ""){
				// Creamos una session para mostrar el mensaje de denegación por permiso
				$session = session();
				$session->setFlashdata('error', 'No se ha seleccionado ningun elemento para editar');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url()."/".'role/show');
			}
		}

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url()."/".'role/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {
		

			// reglas de validación
			$rules = [
				'description' => 'required|min_length[3]|max_length[100]',
				'facturar' => 'required',
				'permsver' => 'required'
			];

			// Comprobación de las validaciones
			if(! $this->validate($rules)) {

				$newData = [
					'description' => $this->request->getVar('description'),
					'facturar' => $this->request->getVar('facturar'),
					'role_id' => $this->request->getVar('id'),
					'show_secction' => $this->request->getVar('permsver'),
					'edit_secction' => $this->request->getVar('permsedit'),
					'delete_secction' => $this->request->getVar('permsdelete'),
					'create_secction' => $this->request->getVar('permscreate')
				];
				
				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;

			} else {
						  
				$permsver = $this->request->getPost('permsver');
				$permsedit = $this->request->getPost('permsedit');
				$permsdelete = $this->request->getPost('permsdelete');
				$permscreate = $this->request->getPost('permscreate');

				// Acutlizar rol
				$newData = [
					'id' => $id,
					'description' => $this->request->getPost('description'),
					'facturar' => (int)$this->request->getPost('facturar')
				];

				$roleModel->save($newData);

				// Actualizar permisos

				

				$apartados = json_decode($modelApartado->getAll($id));
				$prueba = [];

				foreach($apartados as $item) {
					$show = 0;
					$edit = 0;
					$delete = 0;
					$create = 0;

					$permId = json_decode($modelPerm->getPermsIds($id,$item->ID));
					if($permscreate != NULL){
						foreach($permsver as $key=>$value){
							if($value == $item->ID) {
								$show = 1;
								break;
							}
						}
					}
					if($permscreate != NULL){
						foreach($permsedit as $key=>$value){
							if($value == $item->ID) {
								$edit = 1;
								$show = 1;
								break;
							}
						}
					}
					if($permscreate != NULL){
						foreach($permsdelete as $key=>$value){
							if($value == $item->ID) {
								$delete = 1;
								$show = 1;
								break;
							}
						}
					}
					if($permscreate != NULL){
						foreach($permscreate as $key=>$value){
							if($value == $item->ID) {
								$create = 1;
								$show = 1;
								break;
							}
						}
					}
				if($permId == null) {
					$newData = [
						'role_id' => (int)$id,
						'apartado_id' => (int)$item->ID,
						'show_secction' => (int)$show,
						'edit_secction' => (int)$edit,
						'delete_secction' => (int)$delete,
						'create_secction' => (int)$create
					];
				} else {
					$newData = [
						'id' => (int)$permId->id,
						'role_id' => (int)$id,
						'apartado_id' => (int)$item->ID,
						'show_secction' => (int)$show,
						'edit_secction' => (int)$edit,
						'delete_secction' => (int)$delete,
						'create_secction' => (int)$create
					];
				}
					
					//Guardamos
					$modelPerm->save($newData);

				}
				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url()."/".'role/edit/'.$id);
			}

		}


		$data['data'] = json_decode($roleModel->getByID($id));
		$data['apartados'] = json_decode($modelApartado->getAll($id));
		if($modelPerm->getPerms($id)){
		$data['perms'] = $modelPerm->getPerms($id);

		} else {
		$data['perms'] = [];

		}

			
		echo view('templates/dashboard/header',$data);
		echo view('maintence/role/edit',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function new()
	{
				//Variable con todos los datos a pasar a las vistas
				$data = [];

				// Cargamos los helpers de formularios
				helper(['form']);
				$uri = service('uri');
				$modelPerm = new PermsModel();
				$roleModel = new RoleModel();
				$modelApartado = new ApartadoModel();
		
				$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
				
				if($perm[0]['create'] == 0){
					// Creamos una session para mostrar el mensaje de denegación por permiso
					$session = session();
					$session->setFlashdata('error', 'No tienes permisos para crear en esta sección');
					
					// Redireccionamos a la pagina de login
					return redirect()->to(base_url()."/".'role/show');
				}
		
				// Comprobamos el metodo de la petición
				if($this->request->getMethod() == 'post') {
		
					// reglas de validación
					$rules = [
						'description' => 'required|min_length[3]|max_length[100]',
						'facturar' => 'required',
						'permsver' => 'required'
					];
		
					// Comprobación de las validaciones
					if(! $this->validate($rules)) {
		
						$newData = [
							'description' => $this->request->getVar('description'),
							'facturar' => $this->request->getVar('facturar'),
							'show_secction' => $this->request->getVar('permsver')
						];
						
						// Guardamos el error para mostrar en la vista
						$data['validation'] = $this->validator;
		
					} else {
								  
						$permsver = $this->request->getPost('permsver');
						$permsedit = $this->request->getPost('permsedit');
						$permsdelete = $this->request->getPost('permsdelete');
						$permscreate = $this->request->getPost('permscreate');
		
						// Acutlizar rol
						$newData = [
							'description' => $this->request->getPost('description'),
							'facturar' => (int)$this->request->getPost('facturar')
						];

						if($roleModel->where('description',$this->request->getPost('description'))->findAll() == NULL){
							$idReturn = $roleModel->insert($newData);

						} else {
							// Creamos una session para mostrar el mensaje de denegación por permiso
							$session = session();
							$session->setFlashdata('error', 'Ya existe un registro con esa descripción');
							
							// Redireccionamos a la pagina de login
							return redirect()->to(base_url()."/".'role/new');
						}

		
						// Crear permisos
		
						$apartados = json_decode($modelApartado->getAll());
						$prueba = [];
		
						foreach($apartados as $item) {
							$show = 0;
							$edit = 0;
							$delete = 0;
							$create = 0;
		
							if($permscreate != NULL){
								foreach($permsver as $key=>$value){
									if($value == $item->ID) {
										$show = 1;
										break;
									}
								}
							}
							if($permscreate != NULL){
								foreach($permsedit as $key=>$value){
									if($value == $item->ID) {
										$edit = 1;
										$show = 1;
										break;
									}
								}
							}
							if($permscreate != NULL){
								foreach($permsdelete as $key=>$value){
									if($value == $item->ID) {
										$delete = 1;
										$show = 1;
										break;
									}
								}
							}
							if($permscreate != NULL){
								foreach($permscreate as $key=>$value){
									if($value == $item->ID) {
										$create = 1;
										$show = 1;
										break;
									}
								}
							}

							$newData = [
								'role_id' => (int)$idReturn,
								'apartado_id' => (int)$item->ID,
								'show_secction' => (int)$show,
								'edit_secction' => (int)$edit,
								'delete_secction' => (int)$delete,
								'create_secction' => (int)$create
							];
						
							
							//Guardamos
							// $modelPerm->save($newData);
		
						}
						// Creamos una session para mostrar el mensaje de registro correcto
						$session = session();
						$session->setFlashdata('success', 'Actualizado correctamente');
						
						// Redireccionamos a la pagina de login
						return redirect()->to(base_url()."/".'role/show');
					}
		
				}
		
		
				// $data['data'] = json_decode($roleModel->getByID($id));
				$data['apartados'] = json_decode($modelApartado->getAll());
					
				echo view('templates/dashboard/header',$data);
				echo view('maintence/role/new',$data);
				echo view('templates/dashboard/footer',$data);
	}

	// Borrar
	public function delete($id)
	{
		$modelPerm = new PermsModel();
		$perm = $modelPerm->getPerms(session()->get('role'),'role');

		if($perm[0]['delete'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para borrar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url().'/role/show');
		}

		$modelUser = new UserModel();
		$users = $modelUser->where('role_id',$id)->findAll();

		if($users){
			// Creamos una session para mostrar el mensaje de error
			$session = session();
			$session->setFlashdata('error', 'No puedes eliminar un Tipo que contenga usuarios');
			return redirect()->to(base_url().'/role/show');
		} 

		$model = new RoleModel();
		$answer = $model->delete($id);
		// Creamos una session para mostrar el mensaje de registro correcto
		$session = session();
		$session->setFlashdata('success', 'Eliminado correctamente');

		// Redireccionamos a la pagina de login
		return redirect()->to(base_url().'/role/show');

	}

}