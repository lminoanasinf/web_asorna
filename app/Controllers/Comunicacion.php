<?php namespace App\Controllers;

use App\Models\UserModel;
use App\Models\ApartadoModel;
use App\Models\PermsModel;
use App\Models\RoleModel;
use App\Models\ComunicacionModel;

class Comunicacion extends BaseController
{
	protected $redireccion = "comunicacion";
	protected $redireccionView = "maintence/comunicacion";

	public function index()
	{
		$data = [];

		
		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view('templates/dashboard/dashboard',$data);
		echo view('templates/dashboard/footer',$data);
	}

	// Ver
	public function show()
	{

		helper(['form']);
		$uri = service('uri');
		$modelPerm = new PermsModel();
		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));

		if($perm[0]['show'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para ver esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to('/dashboard');
		}
		
		$data = [];
		$model = new ComunicacionModel();

		$data['columns'] = json_decode($model->getAll());

		// Cargamos las vistas en orden
		echo view('templates/dashboard/header',$data);
		echo view($this->redireccionView.'/show',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function edit($id = "")
	{
		//Variable con todos los datos a pasar a las vistas
		$data = [];

		// Cargamos los helpers de formularios
		helper(['form']);
		$uri = service('uri');
		$modelPerm = new PermsModel();
		$roleModel = new RoleModel();
		$model = new ComunicacionModel();

		$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
		$data['id'] = $id;
		
		
		if($id == "") {

			if($id == ""){
				// Creamos una session para mostrar el mensaje de denegación por permiso
				$session = session();
				$session->setFlashdata('error', 'No se ha seleccionado ningun elemento para editar');
				
				// Redireccionamos a la pagina de login
				return redirect()->to(base_url()."/".$this->redireccion.'/show');
			}
		}

		if($perm[0]['edit'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para editar esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url()."/".$this->redireccion.'/show');
		}

		// Comprobamos el metodo de la petición
		if($this->request->getMethod() == 'post') {
		
			// reglas de validación
			$rules = [
				'description' => 'required|min_length[3]|max_length[100]'
			];

			// Comprobación de las validaciones
			if(! $this->validate($rules)) {

				$newData = [
					'description' => $this->request->getVar('description'),
					'online' => $this->request->getVar('online')
				];
				
				// Guardamos el error para mostrar en la vista
				$data['validation'] = $this->validator;

			} else {
						  
				// Acutlizar rol
				$newData = [
					'id' => $id,
					'description' => $this->request->getPost('description'),
					'online' => $this->request->getVar('online')
				];


				//Guardamos
				$model->save($newData);


				// Creamos una session para mostrar el mensaje de registro correcto
				$session = session();
				$session->setFlashdata('success', 'Actualizado correctamente');
				
				// Redireccionamos a la pagina
				return redirect()->to(base_url()."/".$this->redireccion.'/show');
			}

		}

		$data['data'] = json_decode($model->getByID($id));
		if($modelPerm->getPerms($id)){
		$data['perms'] = $modelPerm->getPerms($id);

		} else {
		$data['perms'] = [];

		}

			
		echo view('templates/dashboard/header',$data);
		echo view($this->redireccionView.'/edit',$data);
		echo view('templates/dashboard/footer',$data);
	}

	public function new()
	{
				//Variable con todos los datos a pasar a las vistas
				$data = [];

				// Cargamos los helpers de formularios
				helper(['form']);
				$uri = service('uri');
				$modelPerm = new PermsModel();
				$model = new ComunicacionModel();
				$modelApartado = new ApartadoModel();
		
				$perm = $modelPerm->getPerms(session()->get('role'),$uri->getSegment(1));
				
				if($perm[0]['create'] == 0){
					// Creamos una session para mostrar el mensaje de denegación por permiso
					$session = session();
					$session->setFlashdata('error', 'No tienes permisos para crear en esta sección');
					
					// Redireccionamos a la pagina de login
					return redirect()->to(base_url()."/".$this->redireccion.'/show');
				}
		
				// Comprobamos el metodo de la petición
				if($this->request->getMethod() == 'post') {
		
					// reglas de validación
					$rules = [
						'description' => 'required|min_length[3]|max_length[100]'
					];
		
					// Comprobación de las validaciones
					if(! $this->validate($rules)) {
		
						$newData = [
							'description' => $this->request->getVar('description'),
							'online' => $this->request->getVar('online')

						];
						
						// Guardamos el error para mostrar en la vista
						$data['validation'] = $this->validator;
		
					} else {
								  
						// Acutlizar rol
						$newData = [
							'description' => $this->request->getPost('description'),
							'online' => $this->request->getVar('online')

							
						];

						if($model->where('description',$this->request->getPost('description'))->findAll() == NULL){
							$idReturn = $model->insert($newData);
						} else {
							// Creamos una session para mostrar el mensaje de denegación por permiso
							$session = session();
							$session->setFlashdata('error', 'Ya existe un registro con esa descripción');
							
							// Redireccionamos a la pagina de login
							return redirect()->to(base_url()."/".$this->redireccion.'/new');
						}
		
					}
						// Creamos una session para mostrar el mensaje de registro correcto
						$session = session();
						$session->setFlashdata('success', 'Actualizado correctamente');
						
						// Redireccionamos a la pagina de login
						return redirect()->to(base_url()."/".$this->redireccion.'/show');
					}
		
		
				$data['apartados'] = json_decode($modelApartado->getAll());
					
				echo view('templates/dashboard/header',$data);
				echo view($this->redireccionView.'/new',$data);
				echo view('templates/dashboard/footer',$data);
	}

	// Borrar
	public function delete($id)
	{
		$modelPerm = new PermsModel();
		$perm = $modelPerm->getPerms(session()->get('role'),'role');

		if($perm[0]['delete'] == 0){
			// Creamos una session para mostrar el mensaje de denegación por permiso
			$session = session();
			$session->setFlashdata('error', 'No tienes permisos para borrar en esta sección');
			
			// Redireccionamos a la pagina de login
			return redirect()->to(base_url()."/".$this->redireccion.'/show');
		}

		$model = new ComunicacionModel();
		$answer = $model->delete($id);
		// Creamos una session para mostrar el mensaje de registro correcto
		$session = session();
		$session->setFlashdata('success', 'Eliminado correctamente');

		// Redireccionamos a la pagina de login
		return redirect()->to(base_url()."/".$this->redireccion.'/show');

	}

}