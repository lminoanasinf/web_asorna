<?php
/**
 * Translate language strings.
 *
 * @package    CodeIgniter
 * @author     Luis Miño
 * @copyright  2020 CodeIgniter Foundation
 * @license    https://opensource.org/licenses/MIT	MIT License
 * @link       https://codeigniter.com
 * @since      Version 4.0.0
 * @filesource
 *
 * @codeCoverageIgnore
 */

return [
    'id'					=> 'ID',
    'email'                 => 'Email',
	'name'					=> 'Nombre',
	'lastname'				=> 'Apellido',
    'password'				=> 'Contraseña',
    'role_id'               => 'RoleID',
    'role'                  => 'Role',
    'created_at'            => 'Creado',
    'updated_at'            => 'Actualizado',
    'deleted_at'            => 'Eliminado',
    'eliminated'            => 'Eliminado',
    'close'                 => 'Cerrar',
    'save'                  => 'Guardar'
];
