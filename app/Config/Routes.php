<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Login');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

// Rutas de Login

$routes->get('/', 'Login::index',['filter' => 'noauth']);
$routes->get('/logout','Login::logout',['filter' => 'auth']);
$routes->match(['get','post'],'/register','Login::register',['filter' => 'noauth']);
$routes->match(['get','post'],'/forgot','Login::forgot',['filter' => 'noauth']);
$routes->match(['get','post'],'/recovery/(:any)','Login::recovery/$1',['filter' => 'noauth']);
$routes->match(['get','post'],'/profile','Login::profile',['filter' => 'auth']);
$routes->get('/dashboard', 'Dashboard::index',['filter' => 'auth']);

// Rutas de Users
$routes->get('user', 'User::show',['filter' => 'auth']);
$routes->get('user/show', 'User::show',['filter' => 'auth']);
$routes->get('user/edit/(:any)', 'User::edit/$1',['filter' => 'auth']);
$routes->get('user/new', 'User::new',['filter' => 'auth']);
$routes->get('user/datosusuario/(:any)', 'User::edit/$1Datosusuario/$1',['filter' => 'auth']);
$routes->get('user/datosordera/(:any)', 'User::datosordera/$1',['filter' => 'auth']);
$routes->match(['get','post'],'user/datominusvalia/(:any)', 'User::datominusvalia/$1',['filter' => 'auth']);

// Rutas Roles
$routes->get('role', 'Role::show',['filter' => 'auth']);
$routes->get('role/show', 'Role::show',['filter' => 'auth']);
$routes->get('role/edit/(:any)', 'Role::edit/$1',['filter' => 'auth']);
$routes->get('role/new', 'Role::new',['filter' => 'auth']);
$routes->get('role/delete/(:any)', 'Role::delete/$1',['filter' => 'auth']);


// Rutas Motivos Situacionales
$routes->get('motivesite', 'Motivesite::show',['filter' => 'auth']);
$routes->get('motivesite/show', 'Motivesite::show',['filter' => 'auth']);
$routes->get('motivesite/edit/(:any)', 'Motivesite::edit/$1',['filter' => 'auth']);
$routes->get('motivesite/new', 'Motivesite::new',['filter' => 'auth']);

// Rutas Situaciones
$routes->get('situaciones', 'Situaciones::show',['filter' => 'auth']);
$routes->get('situaciones/show', 'Situaciones::show',['filter' => 'auth']);
$routes->get('situaciones/edit/(:any)', 'Situaciones::edit/$1',['filter' => 'auth']);
$routes->get('situaciones/new', 'Situaciones::new',['filter' => 'auth']);

// Rutas Motivos Situaciones Acciones
$routes->get('motsitacc', 'Motsitacc::show',['filter' => 'auth']);
$routes->get('motsitacc/show', 'Motsitacc::show',['filter' => 'auth']);
$routes->get('motsitacc/edit/(:any)', 'Motsitacc::edit/$1',['filter' => 'auth']);
$routes->get('motsitacc/new', 'Motsitacc::new',['filter' => 'auth']);

// Rutas Motivos Tipo documentos
$routes->get('tipodocumento', 'Tipodocumento::show',['filter' => 'auth']);
$routes->get('tipodocumento/show', 'Tipodocumento::show',['filter' => 'auth']);
$routes->get('tipodocumento/edit/(:any)', 'Tipodocumento::edit/$1',['filter' => 'auth']);
$routes->get('tipodocumento/new', 'Tipodocumento::new',['filter' => 'auth']);

// Rutas Motivos Tipo contacto
$routes->get('tipocontacto', 'Tipocontacto::show',['filter' => 'auth']);
$routes->get('tipocontacto/show', 'Tipocontacto::show',['filter' => 'auth']);
$routes->get('tipocontacto/edit/(:any)', 'Tipocontacto::edit/$1',['filter' => 'auth']);
$routes->get('tipocontacto/new', 'Tipocontacto::new',['filter' => 'auth']);

// Rutas Motivos grupo pertenencia
$routes->get('grupopertenencia', 'Grupopertenencia::show',['filter' => 'auth']);
$routes->get('grupopertenencia/show', 'Grupopertenencia::show',['filter' => 'auth']);
$routes->get('grupopertenencia/edit/(:any)', 'Grupopertenencia::edit/$1',['filter' => 'auth']);
$routes->get('grupopertenencia/new', 'Grupopertenencia::new',['filter' => 'auth']);

// Rutas Motivos recursos sociales
$routes->get('recursossociales', 'Recursossociales::show',['filter' => 'auth']);
$routes->get('recursossociales/show', 'Recursossociales::show',['filter' => 'auth']);
$routes->get('recursossociales/edit/(:any)', 'Recursossociales::edit/$1',['filter' => 'auth']);
$routes->get('recursossociales/new', 'Recursossociales::new',['filter' => 'auth']);

// Rutas Motivos autonimia personal
$routes->get('autonomiapersonal', 'Autonomiapersonal::show',['filter' => 'auth']);
$routes->get('autonomiapersonal/show', 'Autonomiapersonal::show',['filter' => 'auth']);
$routes->get('autonomiapersonal/edit/(:any)', 'Autonomiapersonal::edit/$1',['filter' => 'auth']);
$routes->get('autonomiapersonal/new', 'Autonomiapersonal::new',['filter' => 'auth']);

// Rutas Motivos Servicios
$routes->get('servicios', 'Servicios::show',['filter' => 'auth']);
$routes->get('servicios/show', 'Servicios::show',['filter' => 'auth']);
$routes->get('servicios/edit/(:any)', 'Servicios::edit/$1',['filter' => 'auth']);
$routes->get('servicios/new', 'Servicios::new',['filter' => 'auth']);

// Rutas Motivos Servicios
$routes->get('tareas', 'Tareas::show',['filter' => 'auth']);
$routes->get('tareas/show', 'Tareas::show',['filter' => 'auth']);
$routes->get('tareas/edit/(:any)', 'Tareas::edit/$1',['filter' => 'auth']);
$routes->get('tareas/new', 'Tareas::new',['filter' => 'auth']);

// Rutas Motivos Comunicacion
$routes->get('comunicacion', 'Comunicacion::show',['filter' => 'auth']);
$routes->get('comunicacion/show', 'Comunicacion::show',['filter' => 'auth']);
$routes->get('comunicacion/edit/(:any)', 'Comunicacion::edit/$1',['filter' => 'auth']);
$routes->get('comunicacion/new', 'Comunicacion::new',['filter' => 'auth']);

// Rutas Motivos Acciones
$routes->get('acciones', 'Acciones::show',['filter' => 'auth']);
$routes->get('acciones/show', 'Acciones::show',['filter' => 'auth']);
$routes->get('acciones/edit/(:any)', 'Acciones::edit/$1',['filter' => 'auth']);
$routes->get('acciones/new', 'Acciones::new',['filter' => 'auth']);

// Rutas Motivos Motivosilse
$routes->get('motivosilse', 'Motivosilse::show',['filter' => 'auth']);
$routes->get('motivosilse/show', 'Motivosilse::show',['filter' => 'auth']);
$routes->get('motivosilse/edit/(:any)', 'Motivosilse::edit/$1',['filter' => 'auth']);
$routes->get('motivosilse/new', 'Motivosilse::new',['filter' => 'auth']);

// Rutas Motivos Solinterpretes
$routes->get('solinterpretes', 'Solinterpretes::show',['filter' => 'auth']);
$routes->get('solinterpretes/show', 'Solinterpretes::show',['filter' => 'auth']);
$routes->get('solinterpretes/edit/(:any)', 'Solinterpretes::edit/$1',['filter' => 'auth']);
$routes->get('solinterpretes/new', 'Solinterpretes::new',['filter' => 'auth']);

// Rutas Formularios Solicitud interpretes
$routes->get('forminterpretes', 'Solinterpretes::formulario');

// Rutas Disponibilidad Interpretes
$routes->get('dispinterpretes', 'Dispinterpretes::show',['filter' => 'auth']);
$routes->get('dispinterpretes/show', 'Dispinterpretes::show',['filter' => 'auth']);
$routes->get('dispinterpretes/edit/(:any)', 'Dispinterpretes::edit/$1',['filter' => 'auth']);
$routes->get('dispinterpretes/new', 'Dispinterpretes::new',['filter' => 'auth']);

// Rutas Asignación interpretes
$routes->get('asigninterpretes', 'Asigninterpretes::show',['filter' => 'auth']);
$routes->get('asigninterpretes/show', 'Asigninterpretes::show',['filter' => 'auth']);
$routes->get('asigninterpretes/edit/(:any)', 'Asigninterpretes::edit/$1',['filter' => 'auth']);
$routes->get('asigninterpretes/new/(:any)', 'Asigninterpretes::new/$1',['filter' => 'auth']);


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
