<div class="container-fluid">

<?php
titleBreadcum('Editar ' . $data[0]->Descripcion,' Situaciones Acciones / Editar');
$slug = "sitacc" ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <?php                           
            if(session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
            <?php endif;
            if(session()->get('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?= session()->get('error') ?>
                </div>
            <?php endif; ?>
            <form class="" action="<?= base_url() ?>/<?= $slug ?>/edit/<?= $id ?>" method="post">
                <div class="form-row">
                    <div class="col-md-12">
                        <!-- Campo Descripción -->
                        <div class="form-group">
                            <label class="small mb-1" for="description">Descripción</label>
                            <input class="form-control py-2" id="description" name="description" type="text"
                                placeholder="Introduce descripción" value="<?= $data[0]->Descripcion ?>" />
                                <input class="form-control py-2" id="id" name="id" type="hidden"
                                placeholder="Introduce tú nombre" value="" />
                        </div>
                    </div>

                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="col-12 form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                </div>
            </form>
        </div>
    </div>
</div>