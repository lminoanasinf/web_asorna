<div class="container-fluid">
    <?= // Miga de pan 
    helper('html');
    ?>

    <?php 
    // Miga de pan
    $titulo = "Autonomia personal";
    $migadepan = "Mantenimento / " . $titulo;
    // Tabla
    $idTabla = 'autonomiapersonal';
    $urlApi = 'api/getAutonomiapersonal';

    titleBreadcum($titulo,$migadepan);
                              
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; 

    dataTable($titulo,$columns,$urlApi,$idTabla,'2,3','text-center',12); 
    
    ?>

<script>
   
</script>   