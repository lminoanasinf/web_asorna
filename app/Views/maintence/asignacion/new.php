<div class="container-fluid">

    <?php titleBreadcum('Asignación de interprete nueva','Asignación de interprete / Nuevo') ?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <?php                           
            if(session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
                <?php endif;
            if(session()->get('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?= session()->get('error') ?>
                </div>
                <?php endif; ?>
                <form class="" action="<?=base_url()?>/asigninterpretes/new" method="POST">
                    <div class="form-row">
                        <div class="col-md-6">
                            <!-- Campo Silse -->
                            <div class="form-group">
                                <label for="silse">Solicitudes</label>
                                <select multiple class="form-control" name="silse" id="silse" size="10">
                                    <?php foreach($silses as $item){ ?>

                                    <option <?php if(isset($id) && $id == $item['id']){ echo 'selected';} ?>
                                        value="<?= $item['id'] ?>">
                                        <?= $item['lugar'] . " - ". invert_fecha($item['fecha_accion'],true) ?></option>

                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- Campo Ilse -->
                            <div class="form-group">
                                <label for="ilse">Ilse</label>
                                <select multiple class="form-control" name="ilse" id="ilse" size="10">
                                    <?php foreach($ilses as $item){ ?>
                                    <option value="<?= $item['id'] ?>">
                                        <?= $item['name'] ." ". $item['lastname'] ." ".  $item['lastlastname'] ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                    </div>

                    <!-- Errores de formulario -->
                    <?php if (isset($validation)){ ?>
                    <div class="col-12">
                        <div class="alert alert-danger" role="alert">
                            <?= $validation->listErrors() ?>
                        </div>
                    </div>
                    <?php } ?>

                    <div class="form-group mt-4 mb-0">
                        <button class="btn btn-primary btn-block" type="submit">Crear</a>
                    </div>
                </form>
            </div>
        </div>
    </div>