<div class="container-fluid">
    <?= // Miga de pan 
    helper('html');
    ?>

    <?php // Miga de pan
    
    titleBreadcum('Asignación de interpretes','Interpretes / Asignación');
                              
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; 

    dataTable("Solicitudes asignadas",$columns,'api/getAsignacion','asignacion','3,4','text-center',12); 
                    
    dataTable("Solicitudes NO asignadas",$columns,'api/getNoAsignacion','noasignacion','8','text-center',12,false,0,'noAsig','0,9'); 
    
    ?>

<script>
   
</script>   