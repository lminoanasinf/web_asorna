<div class="container-fluid">

<?php
$titulo = "Acciones";
titleBreadcum('Editar ' . $titulo ,$titulo.' / Editar');
$slug = "acciones"; ?>

<div class="col-12">
    <div class="row ">
        <div class="col-12">
        <?php if(session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
            <?php endif;
            if(session()->get('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?= session()->get('error') ?>
                </div>
            <?php endif; ?>
            <form class="" action="<?= base_url() ?>/<?= $slug ?>/edit/<?= $id ?>" method="post" novalidate>
                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Grupal -->
                        <div class="form-group">
                            <label class="small mb-1" for="grupal">Grupal</label>
                            <select class="form-control py-2" id="grupal" name="grupal">
                                <option id="grupal" value="1" <?php if($data[0]->Grupal === "1"){ echo 'selected';} ?>>Sí</option>
                                <option id="grupal" value="0"  <?php if($data[0]->Grupal === "0"){ echo 'selected';} ?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Solicitud -->
                        <div class="form-group">
                            <label class="small mb-1" for="solicitud">Fecha Solicitud</label>
                            <input class="form-control py-2" id="solicitud" name="solicitud" type="date"
                                placeholder="Introduce solicitud"  value="<?php campo_fecha($data[0]->Solicitud,false) ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Persona -->
                        <div class="form-group">
                            <label class="small mb-1" for="persona">Solicitante</label>
                            <select  class="form-control selectpicker" id="persona" name="persona" data-live-search="true" data-live-search-placeholder="Persona">
                                    <?php 
                                    foreach($personas as $item){
                                        ?>
                                            <option data-tokens="<?= $item->Nombre . " " . $item->ApellidoUno . " " . $item->ApellidoDos . " - " . $item->Movil ?>" value="<?= $item->ID ?>" <?php if($data[0]->IdPersona == $item->ID){ echo 'selected';}?>><?= $item->Nombre . " " . $item->ApellidoUno . " " . $item->ApellidoDos . " - " . $item->Movil ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Proyecto -->
                        <div class="form-group">
                            <label class="small mb-1" for="proyecto">Proyecto</label>
                            <input class="form-control py-2" id="proyecto" name="proyecto" type="text"
                                placeholder="Introduce proyecto" value="<?= $data[0]->Proyecto ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Servicio -->
                        <div class="form-group">
                            <label class="small mb-1" for="servicio">Servicio</label>
                            <select style="border:1px solid grey"  class="form-control selectpicker" id="servicio" name="servicio" data-live-search="true" data-live-search-placeholder="Servicio">
                                    <?php 
                                    foreach($servicios as $item){
                                        ?>
                                            <option data-tokens="<?= $item->Descripcion ?>" value="<?= $item->ID ?>" <?php if($data[0]->IdServicios == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Acciones -->
                        <div class="form-group">
                            <label class="small mb-1" for="acciones">Accion</label>
                            <select class="form-control py-2" id="acciones" name="acciones">
                                    <?php 
                                    foreach($acciones as $item){
                                        ?>
                                            <option value="<?= $item->ID ?>" <?php if($data[0]->IdAcciones == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Responsable -->
                        <div class="form-group">
                            <label class="small mb-1" for="responsable">Responsable</label>
                            <select  class="form-control selectpicker" id="responsable" name="responsable" data-live-search="true" data-live-search-placeholder="Persona">
                                    <?php 
                                    foreach($personas as $item){
                                        ?>
                                            <option data-tokens="<?= $item->Nombre . " " . $item->ApellidoUno . " " . $item->ApellidoDos . " - " . $item->Movil ?>" value="<?= $item->ID ?>" <?php if($data[0]->Responsable == $item->ID){ echo 'selected';}?>><?= $item->Nombre . " " . $item->ApellidoUno . " " . $item->ApellidoDos . " - " . $item->Movil ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Fecha -->
                        <div class="form-group">
                            <label class="small mb-1" for="fecha">Fecha prevista</label>
                            <input class="form-control py-2" id="fecha" name="fecha" type="date"
                                placeholder="Introduce fecha" value="<?php campo_fecha($data[0]->Fecha,false) ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Lugar -->
                        <div class="form-group">
                            <label class="small mb-1" for="lugar">Lugar</label>
                            <input class="form-control py-2" id="lugar" name="lugar" type="text"
                                placeholder="Introduce lugar"  value="<?= $data[0]->Lugar ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Inicio -->
                        <div class="form-group">
                            <label class="small mb-1" for="inicio">Inicio</label>
                            <input class="form-control py-2" id="inicio" name="inicio" type="time"
                                 value="<?= $data[0]->Inicio ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Final -->
                        <div class="form-group">
                            <label class="small mb-1" for="final">Final</label>
                            <input class="form-control py-2" id="final" name="final" type="time"
                                 value="<?php echo $data[0]->Final ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Observaciones -->
                        <div class="form-group">
                            <label class="small mb-1" for="observaciones">Observaciones</label>
                            <input class="form-control py-2" id="observaciones" name="observaciones" type="text"
                                placeholder="Introduce observaciones" value="<?= $data[0]->Observaciones ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Socio -->
                        <div class="form-group">
                            <label class="small mb-1" for="socio">Socio</label>
                            <select class="form-control py-2" id="socio" name="socio">
                                <option id="socio" value="1" <?php if($data[0]->Socio === "1"){ echo 'selected';} ?>>Sí</option>
                                <option id="socio" value="0"  <?php if($data[0]->Socio === "0"){ echo 'selected';} ?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Facturar -->
                        <div class="form-group">
                            <label class="small mb-1" for="facturar">Facturar</label>
                            <select class="form-control py-2" id="facturar" name="facturar">
                                <option id="facturar" value="1" <?php if($data[0]->Facturar === "1"){ echo 'selected';} ?>>Sí</option>
                                <option id="facturar" value="0"  <?php if($data[0]->Facturar === "0"){ echo 'selected';} ?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Online -->
                        <div class="form-group">
                            <label class="small mb-1" for="online">On-line</label>
                            <select class="form-control py-2" id="online" name="online">
                                    <?php 
                                    foreach($online as $item){
                                        if($item->ID > 4) {
                                        ?>
                                            <option value="<?= $item->ID ?>" <?php if(set_value('online') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                <div class="col-md-4">
                        <!-- Campo Situaciones -->
                        <div class="form-group">
                            <label class="small mb-1" for="situaciones">Situaciones</label>
                            <select class="form-control py-2" id="situaciones" name="situaciones">
                                    <?php 
                                    foreach($situaciones as $item){
                                        ?>
                                            <option value="<?= $item->ID ?>" <?php if($data[0]->Situaciones == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Motivo -->
                        <div class="form-group">
                            <label class="small mb-1" for="motivo">Motivo</label>
                            <select class="form-control py-2" id="motivo" name="motivo">
                                    <?php 
                                    foreach($motivos as $item){
                                        ?>
                                            <option value="<?= $item->ID ?>" <?php if($data[0]->Motivo == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                
                </div>
                 
                <div class="form-row" id="asistentes_especificos">
                    <fieldset class="scheduler-border col-12">
                        <legend class="scheduler-border">Asistentes</legend>
                        <label>Especifico</label>
                        <select id="boton_especifico" class="form-control">
                            <option value="0">No</option>
                            <option value="1">Sí</option>
                        </select>
                        <div id="especifico">
                            <div class="form-group">
                                <label for="sociosasis">Socios Asistentes</label>
                                <select multiple class="form-control" name="sociosasis[]" id="sociosasis" size="10">
                                <?php foreach($socios as $item){ 
                                    if(array_search($item->ID,$asistentesIDS) !== false){ ?>
                                            <option selected value="<?= $item->ID ?>"><?= $item->Nombre . " " . $item->ApellidoUno . " " . $item->ApellidoDos . " - " . $item->Movil ?></option>
                                    <?php 
                                    
                                        } else { ?>
                                            <option value="<?= $item->ID ?>"><?= $item->Nombre . " " . $item->ApellidoUno . " " . $item->ApellidoDos . " - " . $item->Movil ?></option>

                                        <?php }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div id="general">
                            <div class="col-12 ">
                                <div class="form-group col-12">
                                    <label class="small mb-1" for="hombres">Hombres</label>
                                    <div id="lineaH" class="form-inline col-12 mt-2 ">
                                        <input type="number" id="hombres" placeholder="Número de hombres" class="form-control" name="hombres1" value="<?php set_value('hombres') ?>">
                                        <select class="form-control py-2 ml-2" id="edad" name="edadh1" >
                                                <?php 
                                                foreach($edades as $item){
                                                    ?>
                                                        <option value="<?= $item->ID ?>" <?php if(set_value('edad') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                                    <?php
                                                }
                                                ?>
                                        </select>
                                    </div>
                                    <div id="lineaH" class="form-inline col-12 mt-2 ">
                                        <input type="number" id="hombres" placeholder="Número de hombres" class="form-control" name="hombres2" value="<?php set_value('hombres') ?>">
                                        <select class="form-control py-2 ml-2" id="edad" name="edadh2" >
                                                <?php 
                                                foreach($edades as $item){
                                                    ?>
                                                        <option value="<?= $item->ID ?>" <?php if(set_value('edad') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                                    <?php
                                                }
                                                ?>
                                        </select>
                                    </div>
                                    <div id="lineaH" class="form-inline col-12 mt-2 ">
                                        <input type="number" id="hombres" placeholder="Número de hombres" class="form-control" name="hombres3" value="<?php set_value('hombres') ?>">
                                        <select class="form-control py-2 ml-2" id="edad" name="edadh3" >
                                                <?php 
                                                foreach($edades as $item){
                                                    ?>
                                                        <option value="<?= $item->ID ?>" <?php if(set_value('edad') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                                    <?php
                                                }
                                                ?>
                                        </select>
                                    </div>
                                    <div id="lineaH" class="form-inline col-12 mt-2 ">
                                        <input type="number" id="hombres" placeholder="Número de hombres" class="form-control" name="hombres4" value="<?php set_value('hombres') ?>">
                                        <select class="form-control py-2 ml-2" id="edad" name="edadh4" >
                                                <?php 
                                                foreach($edades as $item){
                                                    ?>
                                                        <option value="<?= $item->ID ?>" <?php if(set_value('edad') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                                    <?php
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-12">
                                    <label class="small mb-1"  for="mujeres">Mujeres</label>
                                    <div id="lineaM" class="form-inline col-12 mt-2 ">
                                        <input type="number" id="mujeres" placeholder="Número de Mujeres" class="form-control" name="mujeres1" value="<?php set_value('mujeres') ?>">
                                        <select class="form-control py-2 ml-2" id="edad" name="edadv1" >
                                            <?php 
                                            foreach($edades as $item){
                                                ?>
                                                    <option value="<?= $item->ID ?>" <?php if(set_value('edad') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div id="lineaM" class="form-inline col-12 mt-2 ">
                                        <input type="number" id="mujeres" placeholder="Número de Mujeres" class="form-control" name="mujeres2" value="<?php set_value('mujeres') ?>">
                                        <select class="form-control py-2 ml-2" id="edad" name="edadv2" >
                                            <?php 
                                            foreach($edades as $item){
                                                ?>
                                                    <option value="<?= $item->ID ?>" <?php if(set_value('edad') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div id="lineaM" class="form-inline col-12 mt-2 ">
                                        <input type="number" id="mujeres" placeholder="Número de Mujeres" class="form-control" name="mujeres3" value="<?php set_value('mujeres') ?>">
                                        <select class="form-control py-2 ml-2" id="edad" name="edadv3" >
                                            <?php 
                                            foreach($edades as $item){
                                                ?>
                                                    <option value="<?= $item->ID ?>" <?php if(set_value('edad') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div id="lineaM" class="form-inline col-12 mt-2 ">
                                        <input type="number" id="mujeres" placeholder="Número de Mujeres" class="form-control" name="mujeres4" value="<?php set_value('mujeres') ?>">
                                        <select class="form-control py-2 ml-2" id="edad" name="edadv4" >
                                            <?php 
                                            foreach($edades as $item){
                                                ?>
                                                    <option value="<?= $item->ID ?>" <?php if(set_value('edad') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </fieldset>
                </div>
                <div class="col-12 form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                </div>
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var grupal = <?= $data[0]->Grupal ?>;
        var asistentes = <?php if(sizeof($asistentes) != 0){echo "true";} ?>;
        console.log(asistentes);
        if(grupal == 1) {
            $("#asistentes_especificos").show();
            if(asistentes == true){
                $("#especifico").show();
            } else {
                $("#especifico").hide();
                $("#sociosasis > option:selected").prop("selected",false);
            }
        } else {
            $("#asistentes_especificos").hide();
        }
        
        $("#grupal").on('click',function(){
            if($("#grupal").val() == 1){
                $("#asistentes_especificos").show();
                $("#general").show();
                $("#especifico").hide();
            } else {
                $("#asistentes_especificos").hide();
            }
        });

        $("#socio").on('click',function(){
            if($("#socio").val() == 1 && $("#grupal").val() == 1){
                $("#asistentes_especificos").show();
                $("#especificos").hide();
            } else {
                $("#asistentes_socios").hide();
            }
        });

        $("#boton_especifico").on('click',function(){
            if($("#boton_especifico").val() == 1){
                $("#general").show();
                $("#especifico").show();
            } else {
                $("#sociosasis > option:selected").prop("selected",false);
                $("#hombres").val("");
                $("#mujeres").val("");
                $("#especifico").hide();
            }
        });

    });
</script>