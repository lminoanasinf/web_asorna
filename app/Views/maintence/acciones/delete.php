<div class="container-fluid">

<?php
$titulo = "Acciones";
titleBreadcum('Cancelar ' . $titulo ,$titulo.' / Editar');
$slug = "acciones"; ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <?php                           
            if(session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
            <?php endif;
            if(session()->get('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?= session()->get('error') ?>
                </div>
            <?php endif; ?>
            <form class="" action="<?= base_url() ?>/<?= $slug ?>/delete/<?= $id ?>" method="post">
                
                <div class="form-row">
                    <div class="col-md-6">
                        <!-- Campo Situaciones -->
                        <div class="form-group">
                            <label class="small mb-1" for="situaciones">Situaciones</label>
                            <select class="form-control py-2" id="situaciones" name="situaciones">
                                    <?php 
                                    foreach($situaciones as $item){
                                        if($item->ID != 1){
                                        ?>
                                            <option value="<?= $item->ID ?>" <?php if($data[0]->Situaciones == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php }
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- Campo Motivo -->
                        <div class="form-group">
                            <label class="small mb-1" for="motivo">Motivo</label>
                            <select class="form-control py-2" id="motivo" name="motivo">
                                    <?php 
                                            foreach($motivos as $item){
                                        if($item->ID != 1){
                                            ?>
                                            <option value="<?= $item->ID ?>" <?php if($data[0]->Motivo == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php 
                                    }}
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="col-12 form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Cancelar acción</a>
                </div>
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>
            </form>
            <form action="<?=base_url()?>/<?= $slug ?>/fulldelete/<?= $id ?>">
                <div class="col-12 form-group mt-4 mb-0">
                    <button class="btn btn-danger btn-block" type="submit">Eliminar acción</a>
                </div>
            </form>
        </div>
    </div>
</div>