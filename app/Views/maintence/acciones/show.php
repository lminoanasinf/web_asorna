<div class="container-fluid">
    <?= // Miga de pan 
    helper('html');
    ?>

    <?php 
    // Miga de pan
    $titulo = "Acciones";
    $migadepan = "Mantenimento / " . $titulo;
    // Tabla
    $idTabla = 'acciones';
    $urlApi = 'api/getAcciones'; 

    titleBreadcum($titulo,$migadepan);
                              
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; 
                $columns[0]->Asistentes = "Asistentes";
                $columns[0]->Mujeres = "Mujeres";
                $columns[0]->Hombres = "Hombres";
    dataTable($titulo,$columns,$urlApi,$idTabla,'20,21','text-center',12,true,16,'dataTable','0,2,7'); 
    
    ?>

<script>
   
</script>   