<div class="container-fluid">

    <?php

$titulo = "Parte de trabajo";
titleBreadcum('Editar ' . $titulo ,$titulo.' / Editar');
$slug = "partetrabajo"; 
$data[0] = (array) $data[0];

if($data[0]['Verificado'] = "Si"){
    $readOnly = 'readonly';
} else {
    $readOnly = '';
}

?>

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <?php                           
            if(session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php session()->get('success') ?>
                </div>
                <?php endif;
            if(session()->get('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?php session()->get('error') ?>
                </div>
                <?php endif; 
            
             ?>
                <div class="row col-12">

                    <div class="col-md-4">
                        <!-- Campo tipousuario -->
                        <div class="form-group">
                            <label class="small mb-1" for="tipousuario">Tipo de Usuario</label>
                            <input class="form-control py-2" type="text" name="tipousuario" readonly id="tipousuario"
                                value="<?php echo $data[0]['Tipo Usuario'] ?>">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- Campo numerosocio -->
                        <div class="form-group">
                            <label class="small mb-1" for="numerosocio">Nº Socio</label>
                            <input class="form-control py-2" type="text" name="numerosocio" readonly id="numerosocio"
                                value="<?php echo $data[0]['NºSocio'] == '0' ? 'No es socio' : $data[0]['NºSocio'] ?>">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- Campo Servicio -->
                        <div class="form-group">
                            <label class="small mb-1" for="servicio">Servicio</label>
                            <input class="form-control py-2" type="text" name="servicio" readonly id="servicio"
                                value="<?php echo $data[0]['Servicio'] ?>">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- Campo Usuario -->
                        <div class="form-group">
                            <label class="small mb-1" for="nombre">Nombre</label>
                            <input class="form-control py-2" type="text" name="nombre" readonly id="nombre"
                                value="<?php echo $data[0]['Usuario'] ?>">
                        </div>
                    </div>
                </div>

                <hr>

                <form class="" action="<?= base_url() ?>/<?= $slug ?>/edit/<?= $id ?>" method="post" novalidate>
                    <div class="form-row">
                        <div class="col-md-2">
                            <!-- Campo Verificado -->
                            <div class="form-group">
                                <label class="small mb-1" for="Verificado">Verificado</label>
                                <select <?= $readOnly ?> class="form-control selectpciker py-2" id="verificado" name="verificado">
                                    <option value="Si" <?php if($data[0]['Verificado'] == 'Si'){ echo 'selected';} ?>>Sí
                                    </option>
                                    <option value="No" <?php if($data[0]['Verificado'] == 'No'){ echo 'selected';} ?>>No
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <!-- Campo Duración -->
                            <div class="form-group">
                                <label class="small mb-1" for="duracion">Duración</label>
                                <input <?= $readOnly ?> class="form-control py-2" id="duracion" name="duracion" type="number"
                                    value="<?php echo $data[0]['Duración'] ?>" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Grupal -->
                            <div class="form-group">
                                <label class="small mb-1" for="grupal">Grupal</label>
                                <input class="form-control py-2" type="text" readonly
                                    value="<?php if($data){echo 'Sí';} else { echo 'No';}?>" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Situacion -->
                            <div class="form-group">
                                <label class="small mb-1" for="situacion">Situación</label>
                                <input class="form-control py-2" id="situacion" name="situacion" readonly type="text"
                                    value="<?php echo $data[0]['Situación'] ?>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- Campo Proyecto -->
                            <div class="form-group">
                                <label class="small mb-1" for="proyecto">Proyecto</label>
                                <input class="form-control py-2" id="proyecto" name="proyecto" type="text" readonly
                                    value="<?php echo $data[0]['Proyecto']; ?>" />
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-4">
                            <!-- Campo Responsable -->
                            <div class="form-group">
                                <label class="small mb-1" for="responsable">Interprete</label>
                                <input class="form-control py-2" id="responsable" name="responsable" readonly
                                    type="text" value="<?php echo $data[0]["Interprete"]; ?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Fecha Solicitud -->
                            <div class="form-group">
                                <label class="small mb-1" for="fechasolicitud">Fecha Solicitud</label>
                                <input class="form-control py-2" type="text" name="fechasolicitud" id="fechasolicitud"
                                    readonly
                                    value="<?= invert_fecha(str_replace('00:00:00','',$data[0]['Solicitud']));?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Hora inicio -->
                            <div class="form-group">
                                <label class="small mb-1" for="horainicio">Hora Inicio</label>
                                <input <?= $readOnly ?> class="form-control py-2" type="text" name="horainicio" id="horainicio" readonly
                                    value="<?= $data[0]['Inicio'];?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Fecha Solicitud -->
                            <div class="form-group">
                                <label class="small mb-1" for="horafin">Hora Fin</label>
                                <input <?= $readOnly ?> class="form-control py-2" type="text" name="horafin" id="horafin" readonly
                                    value="<?= $data[0]['Fin'];?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Motivo -->
                            <div class="form-group">
                                <label class="small mb-1" for="motivo">Motivo</label>
                                <input class="form-control py-2" type="text" name="motivo" id="motivo" readonly
                                    value="<?= $data[0]['Motivo'];?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-4">
                            <!-- Campo Lugar -->
                            <div class="form-group">
                                <label class="small mb-1" for="direccion">Direccion</label>
                                <input class="form-control py-2" id="direccion" name="direccion" type="text"
                                    readonly value="<?= $data[0]['Dirección'] ?>" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Unidades -->
                            <div class="form-group">
                                <label class="small mb-1" for="unidades">Nº Unidades </label>
                                <input <?= $readOnly ?> class="form-control py-2" id="unidades" name="unidades" type="number"
                                    value="<?= $data[0]['NºUnidades'] ?>" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <!-- Campo PAX -->
                            <div class="form-group">
                                <label class="small mb-1" for="PAX">PAX </label>
                                <input <?= $readOnly ?> class="form-control py-2" id="PAX" name="PAX" type="number"
                                    value="<?= $data[0]['PAX'] ?>" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <!-- Campo PS -->
                            <div class="form-group">
                                <label class="small mb-1" for="PS">PS </label>
                                <input <?= $readOnly ?> class="form-control py-2" id="PS" name="PS" type="number"
                                    value="<?= $data[0]['PS'] ?>" />
                            </div>
                        </div>

                        <div class="col-md-2">
                            <!-- Campo Desplazamiento Ida -->
                            <div class="form-group">
                                <label class="small mb-1" for="despIda">Desplazamiento Ida </label>
                                <input <?= $readOnly ?> class="form-control py-2" id="despIda" name="despIda" type="number"
                                    value="<?= $data[0]['Desplazamiento Ida'] ?>" />
                            </div>
                        </div>

                        <div class="col-md-2">
                            <!-- Campo Desplazamiento Vuelta -->
                            <div class="form-group">
                                <label class="small mb-1" for="despVuelta">Desplazamiento Vuelta </label>
                                <input <?= $readOnly ?> class="form-control py-2" id="despVuelta" name="despVuelta" type="number"
                                    value="<?= $data[0]['Desplazamiento Vuelta'] ?>" />
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-6">
                            <!-- Campo Observaciones -->
                            <div class="form-group">
                                <label class="small mb-1" for="observaciones">Observaciones</label>
                                <textarea  <?= $readOnly ?> class="form-control py-2" id="observaciones" name="observaciones"
                                    ><?= $data[0]['Observaciones'] ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- Campo comunicacion -->
                            <div class="form-group">
                                <label class="small mb-1" for="comunicacion">comunicacion</label>
                                <textarea readonly class="form-control py-2" id="comunicacion" name="comunicacion"
                                    ><?= $data[0]['Comunicación'] ?></textarea>
                            </div>
                        </div>
                    <?php if($data[0]['Verificado'] == 'No'){ ?>

                    <div class="col-12 form-group mt-4 mb-0">
                        <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                    </div>

                    <?php } ?>
                    <!-- Errores de formulario -->
                    <?php if (isset($validation)){ ?>
                    <div class="col-12">
                        <div class="alert alert-danger" role="alert">
                            <?php // $validation->listErrors() ?>
                        </div>
                    </div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>