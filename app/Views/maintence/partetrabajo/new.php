<div class="container-fluid">

    <?php
$titulo = "Parte de trabajo";
titleBreadcum('Nueva ' . $titulo ,$titulo.' / Nuevo');
$slug = "partetrabajo";

?>

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <?php                           
            if(session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
                <?php endif;
            if(session()->get('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?= session()->get('error') ?>
                </div>
                <?php endif; ?>



                <form class="" action="<?= base_url() ?>/<?= $slug ?>/new" method="post" novalidate>
                    <div class="row col-12">
                        <select class="form-control selectpicker" name="id_ar" id="id_ar" data-live-search="true"
                            data-live-search-placeholder="Solicitud ILSE">
                            <option value="">Seleccionar una solicitud</option>
                            <?php foreach ($silse as $item) {
                            $fecha = invert_fecha(str_replace('00:00:00','',$item['fecha_accion']),true);

                        if(session()->get('role') == 1){
                            ?>
                            <option value="<?= $item['id'];?>">
                                <?= "Fecha: " . $fecha  . " - Hora Inicio: " . $item['hora_inicio'] . " - Hora Fin: " . $item['hora_final'] . " - Lugar: " . $item['lugar'] ;?>
                            </option>
                            <?php
                        }else if($item['responsable'] == session()->get('id')){
                        ?>
                            <option value="<?= $item['id'];?>">
                                <?= "Fecha: " . $fecha  . " - Hora Inicio: " . $item['hora_inicio'] . " - Hora Fin: " . $item['hora_final'] . " - Lugar: " . $item['lugar'] ;?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                        </select>
                    </div>
                    <hr>
                    <div class="row col-12">
                        <div class="col-md-4">
                            <!-- Campo tipousuario -->
                            <div class="form-group">
                                <label class="small mb-1" for="tipousuario">Tipo de Usuario</label>
                                <input class="form-control py-2" type="text" name="tipousuario" readonly
                                    id="tipousuario" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <!-- Campo numerosocio -->
                            <div class="form-group">
                                <label class="small mb-1" for="numerosocio">Nº Socio</label>
                                <input class="form-control py-2" type="text" name="numerosocio" readonly
                                    id="numerosocio" value="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Servicio -->
                            <div class="form-group">
                                <label class="small mb-1" for="servicio">Servicio</label>
                                <input class="form-control py-2" type="text" name="servicio" readonly id="servicio"
                                    value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <!-- Campo Usuario -->
                            <div class="form-group">
                                <label class="small mb-1" for="nombre">Nombre</label>
                                <input class="form-control py-2" type="text" name="nombre" readonly id="nombre"
                                    value="">
                                <input class="form-control py-2" type="hidden" name="idusuario" readonly id="idusuario"
                                    value="">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="form-row">
                        <div class="col-md-2">
                            <!-- Campo Verificado -->
                            <div class="form-group">
                                <label class="small mb-1" for="Verificado">Verificado</label>
                                <select class="form-control selectpciker py-2" id="verificado" name="verificado">
                                    <?php if(session()->get('role') == 1) { ?> <option value="Si">Sí
                                    </option> <?php } ?>
                                    <option <?php if(session()->get('role') != 1) { echo 'selected'; }?> value="No">No
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <!-- Campo Duración -->
                            <div class="form-group">
                                <label class="small mb-1" for="duracion">Duración</label>
                                <input class="form-control py-2" id="duracion" name="duracion" type="number" value="" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Grupal -->
                            <div class="form-group">
                                <label class="small mb-1" for="grupal">Grupal</label>
                                <input class="form-control py-2" type="text" id="grupal" readonly value="" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Situacion -->
                            <div class="form-group">
                                <label class="small mb-1" for="situacion">Situación</label>
                                <input class="form-control py-2" id="situacion" name="situacion" readonly type="text"
                                    value="" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- Campo Proyecto -->
                            <div class="form-group">
                                <label class="small mb-1" for="proyecto">Proyecto</label>
                                <input class="form-control py-2" id="proyecto" name="proyecto" type="text" readonly
                                    value="" />
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-4">
                            <!-- Campo Responsable -->
                            <div class="form-group">
                                <label class="small mb-1" for="responsable">Interprete</label>
                                <input class="form-control py-2" id="responsable" name="responsable" readonly
                                    type="text" value="">
                                <input class="form-control py-2" id="idresponsable" name="idresponsable" readonly
                                    type="hidden" value="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Fecha Solicitud -->
                            <div class="form-group">
                                <label class="small mb-1" for="fechasolicitud">Fecha Solicitud</label>
                                <input class="form-control py-2" type="text" name="fechasolicitud" id="fechasolicitud"
                                    readonly value="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Hora inicio -->
                            <div class="form-group">
                                <label class="small mb-1" for="horainicio">Hora Inicio</label>
                                <input class="form-control py-2" type="time" name="horainicio" id="horainicio" 
                                    value="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Fecha Solicitud -->
                            <div class="form-group">
                                <label class="small mb-1" for="horafin">Hora Fin</label>
                                <input class="form-control py-2" type="time" name="horafin" id="horafin" 
                                    value="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Motivo -->
                            <div class="form-group">
                                <label class="small mb-1" for="motivo">Motivo</label>
                                <input class="form-control py-2" type="text" name="motivo" id="motivo" readonly
                                    value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-4">
                            <!-- Campo Lugar -->
                            <div class="form-group">
                                <label class="small mb-1" for="direccion">Direccion</label>
                                <input class="form-control py-2" id="direccion" name="direccion" type="text" readonly
                                    value="" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Campo Unidades -->
                            <div class="form-group">
                                <label class="small mb-1" for="unidades">Nº Unidades </label>
                                <input class="form-control py-2" id="unidades" name="unidades" type="number" value="" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <!-- Campo PAX -->
                            <div class="form-group">
                                <label class="small mb-1" for="PAX">PAX </label>
                                <input class="form-control py-2" id="PAX" name="PAX" type="number" value="" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <!-- Campo PS -->
                            <div class="form-group">
                                <label class="small mb-1" for="PS">PS </label>
                                <input class="form-control py-2" id="PS" name="PS" type="number" value="" />
                            </div>
                        </div>

                        <div class="col-md-2">
                            <!-- Campo Desplazamiento Ida -->
                            <div class="form-group">
                                <label class="small mb-1" for="despIda">Desplazamiento Ida </label>
                                <input class="form-control py-2" id="despIda" name="despIda" type="number" value="" />
                            </div>
                        </div>

                        <div class="col-md-2">
                            <!-- Campo Desplazamiento Ida -->
                            <div class="form-group">
                                <label class="small mb-1" for="despVuelta">Desplazamiento Vuelta </label>
                                <input class="form-control py-2" id="despVuelta" name="despVuelta" type="number"
                                    value="" />
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-6">
                            <!-- Campo Observaciones -->
                            <div class="form-group">
                                <label class="small mb-1" for="observaciones">Observaciones</label>
                                <textarea class="form-control py-2" id="observaciones" name="observaciones"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- Campo comunicacion -->
                            <div class="form-group">
                                <label class="small mb-1" for="comunicacion">Comunicacion</label>
                                <textarea readonly class="form-control py-2" id="comunicacion"
                                    name="comunicacion"></textarea>
                            </div>
                        </div>

                        <div class="col-12 form-group mt-4 mb-0">
                            <button class="btn btn-primary btn-block" type="submit">Crear Parte de trabajo</a>
                        </div>
                        <!-- Errores de formulario -->
                        <?php if (isset($validation)){ ?>
                        <div class="col-12">
                            <div class="alert alert-danger" role="alert">
                                <?php // $validation->listErrors() ?>
                            </div>
                        </div>
                        <?php } ?>
                </form>
            </div>
        </div>
    </div>
    <script>
    $("#id_ar").on('change', function() {
        if($("#id_ar").val() != ""){
          $.ajax({
                method: "POST",
                url: "<?=base_url()?>/partetrabajo/datos",
                data: {
                    id: $("#id_ar").val()
                },
                dataType: "JSON"
            })
            .done(function(response) {
                $('#tipousuario').val(response['Tipo Usuario']);
                $('#numerosocio').val(response['NºSocio']);
                $('#servicio').val(response['Servicio']);
                $('#nombre').val(response['Nombre']);
                $('#idusuario').val(response['IdUsuario']);
                $('#grupal').val(response['Grupal']);
                $('#situacion').val(response['Situación']);
                $('#proyecto').val(response['Proyecto']);
                $('#responsable').val(response['Interprete']);
                $('#proyecto').val(response['Proyecto']);
                $('#idresponsable').val(response['IdResponsable']);
                var str = response['Solicitud'];
                $('#fechasolicitud').val(str.replace('00:00:00',''));
                $('#horainicio').val(response['Inicio']);
                $('#horafin').val(response['Fin']);
                $('#motivo').val(response['Motivo']);
                $('#direccion').val(response['Dirección']);
                $('#motivo').val(response['Motivo']);
                $('#comunicacion').val(response['Comunicación']);
            });  
        }
        
    });
    </script>