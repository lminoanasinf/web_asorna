<div class="container-fluid">
    <?= // Miga de pan 
    helper('html');
    ?>

    <?php 
    // Miga de pan
    $titulo = "Partes de trabajo";
    $migadepan = "Mantenimento / " . $titulo;
    // Tabla
    $idTabla = 'partetrabajo';
    $urlApi = 'api/getParteTrabajo'; 

    titleBreadcum($titulo,$migadepan);
                              
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; 
      

      dataTable($titulo,$columns,$urlApi,$idTabla,'27,28','text-center',12,false,0,'dateTable','0,1,2,3,4') 
    
    ?>

<script>
   
</script>   