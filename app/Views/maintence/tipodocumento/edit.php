<div class="container-fluid">

<?php
titleBreadcum('Editar ' . $data[0]->Descripcion,' Tipo Documento / Editar');
$slug = "tipodocumento" ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <?php                           
            if(session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
            <?php endif;
            if(session()->get('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?= session()->get('error') ?>
                </div>
            <?php endif; ?>
            <form class="" action="<?= base_url() ?>/<?= $slug ?>/edit/<?= $id ?>" method="post">
                <div class="form-row">
                    <div class="col-md-6">
                        <!-- Campo Descripción -->
                        <div class="form-group">
                            <label class="small mb-1" for="description">Descripción</label>
                            <input class="form-control py-2" id="description" name="description" type="text"
                                placeholder="Introduce descripción" value="<?= $data[0]->Descripcion ?>" />
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                            <!-- Campo Facturar -->
                            <div class="form-group">
                                <label class="small mb-1" for="service">Servicio</label>
                                <select class="form-control py-2" id="service" name="service">
                                    <?php 
                                    foreach($servicios as $item){
                                        ?>
                                            <option value="<?= $item->ID ?>" <?php if($data[0]->ServicioID == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                </div>
                <div class="form-row">
                    <div class="col-md-12">
                        <!-- Campo Documento -->
                        <div class="form-group">
                            <label class="small mb-1" for="documento">Documento</label>
                            <input class="form-control py-2" id="documento" name="documento" type="text"
                                placeholder="Introduce descripción" value="<?= $data[0]->Documento ?>" />
                        </div>
                    </div>
                </div>
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12"> 
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="col-12 form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                </div>
            </form>
        </div>
    </div>
</div>