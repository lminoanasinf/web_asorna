
<div class="container-fluid">
    <?= // Miga de pan 
    helper('html');
    ?>

    <?php 
    // Miga de pan
    $titulo = "Disponibilidad Interpretes";
    $migadepan = "Mantenimento / " . $titulo;
    // Tabla
    $idTabla = 'dispinterpretes';
    $urlApi = 'api/getDispinterpretes';

    titleBreadcum($titulo,$migadepan);
    helper('html');

    if(session()->get('success')): ?>
    <div class="alert alert-success" role="alert">
        <?= session()->get('success') ?>
    </div>
    <?php endif;
    if(session()->get('error')): ?>
    <div class="alert alert-danger" role="alert">
        <?= session()->get('error') ?>
    </div>
    <?php endif; ?>

    <div class="container col-12">
        <div class="row justify-content-center ">
            <div class="col-lg-12">
                <div class="card mb-4 ">
                    <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        <?= $titulo ?>
                    </div>
                    <div class="card-body col-12">

                        <!-- Modal -->
                        <div id="myModal" class="modal fade" role="dialog" tabindex="-1">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Editar disponibilidad</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                                    </div>
                                    <div class="modal-body">
                                        <form action="<?= base_url() ?>/dispinterpretes/nuevadisp" method="post" novalidate>
                                            <div class="row">
                                                <div class="col-12">
                                                    <label class="form-label" for="interprete">Interprete</label>
                                                    <select class="form-control" name="interprete" id="interprete">
                                                        <?php 
                                                            foreach($interpretes as $interprete){
                                                                ?>
                                                                    <option value="<?php echo $interprete['id'];?>"><?php echo $interprete['name']." ".$interprete['lastname']." ".$interprete['lastlastname']; ?></option>
                                                                <?php
                                                            }
                                                        
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <label class="form-label" for="fecha_inicio">Fecha
                                                            inicio</label>
                                                        <input class="form-control" type="date" name="fecha_inicio"
                                                            id="fecha_inicio">
                                                    </div>
                                                    <div class="col-6">
                                                        <label class="form-label" for="fecha_fin">Fecha fin</label>
                                                        <input class="form-control" type="date" name="fecha_fin"
                                                            id="fecha_fin">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <br>
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="dias[]"
                                                            id="L" value="L">
                                                        <label class="form-check-label" for="L">L</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="dias[]"
                                                            id="M" value="M">
                                                        <label class="form-check-label" for="M">M</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="dias[]"
                                                            id="X" value="X">
                                                        <label class="form-check-label" for="X">X</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="dias[]"
                                                            id="J" value="J">
                                                        <label class="form-check-label" for="J">J</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="dias[]"
                                                            id="V" value="V">
                                                        <label class="form-check-label" for="V">V</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="dias[]"
                                                            id="S" value="S">
                                                        <label class="form-check-label" for="S">S</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" name="dias[]"
                                                            id="D" value="D">
                                                        <label class="form-check-label" for="D">D</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-6">
                                                    <label class="form-label" for="hora_inicio">De hora:</label>
                                                    <input class="form-control" type="time" name="hora_inicio"
                                                        id="hora_inicio">
                                                </div>
                                                <div class="col-6">
                                                    <label class="form-label" for="hora_fin">A hora:</label>
                                                    <input class="form-control" type="time" name="hora_fin"
                                                        id="hora_fin">
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-12">
                                                    <label class="form-label" for="comentario">Comentario</label>
                                                    <textarea class="form-control" id="comentario" type="text" name="comentario"></textarea>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                    <div class="modal-footer text-center">
                                        <button id="guardar" type="button" class="btn btn-primary col-12">Guardar</button>
                                        <button id="borrar" type="button" class="btn btn-danger col-12" hidden>Borrar</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id='calendar-container'>
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    document.addEventListener('DOMContentLoaded', function() {
        var Calendar = FullCalendar.Calendar;
        var calendarEl = document.getElementById('calendar');
    

        // initialize the calendar
        // -----------------------------------------------------------------

        var calendar = new Calendar(calendarEl, {
            customButtons: {
                myCustomButton: {
                    text: 'Añadir',
                    click: function() {
                        $('#myModal').modal('show');
                        $('#borrar').prop('hidden',true);
                        
                        $('#guardar').on('click',function(){
                            var dias = "";

                            $.each($("input[type='checkbox']"), function(){
                            if($(this).prop("checked")){
                                dias = dias + $(this).val();
                            }
                            });
                            $.ajax({
                                method: "POST",
                                url: "<?=base_url()?>/dispinterpretes/nuevadisp",
                                data: {
                                    titulo: $("#interprete").text(),
                                    interprete: $("#interprete").val(),
                                    fecha_inicio: $("#fecha_inicio").val(),
                                    fecha_fin: $("#fecha_fin").val(),
                                    hora_inicio: $("#hora_inicio").val(),
                                    hora_fin: $("#hora_fin").val(),
                                    comentario: $("#comentario").val(),
                                    patron: dias
                                }
                            })
                            .done(function(msg) {
                                if(msg == 'redirect'){
                                    window.location.replace("<?= base_url() ?>/dispinterpretes");
                                }
                                $('#myModal').modal('hide');
                            });
                        });
                    }
                }
            },
            headerToolbar: {
                left: 'prev,next today myCustomButton',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            editable: true,
            events: [
                <?php 
                    $totalEventos = count($eventos);
                    $contador = 0;
                    foreach($eventos as $evento){
                        echo '{';
                        echo 'id:"'.$evento['id'].'",';
                        echo 'idinterprete:"'.$evento['id_user'].'",';
                        echo 'patron:"'.$evento['patron'].'",';
                        echo 'comentarios:"'.$evento['comentarios'].'",';
                        echo 'hora_inicio:"'.$evento['hora_inicio'].'",';
                        echo 'hora_fin:"'.$evento['hora_fin'].'",';
                        echo 'title:"'.$evento['titulo'].'",' ;
                        echo 'start:"'.$evento['fecha_inicio'].'",' ;
                        echo 'end:"'.$evento['fecha_fin'].'",' ;                       
                        echo 'backgroundColor:"'.$evento['color_fondo'].'",' ;                       
                        echo 'borderColor:"'.$evento['color_fondo'].'",' ;                       
                        echo 'textColor:"'.$evento['color_texto'].'",' ;                       
                        echo 'color:"'.$evento['color_fondo'].'",' ;           
                        echo 'eventDisplay: "block"' ;           
                        echo '}';
                        if($contador != $totalEventos){
                            echo ',';
                            $contador++;
                        } 
                    }
                    ?>
            ],
            eventClick: function(info) {
                console.log(info.event);

                var evento = info.event;
                var eventoesp = info.event.extendedProps;
                var finicio =  evento.start.getFullYear()+"-"+(("0" + (evento.start.getMonth() + 1)).slice(-2))+"-"+(("0" + evento.start.getDate()).slice(-2)) ;
                var ffin =  evento.end.getFullYear()+"-"+(("0" + (evento.end.getMonth() + 1)).slice(-2))+"-"+(("0" + evento.end.getDate()).slice(-2)) ;

                $('#fecha_inicio').val(finicio);
                $('#fecha_fin').val(ffin);
                $('#hora_inicio').val(eventoesp.hora_inicio);
                $('#hora_fin').val(eventoesp.hora_fin);
                $('#comentario').val(eventoesp.comentarios);
                $('#titulo').val(evento.title);
                $('#interprete option[velue="'+eventoesp.idinterprete+'"]').prop('selected',true);
                $('#interprete').prop('readonly',true);
                $('[type=checkbox]').each(function(i){
                    if(eventoesp.patron.includes($(this).val())){
                        $(this).prop('checked',true);
                    }
                });
                $('#myModal').modal('show');
                $('#borrar').prop('hidden',false);

                $('#myModal').on('hidden.bs.modal', function (e) {
                    $('#borrar').prop('hidden',true);
                    $('#interprete').prop('readonly',false);
                    $('#fecha_inicio').val('');
                    $('#fecha_fin').val('');
                    $('#hora_inicio').val('');
                    $('#hora_fin').val('');
                    $('#comentario').val('');
                    $('#titulo').val('');
                })

                $('#borrar').on('click',function(){
                    $.ajax({
                        method: "POST",
                        url: "<?=base_url()?>/dispinterpretes/borrardisp/"+evento.id,
                        data: {
                            id: evento.Id
                        }
                    })
                    .done(function(msg) {
                        alert("Se ha eliminado correctamente");
                        evento.remove();
                        $('#myModal').modal('hide');
                        $('#borrar').prop('hidden',true);
                    });
                   
                });
            },
            eventDropInfo: function(info) {
                var evento = info.event;
                var hours = evento.start.getHours(); //returns 0-23
                var minutes = evento.start.getMinutes(); //returns 0-59
                var seconds = evento.start.getSeconds(); //returns 0-59

                if(minutes<10){
                    minutesString = 0+minutes+""; //+""casts minutes to a string.
                } else {
                    minutesString = minutes;
                }

                if(seconds<10){
                    secondsString = 0+seconds+""; //+""casts seconds to a string.
                } else {
                    secondsString = seconds;
                }

                var hinicio = hours + ":" + minutesString + ":" + secondsString;

                var hours = evento.end.getHours(); //returns 0-23
                var minutes = evento.end.getMinutes(); //returns 0-59
                var seconds = evento.end.getSeconds(); //returns 0-59

                if(minutes<10){
                    minutesString = 0+minutes+""; //+""casts minutes to a string.
                } else {
                    minutesString = minutes;
                }
                
                if(seconds<10){
                    secondsString = 0+seconds+""; //+""casts seconds to a string.
                } else {
                    secondsString = seconds;
                }

                var hfin = hours + ":" + minutesString + ":" + secondsString;
                $.ajax({
                        method: "POST",
                        url: "<?=base_url()?>/dispinterpretes/actualizardisp",
                        data: {
                            titulo: "",
                            id_user: id_user,
                            fecha_inicio: evento.start,
                            fecha_fin: evento.end,
                            hora_inicio: hinicio,
                            hora_fin: "",
                            comentarios: "",
                            color_fondo: "",
                            color_texto: ""
                        }
                    })
                    .done(function(msg) {
                        alert("Disponibilidad actualizada");
                    });
            }
        });
        calendar.setOption('locale', 'es');

        calendar.render();
    });
    </script>