
<div class="container-fluid">

<?php
$titulo = "Solicitud interpretes";
titleBreadcum('Nueva ' . $titulo ,$titulo.' / Nuevo');
$slug = "solinterpretes"; ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <?php                           
            if(session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
            <?php endif;
            if(session()->get('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?= session()->get('error') ?>
                </div>
            <?php endif; ?>
            <form class="" action="<?= base_url() ?>/<?= $slug ?>/new" method="post">
                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Grupal -->
                        <div class="form-group">
                            <label class="small mb-1" for="grupal">Grupal</label>
                            <select class="form-control py-2" id="grupal" name="grupal">
                                <option id="grupal" value="1">Sí</option>
                                <option id="grupal" value="0" >No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Solicitud -->
                        <div class="form-group">
                            <label class="small mb-1" for="solicitud">Solicitud</label>
                            <input class="form-control py-2" id="solicitud" name="solicitud" type="date"
                                placeholder="Introduce solicitud" value="<?= set_value('solicitud')  ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Persona -->
                        <div class="form-group">
                            <label class="small mb-1" for="persona">Persona</label>
                            <select  class="form-control selectpicker" id="persona" name="persona" data-live-search="true" data-live-search-placeholder="Persona">
                                    <?php 
                                    foreach($personas as $item){
                                        ?>
                                            <option data-tokens="<?= $item->Nombre . " " . $item->ApellidoUno . " " . $item->ApellidoDos . " - " . $item->Movil ?>" value="<?= $item->ID ?>" ><?= $item->Nombre . " " . $item->ApellidoUno . " " . $item->ApellidoDos . " - " . $item->Movil ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Proyecto -->
                        <div class="form-group">
                            <label class="small mb-1" for="proyecto">Proyecto</label>
                            <input class="form-control py-2" id="proyecto" name="proyecto" type="text"
                                placeholder="Introduce proyecto" value="<?= set_value('proyecto') ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Servicio -->
                        <div class="form-group">
                            <label class="small mb-1" for="servicio">Servicio</label>
                            <select class="form-control py-2" id="servicio" name="servicio">
                                    <?php 
                                    foreach($servicios as $item){
                                        ?>
                                            <option value="<?= $item->ID ?>" <?php if(set_value('servicio') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Acciones -->
                        <div class="form-group">
                            <label class="small mb-1" for="acciones">Acciones</label>
                            <select class="form-control py-2" id="acciones" name="acciones">
                                    <?php 
                                    foreach($acciones as $item){
                                        ?>
                                            <option value="<?= $item->ID ?>" <?php if(set_value('acciones') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                <div class="col-md-4">
                        <!-- Campo Responsable -->
                        <div class="form-group">
                            <label class="small mb-1" for="responsable">Responsable</label>
                            <select  class="form-control selectpicker" id="responsable" name="responsable" data-live-search="true" data-live-search-placeholder="Persona">
                                    <?php 
                                    foreach($personas as $item){
                                        ?>
                                            <option data-tokens="<?= $item->Nombre . " " . $item->ApellidoUno . " " . $item->ApellidoDos . " - " . $item->Movil ?>" value="<?= $item->ID ?>" ><?= $item->Nombre . " " . $item->ApellidoUno . " " . $item->ApellidoDos . " - " . $item->Movil ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Fecha -->
                        <div class="form-group">
                            <label class="small mb-1" for="fecha">Fecha</label>
                            <input class="form-control py-2" id="fecha" name="fecha" type="date"
                                placeholder="Introduce fecha" value="<?= set_value('fecha') ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Lugar -->
                        <div class="form-group">
                            <label class="small mb-1" for="lugar">Lugar</label>
                            <input class="form-control py-2" id="lugar" name="lugar" type="text"
                                placeholder="Introduce lugar" value="<?= set_value('lugar') ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Inicio -->
                        <div class="form-group">
                            <label class="small mb-1" for="inicio">Inicio</label>
                            <input class="form-control py-2" id="inicio" name="inicio" type="time"
                                placeholder="Introduce inicio" value="<?= set_value('inicio') ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Final -->
                        <div class="form-group">
                            <label class="small mb-1" for="final">Final</label>
                            <input class="form-control py-2" id="final" name="final" type="time"
                                placeholder="Introduce final" value="<?= set_value('final') ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Online -->
                        <div class="form-group">
                            <label class="small mb-1" for="online">On-line</label>
                            <select class="form-control py-2" id="online" name="online">
                                    <?php 
                                    foreach($online as $item){
                                        if($item->ID > 4) {
                                        ?>
                                            <option value="<?= $item->ID ?>" <?php if(set_value('online') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Socio -->
                        <div class="form-group">
                            <label class="small mb-1" for="socio">Socio</label>
                            <select class="form-control py-2" id="socio" name="socio">
                                <option id="socio" value="1" <?php if(set_value('asistentes') === "1"){ echo 'selected';} ?>>Sí</option>
                                <option id="socio" value="0"  <?php if(set_value('asistentes') === "0"){ echo 'selected';} ?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Facturar -->
                        <div class="form-group">
                            <label class="small mb-1" for="facturar">Facturar</label>
                            <select class="form-control py-2" id="facturar" name="facturar">
                                <option id="facturar" value="1" <?php if(set_value('asistentes') === "1"){ echo 'selected';} ?>>Sí</option>
                                <option id="facturar" value="0"  <?php if(set_value('asistentes') === "0"){ echo 'selected';} ?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Observaciones -->
                        <div class="form-group">
                            <label class="small mb-1" for="observaciones">Observaciones</label>
                            <input class="form-control py-2" id="observaciones" name="observaciones" type="text"
                                placeholder="Introduce observaciones" value="<?= set_value('observaciones') ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-row" id="asistentes_especificos">
                    <fieldset class="scheduler-border col-12">
                        <legend class="scheduler-border">Asistentes</legend>
                        <label>Especifico</label>
                        <select id="boton_especifico" name="boton_especifico" class="form-control">
                            <option value="0">No</option>
                            <option value="1">Sí</option>
                        </select>
                        <div id="especifico">
                            <div class="form-group">
                                <label for="sociosasis">Socios Asistentes</label>
                                <select multiple class="form-control" name="sociosasis[]" id="sociosasis" size="10">
                                <?php foreach($socios as $item){ ?>
                                            <option value="<?= $item->ID ?>"><?= $item->Nombre . " " . $item->ApellidoUno . " " . $item->ApellidoDos . " - " . $item->Movil ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div id="general">
                            <div class="col-12 ">
                                <div class="form-group col-12">
                                    <label class="small mb-1" for="hombres">Hombres</label>
                                    <input type="number" id="hombres" placeholder="Número de hombres" class="form-control" name="hombres" value="<?php set_value('hombres') ?>">
                                </div>
                                <div class="form-group col-12">
                                    <label class="small mb-1"  for="mujeres">Mujeres</label>
                                    <input type="number" id="mujeres" placeholder="Número de Mujeres" class="form-control" name="mujeres" value="<?php set_value('mujeres') ?>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <!-- Campo Edad -->
                                <div class="form-group">
                                    <label class="small mb-1" for="edad">Rango de edad media</label>
                                    <select class="form-control py-2" id="edad" name="edad" >
                                            <?php 
                                            foreach($edades as $item){
                                                ?>
                                                    <option value="<?= $item->ID ?>" <?php if(set_value('edad') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                                <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-12 form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                </div>
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#general").hide();
        $("#especifico").hide();
       
        $("#grupal").on('click',function(){
            if($("#grupal").val() == 1){
                $("#asistentes_especificos").show();
                $("#general").show();
                $("#especifico").hide();
            } else {
                $("#asistentes_especificos").hide();
            }
        });

        $("#socio").on('click',function(){
            if($("#socio").val() == 1 && $("#grupal").val() == 1){
                $("#asistentes_especificos").show();
                $("#especificos").hide();
            } else {
                $("#asistentes_socios").hide();
            }
        });

        $("#boton_especifico").on('click',function(){
            if($("#boton_especifico").val() == 1){
                if($('#socio').val() == 1){
                    $("#general").show();
                $("#especifico").show();
                } else {
                    alert("Solo puede ser especifico si la casilla Socios está activada");
                }
                
            } else {
                $("#sociosasis > option:selected").prop("selected",false);
                $("#hombres").val("");
                $("#mujeres").val("");
                $("#especifico").hide();
            }
        });


        $("#grupal").val(0);
        $("#socio").val(0);

        $("#asistentes_socios").hide();
        $("#asistentes_especificos").hide();
        $("#especifico").hide();

    });
</script>