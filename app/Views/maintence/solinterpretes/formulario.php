
<div class="container-fluid">

<?php
$titulo = "Solicitud interprete";
titleBreadcum('Nueva ' . $titulo ,$titulo.' / Formulario');
$slug = "solinterpretes"; ?>

<div class="col-12">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <?php                           
            if(session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
            <?php endif;
            if(session()->get('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?= session()->get('error') ?>
                </div>
            <?php endif; ?>
            <form class="" action="<?= base_url() ?>/<?= $slug ?>/formulario" method="post">
                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Lugar -->
                        <div class="form-group">
                            <label class="small mb-1" for="lugar">Lugar</label>
                            <input class="form-control py-2" id="lugar" name="lugar" type="text"
                                placeholder="Introduce lugar" value="<?= set_value('lugar') ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                            <!-- Campo Fecha -->
                            <div class="form-group">
                                <label class="small mb-1" for="fecha">Fecha</label>
                                <input class="form-control py-2" id="fecha" name="fecha" type="date"
                                    placeholder="Introduce fecha" value="<?= set_value('fecha') ?>" />
                            </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Inicio -->
                        <div class="form-group">
                            <label class="small mb-1" for="inicio">Inicio</label>
                            <input class="form-control py-2" id="inicio" name="inicio" type="time"
                                placeholder="Introduce inicio" value="<?= set_value('inicio') ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    
                    <div class="col-md-4">
                        <!-- Campo Final -->
                        <div class="form-group">
                            <label class="small mb-1" for="final">Final</label>
                            <input class="form-control py-2" id="final" name="final" type="time"
                                placeholder="Introduce final" value="<?= set_value('final') ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Online -->
                        <div class="form-group">
                            <label class="small mb-1" for="online">On-line</label>
                            <select class="form-control py-2" id="online" name="online">
                                    <?php 
                                    foreach($online as $item){
                                        if($item->ID > 4) {
                                        ?>
                                            <option value="<?= $item->ID ?>" <?php if(set_value('online') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Motivos silse -->
                        <div class="form-group">
                            <label class="small mb-1" for="Motivo">Motivo</label>
                            <select class="form-control py-2" id="Motivo" name="Motivo">
                                    <?php 
                                    foreach($motivossilse as $item){
                                        
                                        ?>
                                            <option value="<?= $item->ID ?>" <?php if(set_value('motivossilse') == $item->ID){ echo 'selected';}?>><?= $item->Descripcion ?></option>
                                        <?php
                                        
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <!-- Campo Observaciones -->
                        <div class="form-group">
                            <label class="small mb-1" for="observaciones">Observaciones</label>
                            <input class="form-control py-2" id="observaciones" name="observaciones" type="text"
                                placeholder="Introduce observaciones" value="<?= set_value('observaciones') ?>" />
                        </div>
                    </div>
                </div>

                
                <div class="col-12 form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Enviar solicitud</a>
                </div>
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>
            </form>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){

    $("#volver").hide();
    $("#sidebarToggle").trigger( "click" );
    $("#sidebarToggle").hide();
    $("#userDropdown").hide();

});
</script>