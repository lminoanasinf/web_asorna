<div class="container-fluid">

<?php
helper('html');
titleBreadcum('Datos sordera  ' . $user['name']." ".$user['lastname'],'Usuario / Editar') ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
<?php                           
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; ?>
           
           
            
            <form class="" action="<?=base_url()?>/user/datosordera/<?= $id ?>" method="post">
            <!-- Fila 1 -->
                <div class="form-row">
                    <div class="col-md-4">
            
                        <!-- Campo Tipo Sordera -->
                        <div class="form-group">
                            <label class="small mb-1" for="tipo_sordera">Tipo Sordera</label>
                            <select class="form-control py-2" id="tipo_sordera" name="tipo_sordera">
                            <option value="Ninguna" <?php if(isset($user['tipo_sordera']) && $user['tipo_sordera'] == 'Ninguna'){echo 'selected';}?>>Ninguna</option>
                            <option value="Moderada" <?php if(isset($user['tipo_sordera']) && $user['tipo_sordera'] == 'Moderada'){echo 'selected';}?>>Moderada</option>
                            <option value="Media" <?php if(isset($user['tipo_sordera']) && $user['tipo_sordera'] == 'Media'){echo 'selected';}?>>Media</option>
                            <option value="Alta" <?php if(isset($user['tipo_sordera']) && $user['tipo_sordera'] == 'Alta'){echo 'selected';}?>>Alta</option>
                            <option value="Total" <?php if(isset($user['tipo_sordera']) && $user['tipo_sordera'] == 'Total'){echo 'selected';}?>>Total</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Causa de la sordera -->
                        <div class="form-group">
                            <label class="small mb-1" for="causa_sordera">Causa de la sordera</label>
                            <input class="form-control py-2" id="causa_sordera" name="causa_sordera" type="text"
                                placeholder="Introduce causa sordera" value="<?= $user['causa_sordera'] ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Ayudas técnicas -->
                        <div class="form-group">
                            <label class="small mb-1" for="ayuda_tecnica">Ayudas técnicas</label>
                            <input class="form-control py-2" id="ayuda_tecnica" name="ayuda_tecnica" type="text"
                                placeholder="Introduce ayudas técnicas" value="<?= $user['ayuda_tecnica'] ?>" />
                        </div>
                    </div>
                </div>
            <!-- Fila 2 -->
                <div class="form-row">
                    <!-- Campo Nivel de estudios -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="small mb-1" for="nivel_estudios">Nivel de estudios</label>
                            <input class="form-control py-2" id="nivel_estudios" name="nivel_estudios" type="text"  
                                placeholder="Introduce nivel de estudios" value="<?= $user['nivel_estudios'] ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Centro de estudios -->
                        <div class="form-group">
                            <label class="small mb-1" for="centro_estudios">Centro de estudios</label>
                            <input class="form-control py-2" id="centro_estudios" name="centro_estudios" type="text"
                                placeholder="Introduce centro de estudios"  value="<?= $user['centro_estudios'] ?>"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Comunicación -->
                        <div class="form-group">
                            <label class="small mb-1" for="comunicacion">Comunicación</label>
                            <input class="form-control py-2" id="comunicacion" name="comunicacion"
                                type="text" placeholder="Introduce comunicación" value="<?= $user['comunicacion'] ?>"/>
                        </div>
                    </div>
                </div>
        
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                </div>
            </form>
  
        </div>
    </div>
</div>