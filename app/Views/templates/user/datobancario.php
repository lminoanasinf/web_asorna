<div class="container-fluid">

<?php
helper('html');
titleBreadcum('Datos bancario  ' . $user['name']." ".$user['lastname'],'Usuario / Editar') ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
<?php                           
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; ?>
           
           
            
            <form class="" action="<?=base_url()?>/user/datobancarioedit/<?php if(isset($datobancario)){echo $datobancario['id'];} echo "/" ; echo $user['id'] ?>" method="post">
            <!-- Fila 1 -->
                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Tipo Sordera -->
                        <div class="form-group">
                            <label class="small mb-1" for="fecha">Fecha</label>
                            <input class="form-control py-2" id="fecha" name="fecha" type="date"
                                placeholder="Selecciona fecha" value="<?= $datobancario['fecha'] ?>" />
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <!-- Campo Observaciones -->
                        <div class="form-group">
                            <label class="small mb-1" for="observacion">Numero de cuenta</label>
                            <input class="form-control py-2" id="numero_cuenta" name="numero_cuenta" type="text"
                                placeholder="Introduce IBAN" value="<?= $datobancario['numero_cuenta'] ?>" />
                        </div>
                    </div>
                </div>
                <!-- Fila 2 -->
                <div class="form-row">
                    <div class="col-md-12">
                        <!-- Campo Documento -->
                        <div class="form-group">
                            <label class="small mb-1" for="ruta">Ruta documento SEPA</label>
                            <input class="form-control py-2" id="ruta" name="ruta" type="text"
                                placeholder="Introduce Ruta documento SEPA" value="<?= $datobancario['ruta'] ?>" />
                        </div>
                    </div>
                </div>
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                </div>
            </form>
  
        </div>
    </div>
</div>