<div class="container-fluid">

    <?php
helper('html');
titleBreadcum('Editar Usuario  ' . $user['name']." ".$user['lastname'],'Usuario / Editar') ?>

    <div class="container col-12">
        <div class="row col-12">
            <div class="col-12">
                <?php                           
                if(session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
                <?php endif;
                if(session()->get('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?= session()->get('error') ?>
                </div>
                <?php endif; ?>
                <!-- Datos usuario -->
                <div class="row">
                    <div class="table-responsive col-12">
                        <h5>Datos usuario</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col">Tipo usuario</th>
                                    <th scope="col">Nº Socio</th>
                                    <th scope="col">Servicio alta</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Apellido</th>
                                    <th scope="col">DNI</th>
                                    <th scope="col">Fehca Nacimiento</th>
                                    <th scope="col">Lugar Nacimiento</th>
                                    <th scope="col">Nacionalidad</th>
                                    <th scope="col">Sexo</th>
                                    <th scope="col">Dirección Postal</th>
                                    <th scope="col">Localidad</th>
                                    <th scope="col">Provincia</th>
                                    <th scope="col">Estado civil</th>
                                    <th scope="col">Hijos</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $user['TipoUsuario'] ?></td>
                                    <td><?= $user['id'] ?></td>
                                    <td><?= $user['Servicio'] ?></td>
                                    <td><?= $user['name'] ?></td>
                                    <td><?= $user['lastname'] . " " . $user['lastlastname'] ?></td>
                                    <td><?= $user['dni'] ?></td>
                                    <td><?php if($user['fecha_nacimiento']){invert_fecha($user['fecha_nacimiento'],false); }  ?>
                                    </td>
                                    <td><?= $user['lugar_nacimiento'] ?></td>
                                    <td><?= $user['nacionalidad'] ?></td>
                                    <td><?= $user['sexo'] ?></td>
                                    <td><?= $user['direccion'] ?></td>
                                    <td><?= $user['localidad'] . " " . $user['codigo_postal'] ?></td>
                                    <td><?= $user['provincia'] ?></td>
                                    <td><?= $user['estado_civil'] ?></td>
                                    <td><?= $user['hijos'] ?></td>
                                    <td><a href="<?= base_url().'/user/datousuario/'.$user['id'] ?>"
                                            class="btn btn-primary">Editar</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Datos sordera -->
                <div class="row">
                    <div class="table-responsive mt-4 col-12">
                        <h5>Datos sordera</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col">Tipo sordera</th>
                                    <th scope="col">Causa de la sordera</th>
                                    <th scope="col">Ayudas técnicas</th>
                                    <th scope="col">Nivel de estudios</th>
                                    <th scope="col">Centro de estudios</th>
                                    <th scope="col">Comunicación</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $user['tipo_sordera'] ?></td>
                                    <td><?= $user['causa_sordera'] ?></td>
                                    <td><?= $user['ayuda_tecnica'] ?></td>
                                    <td><?= $user['nivel_estudios'] ?></td>
                                    <td><?= $user['centro_estudios'] ?></td>
                                    <td><?= $user['comunicacion'] ?></td>
                                    <td><a href="<?= base_url().'/user/datosordera/'.$user['id'] ?>"
                                            class="btn btn-primary">Editar</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Datos minusvalia -->
                <div class="row">
                    <div class="table-responsive mt-4 col-6">
                        <h5>Datos minusvalia</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col">Minusvalia</th>
                                    <th scope="col">% Minusvalia</th>
                                    <th scope="col">Motivo</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $user['minusvalia'] ?></td>
                                    <td><?= $user['porcentaje'] ?>%</td>
                                    <td><?= $user['motivo'] ?></td>
                                    <td><a href="<?= base_url().'/user/datominusvalia/'.$user['id'] ?>"
                                            class="btn btn-primary">Editar</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <!-- Recursos sociales -->
                    <div class="table-responsive mt-4 col-6">
                        <h5>&nbsp;</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col">Recursos sociales</th>
                                    <th scope="col"><a href="<?= base_url().'/user/nuevorecurso/' . $user['id']?>"
                                            class="btn btn-success">Nuevo</a></th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach($recursos as $option){ 
                                     ?>
                                <tr>
                                    <td><?= $option->description ?></td>
                                    <td><a href="<?= base_url().'/user/borrarrecurso/'.$option->id_user."/".$option->id_recurso?>"
                                            class="btn btn-danger">Borrar</a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
                <!-- Datos comunicacion -->
                <div class="row">
                    <div class="table-responsive mt-4 col-6">
                        <h5>Datos comunicación</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col">Tfno fijo</th>
                                    <th scope="col">Tfno movil</th>
                                    <th scope="col">Fax</th>
                                    <th scope="col">Email</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $user['telefono_fijo'] ?></td>
                                    <td><?= $user['telefono_movil'] ?></td>
                                    <td><?= $user['fax'] ?></td>
                                    <td><?= $user['email'] ?></td>
                                    <td><a href="<?= base_url().'/user/datocomunicacion/'.$user['id'] ?>"
                                            class="btn btn-primary">Editar</a></td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col">Envio Email</th>
                                    <th scope="col">Envio Fax</th>
                                    <th scope="col">Envío SMS</th>
                                    <th scope="col">Envío Movil</th>
                                    <th scope="col">Envio WP</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php if($user['envio_email'] == "1"){echo 'Sí';}elseif($user['envio_email'] == "0") {echo "No";} else { echo "Sin asignar";} ?>
                                    </td>
                                    <td><?php if($user['envio_fax'] == "1"){echo 'Sí';}elseif($user['envio_fax'] == "0") {echo "No";} else { echo "Sin asignar";}?>
                                    </td>
                                    <td><?php if($user['envio_sms'] == "1"){echo 'Sí';}elseif($user['envio_sms'] == "0") {echo "No";} else { echo "Sin asignar";}?>
                                    </td>
                                    <td><?php if($user['envio_movil'] == "1"){echo 'Sí';}elseif($user['envio_movil'] == "0") {echo "No";} else { echo "Sin asignar";}?>
                                    </td>
                                    <td><?php if($user['envio_mensajeria'] == "1"){echo 'Sí';}elseif($user['envio_mensajeria'] == "0") {echo "No";} else { echo "Sin asignar";}?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <!-- Autonomia -->
                    <div class="table-responsive mt-4 col-6">
                        <h5>Autonomia</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Observaciones</th>
                                    <th scope="col"></th>
                                    <th scope="col"><a href="<?= base_url().'/user/datoautonomianew/' . $user['id']?>"
                                            class="btn btn-success">Nuevo</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($autonomia as $option){ 
                                     ?>
                                <tr>
                                    <td><?= $option->Tipo ?></td>
                                    <td><?= $option->Observacion ?></td>
                                    <td><a href="<?= base_url().'/user/datoautonomiaedit/'.$option->ID. "/" . $user['id'] ?>"
                                            class="btn btn-primary">Editar</a></td>
                                    <td><a href="<?= base_url().'/user/borrarautonomia/'.$option->ID . "/" . $user['id'] ?>"
                                            class="btn btn-danger">Borrar</a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="row">
                    <div class="table-responsive mt-4 col-12">
                        <h5>Datos bancarios</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Cuenta Bancaria</th>
                                    <th scope="col">Documento SEPA</th>
                                    <th scope="col"></th>
                                    <th scope="col"><a href="<?= base_url().'/user/datobancarionew/'. $user['id'] ?>"
                                            class="btn btn-success">Nuevo</a></th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($datobanco as $item){ 
                                     ?>
                                <tr>
                                    <td><?= invert_fecha($item['fecha']) ?></td>
                                    <td><?= $item['numero_cuenta'] ?></td>
                                    <td><a class="btn btn-link" href="javascript:window.location='file:///<?= str_replace('\\','/',$item['ruta']) ?>'"><?= $item['ruta'] ?></a></td>
                                    <td><a href="<?= base_url().'/user/datobancarioedit/'.$item['id']. "/" . $user['id'] ?>"
                                            class="btn btn-primary">Editar</a></td>
                                    <td><a href="<?= base_url().'/user/borrardatobancario/'.$item['id'] . "/" . $user['id'] ?>"
                                            class="btn btn-danger">Borrar</a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="table-responsive mt-4 col-12">
                        <h5>Historial</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Situación</th>
                                    <th scope="col">Motivo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($historial as $item){ 
                                $item = (array)$item;
                                     ?>
                                <tr>
                                    <td><?= invert_fecha($item['fecha']) ?></td>
                                    <td><?= $item['situacion'] ?></td>
                                    <td><?= $item['motivo'] ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Acciones asociadas al usuario -->
                <div class="row">
                    <div class="table-responsive mt-4 col-12">
                        <h5>Acciones</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <tr role="row">
                                        <th>ID</th>
                                        <th>
                                            Situaciones</th>
                                        <th>
                                            Motivo</th>
                                        <th>
                                            Grupal</th>
                                        <th>
                                            Solicitud</th>
                                        <th>
                                            Proyecto</th>
                                        <th>
                                            Servicios</th>
                                        <th>
                                            Acciones</th>
                                        <th>
                                            Responsable</th>
                                        <th>
                                            Fecha</th>
                                        <th>
                                            Lugar</th>
                                        <th>
                                            Inicio</th>
                                        <th>
                                            Final</th>
                                        <th>
                                            Facturar</th>
                                        <th>
                                            Observaciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach($acciones as $item){ 
                                $item = (array)$item;
                                     ?>
                                <tr>
                                    <td><?= $item['ID'] ?></td>
                                    <td><?= $item['Situaciones'] ?></td>
                                    <td><?= $item['Motivo'] ?></td>
                                    <td><?php if($item['Grupal'] == 0){echo "No";} else {echo "Sí";} ?></td>
                                    <td><?= invert_fecha($item['Solicitud']) ?></td>
                                    <td><?= $item['Proyecto'] ?></td>
                                    <td><?= $item['Servicios'] ?></td>
                                    <td><?= $item['Acciones'] ?></td>
                                    <td><?= $item['Responsable'] ?></td>
                                    <td><?= invert_fecha($item['Fecha']); ?></td>
                                    <td><?= $item['Lugar'] ?></td>
                                    <td><?= $item['Inicio'] ?></td>
                                    <td><?= $item['Final'] ?></td>
                                    <td><?php if($item['Facturar'] == 0){echo "No";} else {echo "Sí";} ?></td>
                                    <td><?php if($item['Observaciones'] == 0){echo " ";} else {echo $item['Observaciones'];} ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="table-responsive mt-4 col-12">
                        <h5>Otros Contactos</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col">Tipo contacto</th>
                                    <th scope="col">nombre</th>
                                    <th scope="col">Tfno fijo</th>
                                    <th scope="col">Tfno movil</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Fax</th>
                                    <th scope="col">Observaciones</th>
                                    <th scope="col"></th>
                                    <th scope="col"><a href="<?= base_url().'/user/datocontactonew/'. $user['id'] ?>"
                                            class="btn btn-success">Nuevo</a></th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach($contactos as $item){ 
                                    $item = (array)$item;
                                     ?>
                                <tr>
                                    <td><?= $item['tipo_contacto'] ?></td>
                                    <td><?= $item['nombre'] ?></td>
                                    <td><?= $item['telefono_fijo'] ?></td>
                                    <td><?= $item['telefono_movil'] ?></td>
                                    <td><?= $item['email'] ?></td>
                                    <td><?= $item['fax'] ?></td>
                                    <td><?= $item['observaciones'] ?></td>
                                    <td><a href="<?= base_url().'/user/contactoedit/'.$item['id']. "/" . $user['id'] ?>"
                                            class="btn btn-primary">Editar</a></td>
                                    <td><a href="<?= base_url().'/user/borrarcontacto/'.$item['id'] . "/" . $user['id'] ?>"
                                            class="btn btn-danger">Borrar</a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="table-responsive mt-4 col-12">
                        <h5>Datos Grupos</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col">Grupo al que pertenece</th>
                                    <th scope="col">Observaciones</th>
                                    <th scope="col"></th>
                                    <th scope="col"><a
                                            href="<?= base_url().'/user/grupopertenencianew/'. $user['id'] ?>"
                                            class="btn btn-success">Nuevo</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($grupos as $item){ 
                                    $item = (array)$item;
                                     ?>
                                <tr>
                                    <td><?= $item['grupo'] ?></td>
                                    <td><?= $item['observaciones'] ?></td>
                                    <td><a href="<?= base_url().'/user/grupopertenenciaedit/'.$item['id']. "/" . $user['id'] ?>"
                                            class="btn btn-primary">Editar</a></td>
                                    <td><a href="<?= base_url().'/user/borrargrupopertenencia/'.$item['id'] . "/" . $user['id'] ?>"
                                            class="btn btn-danger">Borrar</a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="table-responsive mt-4 col-12">
                        <h5><a href="<?= base_url().'/user/comentariosedit/'. $user['id'] ?>"
                                class="btn btn-primary">Editar</a> Observaciones/comentarios </h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2"><?= $user['comentario'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="table-responsive mt-4 col-12">
                        <h5><a href="<?= base_url().'/user/situacioneconomicaedit/'. $user['id'] ?>"
                                class="btn btn-primary">Editar</a> Situación económica</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $user['situacion_economica'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="table-responsive mt-4 col-12">
                        <h5><a href="<?= base_url().'/user/situacionlaboraledit/'. $user['id'] ?>"
                                class="btn btn-primary">Editar</a> Situación laboral</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $user['situacion_laboral'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="table-responsive mt-4 col-12">
                        <h5><a href="<?= base_url().'/user/ambitosocialedit/'. $user['id'] ?>"
                                class="btn btn-primary">Editar</a> Ambito social</h5>
                        <table class="table table-striped text-center">
                            <thead class="thead-dark ">
                                <tr>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $user['ambito_social'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>