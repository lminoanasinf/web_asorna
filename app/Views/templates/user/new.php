<div class="container-fluid">

<?php titleBreadcum('Usuario Nuevo','Usuario / Nuevo') ?>

<div class="container">
<?php                           
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; ?>
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <form class="" action="<?=base_url()?>/user/new" method="post">
                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Nombre -->
                        <div class="form-group">
                            <label class="small mb-1" for="name">Nombre</label>
                            <input class="form-control py-4" id="name" name="name" type="text"
                                placeholder="Introduce tú nombre" value="<?= set_value('name') ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Apellidos -->
                        <div class="form-group">
                            <label class="small mb-1" for="lastname">Apellido 1</label>
                            <input class="form-control py-4" id="lastname" name="lastname" type="text"
                                placeholder="Primer apellido" value="<?= set_value('lastname') ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Apellidos -->
                        <div class="form-group">
                            <label class="small mb-1" for="lastlastname">Apellido 2</label>
                            <input class="form-control py-4" id="lastlastname" name="lastlastname" type="text"
                                placeholder="Segundo apellido" value="<?= set_value('lastlastname') ?>" />
                        </div>
                    </div>
                </div>
                <!-- Campo Email -->
                <div class="form-group">
                    <label class="small mb-1" for="email">Email</label>
                    <input class="form-control py-4" name='email' id="email" type="email" aria-describedby="emailHelp"
                        placeholder="Introduce tú email" value="<?= set_value('email') ?>" />
                </div>
                <div class="form-row">
                    <div class="col-md-6">
                        <!-- Campo Contraseña -->
                        <div class="form-group">
                            <label class="small mb-1" for="password">Contraseña</label>
                            <input class="form-control py-4" id="password" name="password" type="password"
                                placeholder="Introduce Contraseña" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- Campo Confirmar contraseña -->
                        <div class="form-group">
                            <label class="small mb-1" for="password_confirm">Confirmar contraseña</label>
                            <input class="form-control py-4" id="password_confirm" name="password_confirm"
                                type="password" placeholder="Confirmar contraseña" />
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-12">
                        <!-- Campo Confirmar contraseña -->
                        
                       
                        <div class="form-group">
                            <label class="small mb-1" for="role">Permisos</label>
                            <select class="form-control py-2" id="role" name="role">
                            <?php 
                            foreach($roles as $option){
                                if($option['id'] == 3){
                                    ?>
                                        <option id="<?= $option['id'] ?>" value="<?= $option['id'] ?>" selected><?= $option['description'] ?></option>
                                    <?php
                                
                                if(session()->get('role') != 1){
                                    if($option['id'] != 1) {?>
                                        
                                    <option id="<?= $option['id'] ?>" value="<?= $option['id'] ?>"><?= $option['description'] ?></option>
                                <?php
                                    }
                                }
                                ?>
                                
                                <?php } else {
                                    ?>
                                        <option id="<?= $option['id'] ?>" value="<?= $option['id'] ?>"><?= $option['description'] ?></option>
                                    <?php
                                }
                            } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Crear</a>
                </div>
            </form>

        </div>
    </div>
</div>