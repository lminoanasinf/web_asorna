<div class="container-fluid">
    <?= // Miga de pan 
    helper('html');
    $tituloTabla = "Usuarios";
    $idTabla = "users";
    ?>

    <?php // Miga de pan
    
    titleBreadcum('Usuarios','Usuarios / Consultar');
                              
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; 
    dataTable("Usuarios",$columns,'api/getUsers','user','7,8','text-center',12); 
    
    ?>

<script>
   
</script>   