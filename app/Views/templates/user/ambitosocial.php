<div class="container-fluid">

<?php
helper('html');
titleBreadcum('Ámbito social  ' . $user['name']." ".$user['lastname'],'Usuario / Editar') ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
<?php                           
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; ?>
           
           
            
            <form class="" action="<?=base_url()?>/user/ambitosocialedit/<?php echo $user['id'] ?>" method="post">
            <!-- Fila 1 -->
                <div class="form-row">
                    <div class="col-md-12">
                        <!-- Campo Tipo Sordera -->
                        <div class="form-group">
                            <label class="small mb-1" for="comentario">Ámbito social</label>
                            <textarea class="form-control py-2" rows="20" id="comentario" name="comentario" type="textarea"
                                placeholder="Introduce Comentario" ><?php echo $user['ambito_social']; ?></textarea>
                        </div>
                    </div>
                    
                </div>

                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                </div>
            </form>
  
        </div>
    </div>
</div>