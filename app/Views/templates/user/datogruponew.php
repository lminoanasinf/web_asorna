<div class="container-fluid">

<?php
helper('html');
titleBreadcum('Datos grupos  ' . $user['name']." ".$user['lastname'],'Usuario / Editar') ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
<?php                           
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; ?>
           
           
            
            <form class="" action="<?=base_url()?>/user/grupopertenencianew/<?php echo $user['id'] ?>" method="post">
            <!-- Fila 1 -->
                <div class="form-row">
                    <div class="col-md-3">
                        <!-- Campo Tipo Sordera -->
                        <div class="form-group">
                            <label class="small mb-1" for="id_grupo">Grupo</label>
                            <select class="form-control py-2" id="id_grupo" name="id_grupo">
                                <?php foreach($grupos as $item){ ?>
                                    <option value="<?= $item['id'] ?>"><?= $item['description'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- Campo Observaciones -->
                        <div class="form-group">
                            <label class="small mb-1" for="observaciones">Observaciones</label>
                            <input class="form-control py-2" id="observaciones" name="observaciones" type="text"
                                placeholder="Introduce Observaciones" value="" />
                        </div>
                    </div>
                </div>

                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                </div>
            </form>
  
        </div>
    </div>
</div>