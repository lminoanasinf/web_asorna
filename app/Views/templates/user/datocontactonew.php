<div class="container-fluid">

<?php
helper('html');
titleBreadcum('Datos contactos  ' . $user['name']." ".$user['lastname'],'Usuario / Editar') ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
<?php                           
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; ?>
           
           
            
            <form class="" action="<?=base_url()?>/user/datocontactonew/<?php echo $user['id'] ?>" method="post">
            <!-- Fila 1 -->
                <div class="form-row">
                    <div class="col-md-3">
                        <!-- Campo Tipo Sordera -->
                        <div class="form-group">
                            <label class="small mb-1" for="id_tipo_contacto">Tipo</label>
                            <select class="form-control py-2" id="id_tipo_contacto" name="id_tipo_contacto">
                                <?php foreach($tipocontacto as $item){ ?>
                                    <option value="<?= $item['id'] ?>" ><?= $item['description'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- Campo Nombre -->
                        <div class="form-group">
                            <label class="small mb-1" for="nombre">Nombre</label>
                            <input class="form-control py-2" id="nombre" name="nombre" type="text"
                                placeholder="Introduce nombre" value="" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- Campo Telefono Fijo -->
                        <div class="form-group">
                            <label class="small mb-1" for="telefono_fijo">Teléfono Fijo</label>
                            <input class="form-control py-2" id="telefono_fijo" name="telefono_fijo" type="text"
                                placeholder="Introduce Teléfono Fijo" value="" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- Campo Telefono Móvil -->
                        <div class="form-group">
                            <label class="small mb-1" for="telefono_movil">Telefono Móvil</label>
                            <input class="form-control py-2" id="telefono_movil" name="telefono_movil" type="text"
                                placeholder="Introduce Telefono Móvil" value="" />
                        </div>
                    </div>
                    
                </div>
        <!-- Fila 2 -->
                <div class="form-row">
                    <div class="col-md-3">
                        <!-- Campo Email -->
                        <div class="form-group">
                            <label class="small mb-1" for="email">Email</label>
                            <input class="form-control py-2" id="email" name="email" type="email"
                                placeholder="Introduce Email" value="" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- Campo Fax -->
                        <div class="form-group">
                            <label class="small mb-1" for="fax">Fax</label>
                            <input class="form-control py-2" id="fax" name="fax" type="text"
                                placeholder="Introduce Fax" value="" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- Campo Observaciones -->
                        <div class="form-group">
                            <label class="small mb-1" for="observaciones">Observaciones</label>
                            <input class="form-control py-2" id="observaciones" name="observaciones" type="text"
                                placeholder="Introduce observaciones" value="" />
                        </div>
                    </div>
                </div>

                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Crear</a>
                </div>
            </form>
  
        </div>
    </div>
</div>