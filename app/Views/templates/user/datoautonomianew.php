<div class="container-fluid">

<?php
helper('html');
titleBreadcum('Datos autonomia  ' . $user['name']." ".$user['lastname'],'Usuario / Editar') ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
<?php                           
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; ?>
           
           
            
            <form class="" action="<?=base_url()?>/user/datoautonomianew/<?php echo $user['id']  ?>" method="post">
            <!-- Fila 1 -->
                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Tipo Sordera -->
                        <div class="form-group">
                            <label class="small mb-1" for="tipo_autonomia">Tipo</label>
                            <select class="form-control py-2" id="tipo_autonomia" name="tipo_autonomia">
                                <?php foreach($autonomias as $item){ ?>
                                    <option value="<?= $item['id'] ?>" <?php if(isset($autonomiaSel) && $item['id'] == $autonomiaSel['id_autonomia']){echo 'selected';}?>><?= $item['description'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <!-- Campo Observaciones -->
                        <div class="form-group">
                            <label class="small mb-1" for="observacion">Observaciones</label>
                            <input class="form-control py-2" id="observacion" name="observacion" type="text"
                                placeholder="Introduce observación" value="<?php if(isset($autonomiaSel)){ echo $autonomiaSel['Observacion'];} ?>" />
                        </div>
                    </div>
                </div>
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Crear</a>
                </div>
            </form>
  
        </div>
    </div>
</div>