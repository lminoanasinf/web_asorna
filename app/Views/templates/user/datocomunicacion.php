<div class="container-fluid">

<?php
helper('html');
titleBreadcum('Datos comunicación  ' . $user['name']." ".$user['lastname'],'Usuario / Editar') ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
<?php                           
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; ?>
           
           
            <!-- TODO Recursos -->
            <form class="" action="<?=base_url()?>/user/datocomunicacion/<?= $user['id'] ?>" method="post">
            <!-- Fila 1 -->
                <div class="form-row">
                    <div class="col-md-3">
            
                        <!-- Campo Telefono fijo -->
                        <div class="form-group">
                            <label class="small mb-1" for="telefono_fijo">Teléfono Fijo</label>
                            <input class="form-control py-2" id="telefono_fijo" name="telefono_fijo" type="text"
                                placeholder="Introduce telefono_fijo" value="<?= $user['telefono_fijo'] ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- Campo % Minusvalia -->
                        <div class="form-group">
                            <label class="small mb-1" for="telefono_movil">Teléfono Movil</label>
                            <input class="form-control py-2" id="telefono_movil" name="telefono_movil" type="text"
                                placeholder="Introduce Teléfono Movil" value="<?= $user['telefono_movil'] ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- Campo Motivo -->
                        <div class="form-group">
                            <label class="small mb-1" for="fax">Fax</label>
                            <input class="form-control py-2" id="fax" name="fax" type="text"
                                placeholder="Introduce Fax" value="<?= $user['fax'] ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- Campo Motivo -->
                        <div class="form-group">
                            <label class="small mb-1" for="email">Email</label>
                            <input class="form-control py-2" readonly id="email" name="email" type="text"
                                placeholder="Introduce Email" value="<?= $user['email'] ?>" />
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-row justify-content-center">
                    <div class="col-md-2">
                        <!-- Campo Email Check -->
                        <div class="form-check">
                            <input class="form-check-input" <?php if($user['envio_email']){ echo 'checked';} ?> type="checkbox" value="envio_email" id="envio_email" name="envio_email">
                            <label class="form-check-label" for="envio_email">
                                Envío Email
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- Campo Fax Check -->
                        <div class="form-check">
                            <input class="form-check-input" <?php if($user['envio_fax']){ echo 'checked';} ?> type="checkbox" value="envio_fax" id="envio_fax" name="envio_fax">
                            <label class="form-check-label" for="envio_fax">
                                Envío Fax
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- Campo SMS Check -->
                        <div class="form-check">
                            <input class="form-check-input" <?php if($user['envio_sms']){ echo 'checked';} ?> type="checkbox" value="envio_sms" id="envio_sms" name="envio_sms">
                            <label class="form-check-label" for="envio_sms">
                                Envío SMS
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- Campo WP Check -->
                        <div class="form-check">
                            <input class="form-check-input" <?php if($user['envio_movil']){ echo 'checked';} ?> type="checkbox" value="envio_movil" id="envio_movil" name="envio_movil">
                            <label class="form-check-label" for="envio_movil">
                                Envío Movil
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- Campo WP Check -->
                        <div class="form-check">
                            <input class="form-check-input" <?php if($user['envio_mensajeria']){ echo 'checked';} ?> type="checkbox" value="envio_mensajeria" id="envio_mensajeria" name="envio_mensajeria">
                            <label class="form-check-label" for="envio_mensajeria">
                                Envío Mensajería
                            </label>
                        </div>
                    </div>
                </div>
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                </div>
            </form>
  
        </div>
    </div>
</div>