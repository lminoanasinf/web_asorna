<div class="container-fluid">

<?php
helper('html');
titleBreadcum('Recursos sociales  ' . $user['name']." ".$user['lastname'],'Usuario / Editar') ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
<?php                           
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; ?>
           
           
            
            <form class="" action="<?=base_url()?>/user/recursosocial/" method="post">
            <!-- Fila 1 -->
                <div class="form-row">
                    <div class="col-md-4">
            
                        <!-- Campo Minusvalia -->
                        <div class="form-group">
                            <label class="small mb-1" for="minusvalia">Minusvalia</label>
                            <input class="form-control py-2" id="minusvalia" name="minusvalia" type="text"
                                placeholder="Introduce minusvalia" value="<?= $user['minusvalia'] ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo % Minusvalia -->
                        <div class="form-group">
                            <label class="small mb-1" for="porcentaje">% Minusvalia</label>
                            <input class="form-control py-2" id="porcentaje" name="porcentaje" type="text"
                                placeholder="Introduce porcentaje minusvalia" value="<?= $user['porcentaje'] ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Motivo -->
                        <div class="form-group">
                            <label class="small mb-1" for="motivo">Motivo</label>
                            <input class="form-control py-2" id="motivo" name="motivo" type="text"
                                placeholder="Introduce ayudas técnicas" value="<?= $user['motivo'] ?>" />
                        </div>
                    </div>
                </div>
        
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                </div>
            </form>
  
        </div>
    </div>
</div>