<div class="container-fluid">

<?php
helper('html');
titleBreadcum('Datos minusvalia  ' . $user['name']." ".$user['lastname'],'Usuario / Editar') ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
<?php                           
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; ?>
           
           
            <!-- TODO Recursos -->
            <form class="justify-content-center" action="<?=base_url()?>/user/nuevorecurso/<?=$user['id']?>" method="post">
            <!-- Fila 1 -->
                <div class="form-row ">
                    <div class="col-md-12">
                        <!-- Campo Tipo usuario -->
                        <div class="form-group">
                            <label class="small mb-1" for="recurso">Recurso social</label>
                            <select class="form-control py-2" id="recurso" name="recurso">
                            <?php 
                            foreach($recursos as $option){
                                ?>
                                    <option id="<?= $option['id'] ?>" value="<?= $option['id'] ?>"><?= $option['description'] ?></option>
                                <?php 
                            } ?>
                            </select>
                        </div>
                    </div>  
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12 mt-4 mb-0">
                        <button class="btn btn-primary btn-block" type="submit">Añadir</a>
                    </div>
                </div>
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>
                
            </form>
  
        </div>
    </div>
</div>