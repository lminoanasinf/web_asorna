<div class="container-fluid">

<?php
helper('html');
titleBreadcum('Editar Usuario  ' . $user['name']." ".$user['lastname'],'Usuario / Editar') ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
<?php                           
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; ?>
           
           
            
            <form class="" action="<?=base_url()?>/user/datousuario/<?= $user['id']?>" method="post">
            <!-- Fila 1 -->
                <div class="form-row">
                    <div class="col-md-4">
            
                        <!-- Campo Nombre -->
                        <div class="form-group">
                            <label class="small mb-1" for="name">Nombre</label>
                            <input class="form-control py-2" id="name" name="name" type="text"
                                placeholder="Introduce tú nombre" value="<?= $user['name'] ?>" />
                                <input class="form-control py-2" id="id" name="id" type="hidden"
                                placeholder="Introduce tú nombre" value="<?= $user['id'] ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Apellidos -->
                        <div class="form-group">
                            <label class="small mb-1" for="lastname">Primer Apellido</label>
                            <input class="form-control py-2" id="lastname" name="lastname" type="text"
                                placeholder="Introduce primer apellidos" value="<?= $user['lastname'] ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Apellidos -->
                        <div class="form-group">
                            <label class="small mb-1" for="lastlastname">Segundo Apellido</label>
                            <input class="form-control py-2" id="lastlastname" name="lastlastname" type="text"
                                placeholder="Introduce segundo apellidos" value="<?= $user['lastlastname'] ?>" />
                        </div>
                    </div>
                </div>
            <!-- Fila 2 -->
                <div class="form-row">
                    <!-- Campo Email -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="small mb-1" for="email">Email</label>
                            <input class="form-control py-2" name="email" id="email" type="email" aria-describedby="emailHelp"
                                placeholder="Introduce tú email" value="<?= $user['email'] ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Contraseña -->
                        <div class="form-group">
                            <label class="small mb-1" for="password">Contraseña</label>
                            <input class="form-control py-2" id="password" name="password" type="password"
                                placeholder="Introduce Contraseña" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Confirmar contraseña -->
                        <div class="form-group">
                            <label class="small mb-1" for="password_confirm">Confirmar contraseña</label>
                            <input class="form-control py-2" id="password_confirm" name="password_confirm"
                                type="password" placeholder="Confirmar contraseña" />
                        </div>
                    </div>
                </div>
            <!-- Fila 3 -->
                <div class="form-row">
                    <div class="col-md-3">
                        <!-- Campo Tipo usuario -->
                        <div class="form-group">
                            <label class="small mb-1" for="role">Tipo usuario</label>
                            <select class="form-control py-2" id="role" name="role">
                            <?php 
                            foreach($roles as $option){
                                if($option['id'] == $user['role_id']){
                                ?>
                                    <option id="<?= $option['id'] ?>" value="<?= $option['id'] ?>" selected><?= $option['description'] ?></option>
                                <?php
                                } else{
                                ?>
                                    <option id="<?= $option['id'] ?>" value="<?= $option['id'] ?>"><?= $option['description'] ?></option>
                                <?php }
                            } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- Campo Numero socio -->
                        <div class="form-group">
                            <label class="small mb-1" for="id">Nº Socio</label>
                            <input class="form-control py-2" id="id" type="text" name="n_socio"
                                placeholder="Introduce tú Nº Socio" value="<?php if(isset($user['n_socio'])){echo $user['n_socio'];} ?>" />
                        </div>
                    </div>
                    
                    <div class="col-md-2">
                        <!-- Campo Fecha nacimiento -->
                        <div class="form-group">
                            <label class="small mb-1" for="fecha_nacimiento">Fecha nacimiento</label>
                            <input class="form-control py-2" name="fecha_nacimiento" id="fecha_nacimiento" type="date"
                                value="<?= campo_fecha( $user['fecha_nacimiento'],true) ?>" />
                                
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- Campo Lugar nacimiento -->
                        <div class="form-group">
                            <label class="small mb-1" for="lugar_nacimiento">Lugar nacimiento</label>
                            <input class="form-control py-2" name="lugar_nacimiento" id="lugar_nacimiento" type="text"
                                value="<?= $user['lugar_nacimiento'] ?>" />
                                
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- Campo Nacionalidad -->
                        <div class="form-group">
                            <label class="small mb-1" for="nacionalidad">Nacionalidad</label>
                            <input class="form-control py-2" name="nacionalidad" id="nacionalidad" type="text"
                                value="<?=  $user['nacionalidad']?>" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- Campo Sexo -->
                        <div class="form-group">
                            <label class="small mb-1" for="sexo">Sexo</label>
                            <select class="form-control py-2" name="sexo" id="sexo">
                                <option value="Varon" <?php if(isset($user['sexo']) && $user['sexo'] == 'Varon'){echo 'selected';}?>>Varon</option>
                                <option value="Hembra" <?php if(isset($user['sexo']) && $user['sexo'] == 'Hembra'){echo 'selected';}?>>Hembra</option>
                                <option value="Otrxs" <?php if(isset($user['sexo']) && $user['sexo'] == 'Otrxs'){echo 'selected';}?>>Otrxs</option>
                            </select>
                        </div>
                    </div>
                </div>
            
            <!-- Fila 4 -->
                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Dirección -->
                        <div class="form-group">
                            <label class="small mb-1" for="direccion">Dirección</label>
                            <input class="form-control py-2" name="direccion" id="direccion" type="text"
                                placeholder="Introduce tú Dirección" value="<?php if(isset($user['direccion'])){echo $user['direccion'];} ?>" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- Campo Localidad -->
                        <div class="form-group">
                            <label class="small mb-1" for="localidad">Localidad</label>
                            <input class="form-control py-2" name="localidad" id="localidad" type="text"
                                placeholder="Introduce tú Localidad" value="<?php if(isset($user['localidad'])){echo $user['localidad'];} ?>" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- Campo Código postal -->
                        <div class="form-group">
                            <label class="small mb-1" for="codigo_postal">Código Postal</label>
                            <input class="form-control py-2" name="codigo_postal" id="codigo_postal" type="text"
                                placeholder="Introduce tú Código Postal" value="<?php if(isset($user['codigo_postal'])){echo $user['codigo_postal'];} ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Provicincia -->
                        <div class="form-group">
                            <label class="small mb-1" for="provincia">Provincia</label>
                            <input class="form-control py-2" name="provincia" id="provincia" type="text"
                                value="<?php if(isset($user['provincia'])){echo $user['provincia'];}?>" />
                                
                        </div>
                    </div>
                </div>     
                
            <!-- Fila 5 -->
                <div class="form-row">
                    <div class="col-md-2">
                        <!-- Campo Estado civil -->
                        <div class="form-group">
                            <label class="small mb-1" for="estado_civil">Estado civil</label>
                            <select class="form-control py-2" id="estado_civil" name="estado_civil">
                                <option value="Soltero/a" <?php if(isset($user['estado_civil']) && $user['estado_civil'] == 'Soltero/a'){echo 'selected';}?>>Soltero/a</option>
                                <option value="Casado/a" <?php if(isset($user['estado_civil']) && $user['estado_civil'] == 'Casado/a'){echo 'selected';}?>>Casado/a</option>
                                <option value="Viudo/a" <?php if(isset($user['estado_civil']) && $user['estado_civil'] == 'Viudo/a'){echo 'selected';}?>>Viudo/a</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <!-- Campo Hijos -->
                        <div class="form-group">
                            <label class="small mb-1" for="hijos">Hijos</label>
                            <input class="form-control py-2" name="hijos" id="hijos" type="number"
                                placeholder="Introduce nº hijos" value="<?php if(isset($user['hijos'])){echo $user['hijos'];} ?>" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- Campo Tipo usuario -->
                        
                        <div class="form-group">
                            <label class="small mb-1" for="servicio_alta">Servicio alta</label>
                            <select class="form-control py-2" id="servicio_alta" name="servicio_alta">
                            <?php foreach($servicioalta as $option){
                                if($option->id == $user['servicioalta_id']){
                                ?>
                                    <option id="<?= $option->id ?>" value="<?= $option->id ?>" selected><?= $option->description ?></option>
                                <?php
                                } else{
                                ?>
                                    <option id="<?= $option->id ?>" value="<?= $option->id ?>"><?= $option->description ?></option>
                                <?php }
                            } ?>
                            
                            </select>
                        </div>
                    </div>
                </div>  
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                </div>
            </form>
  
        </div>
    </div>
</div>