<div class="container-fluid">

<?php titleBreadcum('Perfil ' . $user['name']." ".$user['lastname'],'Usuario / Perfil') ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <form class="" action="<?= base_url() ?>/profile" method="post">
                <div class="form-row">
                    <div class="col-md-4">
                        <!-- Campo Nombre -->
                        <div class="form-group">
                            <label class="small mb-1" for="name">Nombre</label>
                            <input class="form-control py-4" id="name" name="name" type="text"
                                placeholder="Introduce tú nombre" value="<?= $user['name'] ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Apellidos -->
                        <div class="form-group">
                            <label class="small mb-1" for="lastname">Primer Apellidos</label>
                            <input class="form-control py-4" id="lastname" name="lastname" type="text"
                                placeholder="Introduce primer apellido" value="<?= $user['lastname'] ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!-- Campo Apellidos -->
                        <div class="form-group">
                            <label class="small mb-1" for="lastlastname">Segundo Apellidos</label>
                            <input class="form-control py-4" id="lastlastname" name="lastlastname" type="text"
                                placeholder="Introduce segundo apellido" value="<?= $user['lastlastname'] ?>" />
                        </div>
                    </div>
                </div>
                <!-- Campo Email -->
                <div class="form-group">
                    <label class="small mb-1" for="email">Email</label>
                    <input class="form-control py-4" id="email" type="email" readonly aria-describedby="emailHelp"
                        placeholder="Introduce tú email" value="<?= $user['email'] ?>" />
                </div>
                <div class="form-row">
                    <div class="col-md-6">
                        <!-- Campo Contraseña -->
                        <div class="form-group">
                            <label class="small mb-1" for="password">Contraseña</label>
                            <input class="form-control py-4" id="password" name="password" type="password"
                                placeholder="Introduce Contraseña" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- Campo Confirmar contraseña -->
                        <div class="form-group">
                            <label class="small mb-1" for="password_confirm">Confirmar contraseña</label>
                            <input class="form-control py-4" id="password_confirm" name="password_confirm"
                                type="password" placeholder="Confirmar contraseña" />
                        </div>
                    </div>
                </div>
                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Actualizar</a>
                </div>
            </form>

        </div>
    </div>
</div>