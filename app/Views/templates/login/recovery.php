<?php

?>
       <div id="layoutAuthentication">
            <div id="layoutAuthentication_content" class="m-b-20">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header" style="text-align:center;">
										<!-- <h3 class="text-center font-weight-light my-4">Create Account</h3> -->
										<span class="login-logo">
											<img src="<?= base_url() ?>/assets/images/Logo.png" alt="ASORNA">
										</span>
									</div>
                                    <div class="card-body">
                                    <?php if(session()->get('success')): ?>
                                        <div class="alert alert-success" role="alert">
                                            <?= session()->get('success') ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if(session()->get('error')): ?>
                                        <div class="alert alert-danger" role="alert">
                                            <?= session()->get('error') ?>
                                        </div>
                                    <?php endif; ?>
                                        <form class="" action="<?= base_url() ?>/recovery/<?= $token ?>" method="post">
                                            <div class="form-row">
                                                <div class="col-md-6">
													<!-- Campo Contraseña -->
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="password">Contraseña</label>
                                                        <input class="form-control py-4" id="password" name="password" type="password" placeholder="Introduce Contraseña" <?php if(session()->get('error')){?> disabled <?php } ?>/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
													<!-- Campo Confirmar contraseña -->
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="password_confirm">Confirmar contraseña</label>
                                                        <input class="form-control py-4" id="password_confirm" name="password_confirm" type="password" placeholder="Confirmar contraseña" <?php if(session()->get('error')){?> disabled <?php } ?>/>
                                                    </div>
                                                </div>
											</div>
											<!-- Errores de formulario -->
											<?php if (isset($validation)){ ?>
													<div class="col-12">
														<div class="alert alert-danger" role="alert">
															<?= $validation->listErrors() ?>
														</div>
													</div>
												<?php } ?>

                                            <div class="form-group mt-4 mb-0">
												<button class="btn btn-primary btn-block" type="submit">Cambiar Contraseña</a>
											</div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="<?= base_url() ?>/">¿Ya tienes cuenta? Inicia sesión</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
			</div>
			