<?php

?>
       <div id="layoutAuthentication">
            <div id="layoutAuthentication_content" class="m-b-20">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header" style="text-align:center;">
										<!-- <h3 class="text-center font-weight-light my-4">Create Account</h3> -->
										<span class="login-logo">
											<img src="<?= base_url() ?>/assets/images/Logo.png" alt="ASORNA">
										</span>
									</div>
                                    <div class="card-body">
                                        <form class="" action="<?= base_url() ?>/register" method="post">
                                            <div class="form-row">
                                                <div class="col-md-6">
													<!-- Campo Nombre -->
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="name">Nombre</label>
                                                        <input class="form-control py-4" id="name" name="name" type="text" placeholder="Introduce tú nombre" value="<?= set_value('name') ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
													<!-- Campo Apellidos -->
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="lastname">Primir Apellidos</label>
                                                        <input class="form-control py-4" id="lastname" name="lastname" type="text" placeholder="Introduce primer apellido" value="<?= set_value('lastname') ?>"/>
                                                    </div>
                                                </div>
											</div>
                                            <div class="form-row">
                                                <div class="col-md-6"> 
                                                    <!-- Campo Apellidos -->
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="lastlastname">Segundo Apellidos</label>
                                                        <input class="form-control py-4" id="lastlastname" name="lastlastname" type="text"
                                                            placeholder="Introduce segundo apellido" value="<?= set_value('lastlastname') ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <!-- Campo Email -->
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="email">Email</label>
                                                        <input class="form-control py-4" id="email" type="email" name="email" aria-describedby="emailHelp" placeholder="Introduce tú email" value="<?= set_value('email') ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
													<!-- Campo Contraseña -->
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="password">Contraseña</label>
                                                        <input class="form-control py-4" id="password" name="password" type="password" placeholder="Introduce Contraseña" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
													<!-- Campo Confirmar contraseña -->
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="password_confirm">Confirmar contraseña</label>
                                                        <input class="form-control py-4" id="password_confirm" name="password_confirm" type="password" placeholder="Confirmar contraseña" />
                                                    </div>
                                                </div>
											</div>
											<!-- Errores de formulario -->
											<?php if (isset($validation)){ ?>
													<div class="col-12">
														<div class="alert alert-danger" role="alert">
															<?= $validation->listErrors() ?>
														</div>
													</div>
												<?php } ?>

                                            <div class="form-group mt-4 mb-0">
												<button class="btn btn-primary btn-block" type="submit">Registrarse</a>
											</div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="<?= base_url() ?>/">¿Ya tienes cuenta? Inicia sesión</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
			</div>
			