<?php

?>
<div id="layoutAuthentication">
    <div id="layoutAuthentication_content">
        <main>
            <div class="container ">
                <div class="row justify-content-center align-middle align-items-center" style="height:90vh">
                    <div class="col-lg-5">
                        <div class="card shadow-lg border-0 rounded-lg mt-5">
                            <div class="card-header" style="text-align:center;">
                                <!-- <h3 class="text-center font-weight-light my-4">Login</h3> -->
                                <span class="login-logo">
                                    <img src="<?= base_url() ?>/assets/images/Logo.png" alt="ASORNA">
                                </span>
                                <?php
                                // Comprobamos si en la sesión nos devuelve un succes cuando se han registrado 
                                if(session()->get('success')): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?= session()->get('success') ?>
                                    </div>
                                <?php endif;
                                if(session()->get('error')): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?= session()->get('error') ?>
                                    </div>
                                <?php endif; ?>

                            </div>
                            <div class="card-body">
                                <!-- Formulario Login -->
                                <form class="" action="<?= base_url() ?>/" method="post">
                                    <!-- Campo email -->
                                    <div class="form-group">
                                        <label class="small mb-1" for="email">Email</label>
                                        <input class="form-control py-4" id="email" name="email" type="email" placeholder="Introduce el email"  value="<?= set_value('email') ?>"/>
                                    </div>

                                    <!-- Campo contraseña -->
                                    <div class="form-group">
                                        <label class="small mb-1" for="password">Contraseña</label>
                                        <input class="form-control py-4" id="password" name="password" type="password" placeholder="Introduce la contraseña" />
                                    </div>
                                    
                                    <?php 
                                        // Mostramos los mensajes de error
                                        if (isset($validation)){ ?>
                                        <div class="col-12">
                                            <div class="alert alert-danger" role="alert">
                                                <?= $validation->listErrors() ?>
                                            </div>
                                        </div>
                                    <?php } ?>
            
                                    <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                        <a class="small" href="forgot">¿Has olvidado tu contraseña?</a>
                                        <button class="btn btn-primary" type="submit">Iniciar sesión</a>
                                    </div>
                                            
                                </form>
                                <div class="col-12 text-center mt-4">
                                    <a class="btn btn-success" target="_blank" href="<?=base_url()?>/assets/uploads/Asorna.apk" download>Descargar App Movil</a>
                                        </div>
                            </div>
                            <div class="card-footer text-center">
                                <div class="small"><a href="<?= base_url() ?>/register">Registrarse</a></div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
