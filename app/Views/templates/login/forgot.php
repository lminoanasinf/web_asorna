<?php

?>
<div id="layoutAuthentication">
    <div id="layoutAuthentication_content">
        <main>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="card shadow-lg border-0 rounded-lg mt-5">
                            <div class="card-header" style="text-align:center;">
                                <!-- <h3 class="text-center font-weight-light my-4">Login</h3> -->
                                <span class="login-logo">
                                    <img src="<?= base_url() ?>/assets/images/Logo.png" alt="ASORNA">
                                </span>
                                <?php
                                // Comprobamos si en la sesión nos devuelve un succes cuando se han registrado 
                                if(session()->get('success')): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?= session()->get('success') ?>
                                    </div>
                                <?php endif; ?>

                            </div>
                            <div class="card-body">
                                <!-- Formulario Login -->
                                <form class="" action="<?= base_url() ?>/forgot" method="post">
                                <div class="col-12" >
                                    <p style="text-align: center;">No te acuerdas de tu contraseña? <br> Escribe tu email, haz click en "Recuperar" y sigue los pasos que te explicamos en el correo.</p>
                                </div>
                                    <!-- Campo email -->
                                    <div class="form-group">
                                        <label class="small mb-1" for="email">Email</label>
                                        <input class="form-control py-4" id="email" name="email" type="email" placeholder="Introduce el email"  value="<?= set_value('email') ?>" <?php if(session()->get('success')){?> disabled <?php ;} ?>/>
                                    </div>
                                    
                                    <?php 
                                        // Mostramos los mensajes de error
                                        if (isset($validation)){ ?>
                                        <div class="col-12">
                                            <div class="alert alert-danger" role="alert">
                                                <?= $validation->listErrors() ?>
                                            </div>
                                        </div>
                                    <?php } ?>
            
                                    <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                        <a class="small" href="<?= base_url() ?>/">¿Ya tienes cuenta de usuario?</a>
                                        <button class="btn btn-primary" type="submit" <?php if(session()->get('success')){?> disabled <?php ;} ?>>Recuperar</a>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer text-center">
                                <div class="small"><a href="<?= base_url() ?>/register">Registrarse</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
