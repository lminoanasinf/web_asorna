<div class="container-fluid">
    <!-- Descripción y miga de pan -->
    <!-- <?php //titleBreadcum($apartado['description'],$breadcum) ?> -->
    <?php titleBreadcum('Mantenimiento','Mantenimiento');

    if(session()->get('success')): ?>
        <div class="alert alert-success" role="alert">
            <?= session()->get('success') ?>
        </div>
    <?php endif;
    if(session()->get('error')): ?>
        <div class="alert alert-danger" role="alert">
            <?= session()->get('error') ?>
        </div>
    <?php endif; ?>
        <div class="row">
    <?php
        foreach($mantenimiento as $item){
            cardInfoDef($item->Descripcion,$item->Slug,"",3,6);
        }
?>

</div>


</div>