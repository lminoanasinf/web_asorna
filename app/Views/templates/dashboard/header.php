<!DOCTYPE html>
<html lang="es">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>ASORNA - Dashboard</title>
        <link href="<?= base_url()?>/assets/css/styles.css" rel="stylesheet" />
        <link href="<?= base_url()?>/assets/css/utils.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.4.0/main.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="<?= base_url()?>/assets/fullcalendar/main.min.js"></script>
        <script src="<?= base_url()?>/assets/fullcalendar/locales-all.min.js"></script>
        <script src="<?= base_url()?>/assets/js/momentjs.js"></script>

        
       

        <?php       
        // Usamos el helper HTML para simplificar el código
        helper('html');
        use App\Models\ApartadoModel;

        ?>
	</head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- LOGO -->
            <a class="navbar-brand" style="height:auto;" href="<?= base_url() ?>/">
                <img style="heigth:50px;" src="<?=base_url()?>/assets/images/LogoBlanco.png" alt="ASORNA">
            </a>
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href=""><i class="fas fa-bars"></i></button>
            
            <!-- Espacio para sustituir a la búsqueda -->
            <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            </div>
            
            <!-- Menú de Usuario -->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="<?=base_url()?>/profile">Mi cuenta</a>
                        <!-- <a class="dropdown-item" href="">Activity Log</a> -->
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?=base_url()?>/logout">Cerrar sesión</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            
                            <?php 
                            // Usamos el servicio URI Para las comprobaciones de url activa
                            $uri = service('uri');
                            
                            // $modelSection = new UserSection();
                            $modelApartado = new ApartadoModel();
                            
                            // Consultamos todos los apartados con los permismos según usuario
                            $menu = json_decode($modelApartado->getMenuByRole(session()->get('role')));
                            
                            if(session()->get('role') == 1) {
                                menuSectionName('Mantenimiento');  
                                menuItem('Mantenimiento','dashboard',$uri);
                            }

                                menuSectionName('Menú');
     
                            foreach($menu as $item){
                                if($item->Menu == 'Menu'){
                                    menuItemSubDef($item->Descripcion,$item->Slug,$uri,$item->Icon) ;
                                }
                            }
                            
                            ?>
                            
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Sesión iniciada como:</div>
                            
                            <?php 
                                $sessionName = session()->get('name') ;
                                $sessionLastnames = session()->get('lastname');
                                
                                $arrayLastnames = explode(" ", $sessionLastnames);

                                echo $sessionName . " " . $arrayLastnames[0]. "";
                            
                            ?>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>