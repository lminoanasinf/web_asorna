<div class="container-fluid">
    <?= // Miga de pan 
    helper('html');
    ?>

    <?php // Miga de pan
    
    titleBreadcum('Tipos de Usuarios','Usuarios / Tipo de Usuarios');
                              
                if(session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif;
                if(session()->get('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->get('error') ?>
                    </div>
                <?php endif; 

    dataTable("Tipos de Usuario",$columns,'api/getRoles','role','3,4','text-center',12); 
    
    ?>

<script>
   
</script>   