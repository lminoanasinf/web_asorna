<div class="container-fluid">

<?php titleBreadcum('Tipo de Usuario Nuevo','Tipo de Usuario / Nuevo') ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <?php                           
            if(session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
            <?php endif;
            if(session()->get('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?= session()->get('error') ?>
                </div>
            <?php endif; ?>
            <form class="" action="<?=base_url()?>/role/new" method="POST">
                <div class="form-row">
                    <div class="col-md-6">
                        <!-- Campo Descripción -->
                        <div class="form-group">
                            <label class="small mb-1" for="description">Descripción</label>
                            <input class="form-control py-2" id="description" name="description" type="text"
                                placeholder="Introduce descripción" value="<?= set_value('description') ?>" />
                                <input class="form-control py-2" id="id" name="id" type="hidden"
                                placeholder="Introduce tú nombre" value="" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- Campo Facturar -->
                        <div class="form-group">
                            <label class="small mb-1" for="facturar">Facturar</label>
                            <select class="form-control py-2" id="facturar" name="facturar">
                                <option id="facturar" value="1" >Sí</option>
                                <option id="facturar" value="0" selected>No</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-6">
                        <!-- Campo Apartados -->
                        <div class="form-group">
                        <label for="permsver">Permisos para Ver</label>
                        <select multiple class="form-control" name="permsver[]" id="permsver" size="<?= count($apartados); ?>">
                        <?php foreach($apartados as $item){ ?>
                                    <option value="<?= $item->ID ?>"><?= $item->Descripcion ?></option>
                            <?php } ?>
                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- Campo Apartados -->
                        <div class="form-group">
                        <label for="permsedit">Permisos para Editar</label>
                        <select multiple class="form-control" name="permsedit[]" id="permsedit" size="<?= count($apartados) ?>">
                        <?php foreach($apartados as $item){ ?>
                               
                                    <option value="<?= $item->ID ?>"  ><?= $item->Descripcion ?></option>
                                    
                            <?php } ?>
                        </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-6">
                        <!-- Campo Apartados -->
                        <div class="form-group">
                        <label for="permsdelete">Permisos para Borrar</label>
                        <select multiple class="form-control" name="permsdelete[]" id="permsdelete" size="<?= count($apartados) ?>">
                            <?php foreach($apartados as $item){   ?>
                                    <option value="<?= $item->ID ?>"  ><?= $item->Descripcion ?></option>
                                    
                            <?php } ?>
                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- Campo Apartados -->
                        <div class="form-group">
                        <label for="permscreate">Permisos para Crear</label>
                        <select multiple class="form-control" name="permscreate[]" id="permscreate" size="<?= count($apartados) ?>">
                            <?php foreach($apartados as $item){ ?>
                                    <option value="<?= $item->ID ?>"  ><?= $item->Descripcion ?></option>
                                    
                            <?php } ?>
                        </select>
                        </div>
                    </div>
                </div>

                <!-- Errores de formulario -->
                <?php if (isset($validation)){ ?>
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
                <?php } ?>

                <div class="form-group mt-4 mb-0">
                    <button class="btn btn-primary btn-block" type="submit">Crear</a>
                </div>
            </form>
        </div>
    </div>
</div>