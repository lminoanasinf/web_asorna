-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 08-10-2020 a las 06:42:02
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_asorna`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_acciones_realizar`
--

CREATE TABLE `tbl_acciones_realizar` (
  `id` int(11) NOT NULL,
  `grupal` int(11) NOT NULL,
  `fecha_solicitud` datetime NOT NULL,
  `id_persona` int(11) NOT NULL,
  `proyecto` varchar(50) NOT NULL,
  `id_servicios` int(11) NOT NULL,
  `id_acciones` int(11) NOT NULL,
  `responsable` varchar(50) NOT NULL,
  `fecha_accion` datetime NOT NULL,
  `lugar` varchar(50) NOT NULL,
  `hora_inicio` datetime NOT NULL,
  `hora_final` datetime NOT NULL,
  `asistentes` int(11) NOT NULL,
  `socios` int(11) NOT NULL,
  `facturar` int(11) NOT NULL,
  `documento` int(11) NOT NULL,
  `observaciones` int(11) NOT NULL,
  `id_situaciones` int(11) NOT NULL,
  `id_motivo_situaciones` int(11) NOT NULL,
  `comunicacion_web` int(11) NOT NULL,
  `comunicacion_email` int(11) NOT NULL,
  `comunicacion_redes_sociales` int(11) NOT NULL,
  `comunicacion_mensajeria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_acciones_realizar`
--
ALTER TABLE `tbl_acciones_realizar`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_acciones_realizar`
--
ALTER TABLE `tbl_acciones_realizar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
