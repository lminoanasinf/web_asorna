-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 16-09-2020 a las 06:52:20
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_asorna`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_apartados`
--

CREATE TABLE `tbl_apartados` (
  `id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `sub_slug` varchar(100) NOT NULL DEFAULT '',
  `icon` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_apartados`
--

INSERT INTO `tbl_apartados` (`id`, `description`, `slug`, `sub_slug`, `icon`) VALUES
(1, 'Panel de Usuario', 'panel-usuario', '', 'fas fa-columns'),
(2, 'Usuarios', 'users', '', 'fas fa-user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_email_log`
--

CREATE TABLE `tbl_email_log` (
  `id` int(11) NOT NULL,
  `email_from` varchar(100) NOT NULL,
  `email_to` varchar(100) NOT NULL,
  `object` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_email_log`
--

INSERT INTO `tbl_email_log` (`id`, `email_from`, `email_to`, `object`, `description`, `created_at`, `updated_at`) VALUES
(1, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', 'pturbs', '2020-09-10 14:38:59', '2020-09-10 14:38:59'),
(8, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y asignar una nueva contraseña, confirmar dicha contraseña y a continuación realizar cl', '2020-09-10 14:54:18', '2020-09-10 14:54:18'),
(9, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y asignar una nueva contraseña, confirmar dicha contraseña y a continuación realizar cl', '2020-09-11 08:10:22', '2020-09-11 08:10:22'),
(10, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y asignar una nueva contraseña, confirmar dicha contraseña y a continuación realizar cl', '2020-09-11 08:47:26', '2020-09-11 08:47:26'),
(11, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y asignar una nueva contraseña, confirmar dicha contraseña y a continuación realizar cl', '2020-09-11 08:48:58', '2020-09-11 08:48:58'),
(12, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y asignar una nueva contraseña, confirmar dicha contraseña y a continuación realizar cl', '2020-09-11 08:50:17', '2020-09-11 08:50:17'),
(13, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y asignar una nueva contraseña, confirmar dicha contraseña y a continuación realizar cl', '2020-09-11 08:51:45', '2020-09-11 08:51:45'),
(14, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y asignar una nueva contraseña, confirmar dicha contraseña y a continuación realizar cl', '2020-09-11 08:52:59', '2020-09-11 08:52:59'),
(15, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y asignar una nueva contraseña, confirmar dicha contraseña y a continuación realizar cl', '2020-09-11 12:00:17', '2020-09-11 12:00:17'),
(16, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 13:13:12', '2020-09-11 13:13:12'),
(17, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 13:14:07', '2020-09-11 13:14:07'),
(18, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 13:15:10', '2020-09-11 13:15:10'),
(19, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 13:16:45', '2020-09-11 13:16:45'),
(20, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 13:17:29', '2020-09-11 13:17:29'),
(21, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 13:21:29', '2020-09-11 13:21:29'),
(22, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 13:35:40', '2020-09-11 13:35:40'),
(23, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 14:29:46', '2020-09-11 14:29:46'),
(24, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 14:30:25', '2020-09-11 14:30:25'),
(25, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 20:26:07', '2020-09-11 20:26:07'),
(26, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 20:58:52', '2020-09-11 20:58:52'),
(27, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 21:06:13', '2020-09-11 21:06:13'),
(28, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 21:10:48', '2020-09-11 21:10:48'),
(29, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 21:17:41', '2020-09-11 21:17:41'),
(30, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 21:45:32', '2020-09-11 21:45:32'),
(31, 'noreply@asorna.org', 'asdf@asdf.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 21:54:05', '2020-09-11 21:54:05'),
(32, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 21:55:03', '2020-09-11 21:55:03'),
(33, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 22:12:02', '2020-09-11 22:12:02'),
(34, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 22:30:20', '2020-09-11 22:30:20'),
(35, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        <a href=\"http://asorna.oo\">http://asorna.oo</a><br>\r\n         Conectarse a sú cuenta co', '2020-09-11 22:36:22', '2020-09-11 22:36:22'),
(36, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y seguir los pasos para cambiar la contraseña</p><br>\r\n        <a href=\"http://asorna.o', '2020-09-14 08:56:33', '2020-09-14 08:56:33'),
(37, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y seguir los pasos para cambiar la contraseña</p><br>\r\n        <a href=\"http://asorna.o', '2020-09-14 10:40:12', '2020-09-14 10:40:12'),
(38, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y seguir los pasos para cambiar la contraseña</p><br>\r\n        <a href=\"http://asorna.o', '2020-09-14 10:40:58', '2020-09-14 10:40:58'),
(39, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y seguir los pasos para cambiar la contraseña</p><br>\r\n        <a href=\"http://asorna.o', '2020-09-14 11:16:46', '2020-09-14 11:16:46'),
(40, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y seguir los pasos para cambiar la contraseña</p><br>\r\n        <a href=\"http://asorna.o', '2020-09-14 11:21:53', '2020-09-14 11:21:53'),
(41, 'noreply@asorna.org', 'luisminopicallo@gmail.com', 'ASORNA - Restaurar contraseña', '<h1>Restablecer contraseña de ASORNA</h1><br>\r\n        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>\r\n        y seguir los pasos para cambiar la contraseña</p><br>\r\n        <a href=\"http://asorna.o', '2020-09-14 13:30:44', '2020-09-14 13:30:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_permisos`
--

CREATE TABLE `tbl_permisos` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `apartado_id` int(11) NOT NULL,
  `show_secction` int(11) NOT NULL DEFAULT '1',
  `edit_secction` int(11) NOT NULL DEFAULT '0',
  `delete_secction` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_permisos`
--

INSERT INTO `tbl_permisos` (`id`, `role_id`, `apartado_id`, `show_secction`, `edit_secction`, `delete_secction`) VALUES
(1, 1, 1, 1, 1, 1),
(2, 2, 1, 1, 1, 0),
(3, 3, 1, 1, 0, 0),
(4, 1, 2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_recuperar_pass`
--

CREATE TABLE `tbl_recuperar_pass` (
  `token` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `used` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_recuperar_pass`
--

INSERT INTO `tbl_recuperar_pass` (`token`, `user_id`, `created_at`, `deleted_at`, `used`) VALUES
('12523cc9f9245371944c5fc26166a7b4', 6, '2020-09-14 11:16:44', '2020-09-14 11:21:44', 0),
('17e36e5bf6329ce5d282751eb75777b4', 6, '2020-09-11 21:10:44', '2020-09-11 21:15:44', 1),
('2af51660423cb73a525f30baf118d773', 8, '2020-09-11 21:54:02', '2020-09-11 21:59:02', 1),
('349950e76659ab12e0efde7ddc00db40', 6, '2020-09-14 08:56:29', '2020-09-14 09:01:29', 0),
('36869bf5a4fcbb61853c361fc81eaaa4', 6, '2020-09-11 22:11:58', '2020-09-11 22:16:58', 0),
('4b714887ce750320f5d6f61dff3d2280', 6, '2020-09-11 21:17:37', '2020-09-11 21:22:37', 1),
('55c95cc49f81d162581e028f634be8dc', 6, '2020-09-11 21:06:09', '2020-09-11 21:11:09', 1),
('5ba001b5bd83b417c2ccbd44aa6d65ca', 1, '2020-09-14 13:30:39', '2020-09-14 13:35:39', 1),
('760305b40865eac9865b76dc141334a7', 6, '2020-09-14 11:21:50', '2020-09-14 11:26:50', 1),
('81bf4641438a871a48b05547bbc03f19', 6, '2020-09-14 10:40:09', '2020-09-14 10:45:09', 0),
('919a287b22f96f4414cc85f0878fe92a', 6, '2020-09-11 22:36:19', '2020-09-11 22:41:19', 0),
('a4ad01e01a62e470e0bbc30d458f24a1', 6, '2020-09-14 10:40:55', '2020-09-14 10:45:55', 1),
('ad2e5a8911cd3af85410a637b64d0e6a', 6, '2020-09-11 21:55:00', '2020-09-11 22:00:00', 1),
('c7a9b7942f21476a529c1485eb36fe80', 6, '2020-09-11 22:30:17', '2020-09-11 22:35:17', 0),
('e2e0aa7f3684e2e0156a9b730ac45381', 6, '2020-09-11 21:45:29', '2020-09-11 21:50:29', 1),
('ed3df8b08121cc66103427391c63f79c', 6, '2020-09-11 20:58:49', '2020-09-11 21:03:49', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_roles`
--

INSERT INTO `tbl_roles` (`id`, `description`) VALUES
(1, 'Administrador'),
(2, 'Moderador'),
(3, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuarios`
--

CREATE TABLE `tbl_usuarios` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '3',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `eliminated` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`id`, `email`, `password`, `name`, `lastname`, `role_id`, `created_at`, `updated_at`, `eliminated`) VALUES
(1, 'luisminopicallo@gmail.com', '$2y$10$9kqlJqtVWEWFYvLdcNGVuuEPu/pXZFXtzQyG5PofraBHI9Y8wsPo6', 'Administrador', 'Admin', 1, '2020-09-08 23:32:54', '2020-09-08 23:32:54', 0),
(2, 'moderador@moderador.com', '$2y$10$J97B1QDZVzG8FHHU.K./beZ5Eg41AxwrMo9IXb6GFQ4hBNjvt3yb6', 'Moderador', 'Mod', 2, '2020-09-14 12:21:59', '2020-09-14 12:21:59', 0),
(3, 'user@user.com', '$2y$10$J97B1QDZVzG8FHHU.K./beZ5Eg41AxwrMo9IXb6GFQ4hBNjvt3yb6', 'Usuario', 'User', 3, '2020-09-11 13:27:07', '2020-09-11 13:27:07', 0),
(10, 'prueba@prueba.com', '$2y$10$neP8jew1qQWIwiGy8tXqRO8cWJwoa4YteJZTtTRsrIgwZjVtYH5aq', 'Pruebas', 'Pruebas', 3, '2020-09-14 13:40:07', '2020-09-14 13:40:07', 0),
(11, 'luism@anasinf.info', '$2y$10$5GGAHKDl/1FZ/9kpLw8g3OA0fkLAoQKs2JE61zdFskXCizK7gutVm', 'Luis', 'Anasinf', 1, '2020-09-15 14:06:04', '2020-09-15 14:06:04', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_apartados`
--
ALTER TABLE `tbl_apartados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_email_log`
--
ALTER TABLE `tbl_email_log`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_permisos`
--
ALTER TABLE `tbl_permisos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_recuperar_pass`
--
ALTER TABLE `tbl_recuperar_pass`
  ADD PRIMARY KEY (`token`);

--
-- Indices de la tabla `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_apartados`
--
ALTER TABLE `tbl_apartados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_email_log`
--
ALTER TABLE `tbl_email_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `tbl_permisos`
--
ALTER TABLE `tbl_permisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
