<?php 

// Plantilla para la recuperación de contraseña
function emailForgot($token = '',$id = ''){
    
        return '<h1>Restablecer contraseña de ASORNA</h1><br>
        <p>Para restablecer la contraseña de su cuenta de ASORNA debe realizar click en el siguiente enlace<br>
        y seguir los pasos para cambiar la contraseña</p><br>
        <a href="'.base_url("recovery")."/".$id."@".$token.'">'.base_url("recovery")."/".$id."@".$token.'</a>';
}

// Función para enviar email
function sendEmailPass($emailTo,$message,$token,$idUser){

        $email = \Config\Services::email();

        //Enviamos email con el link de restauración de contraseña
        $email->setFrom("noreply@asorna.org", "ASORNA");
        $email->setTo($emailTo);

        $email->setSubject("Restablecer contraseña ASORNA");
        $email->setMessage(emailForgot($token,$idUser));

        $email->send();
}

// Función para enviar email
function sendEmail($emailTo,$subject,$message, $nameOrg="ASORNA"){

        $email = \Config\Services::email();

        //Enviamos email con el link de restauración de contraseña
        $email->setFrom("noreply@asorna.org", "ASORNA");
        $email->setTo($emailTo);

        $email->setSubject($subject);
        $email->setMessage($message);

        $email->send();
}
?>

