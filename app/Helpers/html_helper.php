<?php
// TODO : Parametrización y menú

// Función para poner nombre de sección en el menú
function menuSectionName($sectionName) 
{?>
    <div class="sb-sidenav-menu-heading"><?= $sectionName ?></div>
<?php }

// Función para crear item de menú pasando los parametros
function menuItem($description,$slug,$uri)
{?>
    <a class="nav-link" href="<?= base_url()."/" . $slug ?>">
        <div class="sb-nav-link-icon <?php if($uri->getSegment(1) == $slug){ ?> 'active' <?php ;} ?>">
            <i class="fas fa-tachometer-alt"></i>
        </div>
        <?= $description ?>
    </a>
<?php }

// Función para crear un Item del menú con submenú pasando el array con los datos del submenú
function menuItemSub($description,$slug,$arraySub,$uri)
{ ?>
    <span class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false"
        aria-controls="collapseLayouts">
        <div class="sb-nav-link-icon <?php if($uri->getSegment(1) == $slug){ ?> 'active' <?php ;} ?>"><i
                class="fas fa-columns"></i></div>
        <?= $description ?>
        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
    </span>
    <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
        <nav class="sb-sidenav-menu-nested nav">
            <?php foreach($arraySub as $item){ ?>
            <a class="nav-link <?php if($uri->getSegment(2) == $item['slug']){ ?> 'active' <?php ;} ?>"
                href="<?=base_url()."/". $slug . "/" . $item["slug"] ?>"><?= $item["description"] ?></a>
            <?php } ?>
        </nav>
    </div>
<?php }

// Función para crear Item de menú con opcines Consultar y Nuevo
function menuItemSubDef($description,$slug,$uri,$icon = 'fas fa-columns')
{ ?>
    <span class="nav-link collapsed" href="" data-toggle="collapse" data-target="#<?=$slug?>" aria-expanded="false"
        aria-controls="<?=$slug?>">
        <div class="sb-nav-link-icon <?php if($uri->getSegment(1) == $slug){ ?> 'active' <?php ;} ?>"><i
                class="<?=$icon?>"></i></div>
        <?= $description ?>
        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
    </span>
    <div class="collapse" id="<?=$slug?>" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
        <nav class="sb-sidenav-menu-nested nav">
            <a class="nav-link <?php if($uri->getSegment(2) == 'show'){ ?> 'active' <?php ;} ?>"
                href="<?=base_url(). "/". $slug . "/"?>show"><i class="fas fa-search"></i> &nbsp; Administrar</a>
            <a class="nav-link <?php if($uri->getSegment(2) == 'new'){ ?> 'active' <?php ;} ?>"
                href="<?=base_url(). "/". $slug . "/"?>new"><i class="fas fa-plus"></i> &nbsp; Nuevo</a>
        </nav>
    </div>
<?php } 

// Función para mostrar Titulo de contenido y miga de pan
function titleBreadcum($description,$breadcum)
{ ?>
    <!-- Titulo del apartado
    <div class="row ml-0 mt-2 mb-0 ">
        <a href="javascript:window.history.back()" class="btn btn-primary pull-right ">Volver</a>
    </div> -->
<?php $uri = service('uri');
?>
    <div class="row ml-0 mt-0 mb-1 h-100 pt-4 pb-2">
        <div class="col-6 my-auto">
            <h1 class="my-auto "><?= $description ?></h1>
        </div>
        <div class="col-6 text-right my-auto">
            <a class="mb-0" id="volver" href="javascript: history.go(-1)"><i class="fas fa-chevron-left"></i> Volver atrás</a>
            <?php if($uri->getSegment(2) == "show" || $uri->getSegment(2) == "edit" ){  if($uri->getSegment(1) != 'dispinterpretes'){?>
                <a class=" btn btn-primary mb-0" href="<?= base_url()."/".$uri->getSegment(1)."/new"; ?>"><i class="fas fa-plus"></i> Nuevo</a>
            <?php }} ?>
        </div>
    </div>
    <!-- Miga de Pan -->
<?php }


//Función para crear una tarjeta de color Azul, Roja, Amarilla o Verde
function cardInfo($title,$textLink,$url,$color = 'blue',$colXl='3',$colMd='6')
{
    switch($color){
        case 'blue':
            $type = 'primary';
            break;
        case 'red':
            $type = 'danger';
            break;
        case 'yellow':
            $type = 'warning';
            break;
        case 'green':
            $type = 'success';
            break;
    }
    ?>
    <div class="col-xl-<?= $colXl ?> col-md-<?= $colMd ?>">
            <div class="card bg-<?= $type?> text-white mb-4">
                <div class="card-body"><?= $title ?></div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="<?= base_url().$url ?>"><?= $textLink ?></a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
    <?php
}

//Función para crear una tarjeta de color Azul, Roja, Amarilla o Verde
function cardInfoDef($title,$slug,$color = '',$colXl='3',$colMd='6')
{
    $type="";
    switch($color){
        case 'blue':
            $type = 'bg-primary';
            break;
        case 'red':
            $type = 'bg-danger';
            break;
        case 'yellow':
            $type = 'bg-warning';
            break;
        case 'green':
            $type = 'bg-success';
            break;
    }
    ?>
    <div class="col-xl-<?= $colXl ?> col-md-<?= $colMd ?>">
            <div class="card <?= $type?> text-black mb-4">
                <div class="card-header text-center" style="background-color:#e8e8e8;"><?= strtoupper($title) ?></div>
                <div class="bg-primary card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white bg-primary" style="text-decoration:none" href="<?= base_url() ?>/<?= $slug ?>/show"><i class="fas fa-search"></i> Administrar</a>
                    <a class="small text-white bg-primary" style="text-decoration:none" href="<?= base_url() ?>/<?= $slug ?>/new"><i class="fas fa-plus"></i> Nuevo</a>
                </div>
            </div>
        </div>
    <?php
}

//Función para crear una grafica de lineas
function linearChart($title,$data,$col = 12)
{ ?>
    <div class="col-xl-<?= $col ?>">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-chart-area mr-1"></i>
                    <?= $title ?>
                </div>
                <div class="card-body">
                    <canvas id="myAreaChart" width="100%" height="40"></canvas>
                </div>
            </div>
        </div>
<?php }

//Función para crear una grafica de barras
function barChart($title,$data,$col = 12)
{ ?>
    <div class="col-xl-<?= $col ?>">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-chart-bar mr-1"></i>
                    <?= $title ?>
                </div>
                <div class="card-body">
                    <canvas id="myBarChart" width="100%" height="40"></canvas>
                </div>
            </div>
        </div>
<?php } 

// Función para crear una grafica circular
function pieChart($title,$data,$col = 12)
{ ?>
    <div class="col-xl-<?= $col ?>">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-chart-pie mr-1"></i>
                    <?= $title ?>
                </div>
                <div class="card-body">
                    <canvas id="myPieChart" width="100%" height="40"></canvas>
                </div>
            </div>
        </div>
<?php } 

//Funcion para crear un datatable autamatizado
function dataTable($title,$data,$apiUrl,$slug,$targets = 0,$targetClass = 'text-center',$col = 12,$color = false,$colorCol = 0,$idTable='dataTable',$targetOcul = "")
{ ?>
    <div class="container col-<?= $col?>">
        <div class="row justify-content-center ">
            <div class="col-lg-12">
                <div class="card mb-4 ">
                    <div class="card-body col-12">
                        <!-- DEMO DATATABLE -->
                        <div class="table-responsive col-12">
                            <table class="table table-bordered" id="<?=$idTable?>" width="100%" cellspacing="0">
                                <thead>
                                </thead>
                                <tfoot>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- FIN DEMO DATATABLE -->
                </div>
            </div>
        </div>
    </div>
    <?php 
        $arrayData = [];
        $counter = 1;
        if($data != NULL) {
            foreach($data[0] as $key=>$value){
                $last = sizeof((array)$data[0]);
                switch($counter) {
                    case $last :      
                        array_push($arrayData,"{data:'".$key."',title:'".$key."'},");
                        array_push($arrayData,"{data:'btnEditar',title:''},");   
                        array_push($arrayData,"{data:'btnEliminar',title:''}");
                        break;
                    default:
                        array_push($arrayData,"{data:'".$key."',title:'".$key."'},");
                        $counter++;    
                }
    
        
            };
        } else {
            $arrayData = "";
        }
         

    ?>
     
    <script>
    // Generación de datatable
        $(document).ready(function() {
            var table = $('#<?=$idTable?>').DataTable( {
                ajax: {
                    url: '<?= base_url() ?>/<?=$apiUrl?>',
                    dataSrc: 'data'
                },
                columns: [
                <?php 
                if($arrayData != ""){
                    foreach ($arrayData as $item){
                        echo $item;
                    }
                } else {
                    $arrayData= "";
                }
                
                    ?>
                ],
                <?php if(isset($color) && $color != "") {
                    ?>
                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    if (aData['Situaciones'] == 'Alta') {
                            $(nRow).find('td:eq(0)').addClass('bg-warning');
                            $(nRow).find('td:eq(1)').addClass('bg-warning');
                        } else if (aData['Situaciones'] == "Cancelada") {
                            $(nRow).find('td:eq(0)').addClass('bg-danger');
                            $(nRow).find('td:eq(1)').addClass('bg-danger');
                        } else if (aData['Situaciones'] == "Realizada") {
                            $(nRow).find('td:eq(0)').addClass('bg-success');
                            $(nRow).find('td:eq(1)').addClass('bg-success');
                        }
                    },
                    <?php
                } ?>
                columnDefs: [
                    {
                        targets: [<?= $targets ?>],
                        className: '<?= $targetClass ?>'
                    },
                    {
                        targets: [<?= $targetOcul ?>],
                        visible: false
                    }
                ],
                "language": {
                    url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
            } );

            $("#<?=$idTable?> tbody").on('click','#btnEliminar',function(){

               var result = confirm("¿Desea eliminar el registro?");
               if(result){
                window.location.replace('<?=base_url()?>/<?= $slug ?>/delete/'+$(this).data('id'));
               }
            });
        } );
    </script>
    <?php
}

function campo_fecha($fecha,$ret = false){
    if($fecha) {
        preg_match( '/([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})/', $fecha, $mifecha);
        $lafecha=$mifecha[1]."-".$mifecha[2]."-".$mifecha[3];
        if($ret){
            return $lafecha;
        } 
        echo $lafecha;
    } else {
        echo "";
    }
}

function invert_fecha($fecha,$ret = false){
    if($fecha) {
        preg_match( '/([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})/', $fecha, $mifecha);
        $lafecha=$mifecha[3]."-".$mifecha[2]."-".$mifecha[1];
        if($ret){
            return $lafecha;
        } 
        echo $lafecha;
    }else {
        if($ret){
            return "";
        } 
        echo "";
    }
}

function invert_fecha_api($fecha,$ret = false){
    if($fecha) {
        $partido = explode("/",$fecha);
        $lafecha = $partido[2] . "-" . $partido[1] . "-". $partido[0];
        if($ret){
            return $lafecha;
        } 
        echo $lafecha;
    }else {
        if($ret){
            return "";
        } 
        echo "";
    }
}

function invert_fecha_api_barra($fecha,$ret = false){
    if($fecha) {
        preg_match( '/([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})/', $fecha, $mifecha);
        $lafecha=$mifecha[1]."/".$mifecha[2]."/".$mifecha[3];
        if($ret){
            return $lafecha;
        } 
        echo $lafecha;
    } else {
        echo "";
    }
}